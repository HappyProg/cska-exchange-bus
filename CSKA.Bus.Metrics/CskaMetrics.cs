﻿using App.Metrics;
using App.Metrics.Counter;

namespace CSKA.Queue.WEB.Metrics
{
    public static class CskaMetrics
    {
        public static CounterOptions SmsCounter => new CounterOptions
        {
            Context = "cska_web_api_service",
            Name = "SMS api provider request",
            MeasurementUnit = Unit.Calls
        };

        public static CounterOptions PassageCounter => new CounterOptions
        {
            Context = "cska_web_api_service",
            Name = "Passages",
            MeasurementUnit = Unit.Calls
        };
    }
}
