﻿using CSKA.Queue.Domain.Core;
using System.Threading.Tasks;

namespace CSKA.Queue.Domain.Interfaces
{
    public interface ISmsProvider : IMessageSenderProvider
    {
        Task<MessageStatus> GetMessageStatusAsync(string id);
    }
}
