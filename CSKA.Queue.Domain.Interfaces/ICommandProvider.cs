﻿using System.Threading.Tasks;

namespace CSKA.Queue.Domain.Interfaces
{
    public interface ICommandProvider<T>
    {
        Task SendAsync(T data);
    }
}
