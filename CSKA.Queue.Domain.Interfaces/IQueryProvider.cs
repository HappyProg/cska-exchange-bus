﻿using CSKA.Queue.Domain.Core;
using System.Threading.Tasks;

namespace CSKA.Queue.Domain.Interfaces
{
    public interface IQueryProvider<TResult>
    {
        string Key { get; }
        Task<QueryResult<TResult>> GetDataAsync(int pageNum, int pageSize, string state = null);
    }
}
