﻿using System;
using System.Threading.Tasks;

namespace CSKA.Queue.Domain.Interfaces
{
    public interface IEventBus
    {
        Task Publish<T>(T message);
        Task PublishDelay<T>(T message, TimeSpan delay) where T : class;
    }
}
