﻿using CSKA.Queue.Domain.Core;
using System.Threading.Tasks;

namespace CSKA.Queue.Domain.Interfaces
{
    public interface IMessageSenderProvider
    {
        /// <summary>
        /// Отправка сообщения
        /// </summary>
        /// <param name="message"></param>
        /// <returns>Идентификатор сообщения</returns>
        Task<string> SendAsync(SmsMessage message);
    }
}
