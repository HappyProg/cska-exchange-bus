﻿using CSKA.Queue.Providers.Infomatika;
using System.Linq;
using Xunit;

namespace Cska.Queue.Providers.Infomatika.Tests
{
    public class InfomatikaQueryProviderTests
    {
        private readonly InfomatikaQueryProvider _infomatikaQueryProvider;
        private const int DEFAULT_PAGE_SIZE = 5;

        public InfomatikaQueryProviderTests()
        {
            _infomatikaQueryProvider = new InfomatikaQueryProvider();
        }

        [Fact]
        public void GetData()
        {
            // Arrange
            var pageSize = DEFAULT_PAGE_SIZE;

            // Act
            var result = _infomatikaQueryProvider.GetData(pageNum: 0, pageSize: pageSize);

            // Assert
            Assert.True(result.Count() > 0);
        }
    }
}
