﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.Infomatika;
using System;
using System.Linq;
using Xunit;

namespace Cska.Queue.Providers.Infomatika.Tests
{
    public class InfomatikaClientTests
    {
        private readonly InfomatikaClient _infomatikaClient;
        private const int DEFAULT_EVENT_ID = 640;
        private const int DEFAULT_UNLOAD_SIZE = 5;

        public InfomatikaClientTests()
        {
            _infomatikaClient = new InfomatikaClient();
        }

        [Fact]
        public void GetEvents()
        {
            // Arrange
            var firstDayOfCurrentYear = new DateTime(DateTime.Now.Year, 1, 1);
            var lastDayOfCurrentYear = new DateTime(DateTime.Now.Year, 12, 31);

            // Act
            var events = _infomatikaClient.GetEventsAsync(startPeriod: firstDayOfCurrentYear, endPeriod: lastDayOfCurrentYear)
                .GetAwaiter()
                .GetResult();

            // Assert
            Assert.True(events.Count() > 0);
            Assert.False(string.IsNullOrWhiteSpace(events.FirstOrDefault()?.sitting_name));
        }

        [Fact]
        public void GetTickets()
        {
            // Arrange
            var eventId = DEFAULT_EVENT_ID;
            var unloadSize = DEFAULT_UNLOAD_SIZE;

            // Act
            var tickets = _infomatikaClient.GetTicketsAsync(ticketProductType: TicketProductType.Ticket, eventId: eventId, unloadSize: unloadSize)
                .GetAwaiter()
                .GetResult();

            // Assert
            Assert.True(tickets.Count() > 0);
            Assert.False(string.IsNullOrWhiteSpace(tickets.FirstOrDefault()?.barcode));
        }

        [Fact]
        public void GetPassagesOfTickets()
        {
            // Arrange
            var eventId = DEFAULT_EVENT_ID;
            var unloadSize = DEFAULT_UNLOAD_SIZE;

            // Act
            var passages = _infomatikaClient.GetPassagesAsync(ticketProductType: TicketProductType.Ticket, eventId: eventId, unloadSize: unloadSize)
                .GetAwaiter()
                .GetResult();

            // Assert
            Assert.True(passages.Count() > 0);
            Assert.False(string.IsNullOrWhiteSpace(passages.FirstOrDefault()?.ticket_barcode));
        }
    }
}
