﻿using System;
using CSKA.PBI.Infrastructure.Api;
using CSKA.PBI.Infrastructure.Configuration;
using CSKA.Queue.Providers.B24.Queries;
using CSKA.Queue.Providers.PBI;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CSKA.PBI.Infrastructure.Extensions
{
    public static class PbiExtension
    {
        public static IServiceCollection AddPbiService(this IServiceCollection services, string host, string connectionString)
        {
            services
                //.AddTransient<PbiPassageProvider>()
                // .AddTransient<PbiTicketsProvider>()
                //.AddTransient<IStorageRepository, StorageRepository>()

               .AddHostedService<PbiDashboardService>()
                //.AddHostedService(sp =>
                //{
                //    var scopeFactory = sp.GetService<IServiceScopeFactory>();
                //    var scope = scopeFactory.CreateScope();
                //    //var repo = scope.ServiceProvider.GetRequiredService<IStorageRepository>();
                //    var connector = sp.GetService<IPbiApiConnector>();
                //    var logger = sp.GetService<ILoggerFactory>().CreateLogger<PbiDashboardService>();
                //    var options = sp.GetService<IOptions<PBIConfiguration>>();
                //    var b24Query = sp.GetService<B24PbiQuery>();
                //    return new PbiDashboardService(connector, logger, options, b24Query);

                //})


                //.AddEntityFrameworkNpgsql().AddDbContext<StorageContext>(options =>
                //{
                //    options.UseNpgsql(connectionString);
                //})
                .AddHttpClient<IPbiApiConnector, PbiApiConnector>(cfg => { cfg.BaseAddress = new Uri(host); });
            return services;
        }
    }
}
