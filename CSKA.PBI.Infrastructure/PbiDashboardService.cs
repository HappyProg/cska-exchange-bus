﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bitrix24.Connector;
using CSKA.PBI.Infrastructure.Api;
using CSKA.PBI.Infrastructure.Configuration;
using CSKA.PBI.Infrastructure.Data;
using CSKA.Queue.Providers.B24;
using CSKA.Queue.Providers.B24.Helpers;
using CSKA.Queue.Providers.B24.Queries;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Text.Json;

namespace CSKA.PBI.Infrastructure
{
    public class PbiDashboardService : BackgroundService
    {

        private readonly IPbiApiConnector _connector;

        private readonly ILogger<PbiDashboardService> _logger;

        private readonly B24PbiQuery _b24Query;

        private readonly double _matchProcessingTime;

        private readonly TimeSpan _period;

        public PbiDashboardService(IPbiApiConnector connector, ILogger<PbiDashboardService> logger, IOptions<PBIConfiguration> options, B24PbiQuery b24Query)
        {
            _connector = connector;
            _logger = logger;
            _b24Query = b24Query;
            _matchProcessingTime = options.Value.MatchProcessingTime;
            _period = TimeSpan.FromMinutes(options.Value.Period);
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"{nameof(PbiDashboardService)} is started");
            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"{nameof(PbiDashboardService)} is stopped");
            return base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (true)
            {
                try
                {
                    // Ближайший матч
                    //
                    var nearestEvent = await _b24Query
                        .GetNearestEvent(_matchProcessingTime)
                        .ConfigureAwait(false);
                    if (nearestEvent == null)
                    {
                        _logger.LogInformation("The nearest event was not found!");
                    };
                    var eventId = nearestEvent.GetValue<long>(Const.Fields.B24ListElement.PROP_NAME_EVENT_ID);

                    // Сделки оффлайн продаж атрибутики
                    //
                    var deals = await _b24Query
                        .GetDeals(nearestEvent.Id)
                        .ConfigureAwait(false);

                    // Сделки продаж билетов
                    //
                    var tickets = await _b24Query
                        .GetTickets(nearestEvent.Id)
                        .ConfigureAwait(false);

                    // Проходы на матч
                    //
                    var countPassages = await _b24Query
                        .GetCountPassages(eventId)
                        .ConfigureAwait(false);

                    var data = CreatePbiData(nearestEvent, deals, tickets, countPassages);
                    var jsonData = JsonSerializer.Serialize(data);
                    await _connector
                        .UpdateData(jsonData)
                        .ConfigureAwait(false);

                    _logger.LogInformation("PBI data send with label time " + DateTimeOffset.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz"));
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message, ex);
                }

                await Task.Delay(_period, stoppingToken)
                    .ConfigureAwait(false);
            }
        }

        private PbiData CreatePbiData(B24ListElement nearestEvent, IEnumerable<Deal> deals, IEnumerable<Deal> tickets,
            long passagesCount)
        {
            var ticketsCount = tickets.Count();
            var seatsCount = long.Parse(nearestEvent[Const.Fields.B24ListElement.PROP_NAME_EVENT_SEATS_COUNT].ToString());
            var data = new PbiData
            {
                Time = DateTimeOffset.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz"),
                TicketSold = ticketsCount,
                TicketProceeds = tickets.Sum(B24Helper.GetPrice),
                MerchandiseProceeds = deals.Sum(B24Helper.GetPrice),
                MatchName = nearestEvent.Name,
                DateBegin = B24Helper.GetDateTime(nearestEvent).ToString("yyyy-MM-ddTHH:mm:ss.fffzzz"),
                TotalPlaces = seatsCount,
                SoldPercentage = Math.Round((double)(ticketsCount * 100) / seatsCount, 2),
                PassesCount = passagesCount,
                PassesPercentage = Math.Round((double)(passagesCount * 100) / ticketsCount, 2),
                FullPercentage = Math.Round((double)(passagesCount * 100) / seatsCount, 2),
            };
            return data;
        }
    }
}
