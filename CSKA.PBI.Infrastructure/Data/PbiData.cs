﻿using System.Text.Json.Serialization;

namespace CSKA.PBI.Infrastructure.Data
{
    public class PbiData
    {
        [JsonPropertyName("Время")]
        public string Time { get; set; }

        [JsonPropertyName("Продано билетов")]
        public int TicketSold { get; set; }

        [JsonPropertyName("Выручка за билеты")]
        public double TicketProceeds { get; set; }

        [JsonPropertyName("Выручка за мерч")]
        public double MerchandiseProceeds { get; set; }

        [JsonPropertyName("Матч")]
        public string MatchName { get; set; }

        [JsonPropertyName("Дата начала")]
        public string DateBegin { get; set; }

        [JsonPropertyName("Всего мест")]
        public long TotalPlaces { get; set; }

        [JsonPropertyName("Продано, %")]
        public double SoldPercentage { get; set; }

        [JsonPropertyName("Проходы")]
        public long PassesCount { get; set; }

        [JsonPropertyName("Проходы, %")]
        public double PassesPercentage { get; set; }

        [JsonPropertyName("Заполненность, %")]
        public double FullPercentage { get; set; }

    }
}
