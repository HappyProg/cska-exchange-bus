﻿using System.Threading.Tasks;

namespace CSKA.PBI.Infrastructure.Api
{
    public interface IPbiApiConnector
    {
        Task UpdateData(string json);
    }
}
