﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace CSKA.PBI.Infrastructure.Api
{
    public class PbiApiConnector : IPbiApiConnector
    {
        private readonly HttpClient _client;

        private readonly ILogger<PbiApiConnector> _logger;

        public PbiApiConnector(HttpClient client, ILogger<PbiApiConnector> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task UpdateData(string json)
        {
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            try
            {
                var response = await _client
                    .PostAsync(_client.BaseAddress, httpContent)
                    .ConfigureAwait(false);

                response.EnsureSuccessStatusCode();
                _logger.LogInformation("Data PBI has been successfully updated");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Data PBI has not been updated");
                throw;
            }
        }
    }
}
