﻿namespace CSKA.PBI.Infrastructure.Configuration
{
    public class PBIConfiguration
    {
        public string Host { get; set; }

        public double MatchProcessingTime { get; set; }

        public int Period { get; set; }
    }
}
