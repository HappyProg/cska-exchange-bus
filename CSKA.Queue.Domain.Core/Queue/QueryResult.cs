﻿using System.Collections.Generic;

namespace CSKA.Queue.Domain.Core
{
    public class QueryResult<T>
    {
        public List<T> Data { get; set; } = new List<T>();
        public string State { get; set; }
        public int Count { get => Data != null ? Data.Count : 0; }
    }
}
