﻿using System;

namespace CSKA.Queue.Domain.Core
{
    [Serializable]
    public class ProviderCriticalException : Exception
    {
        public ProviderCriticalException(string message) : base(message)
        {
        }
    }
}
