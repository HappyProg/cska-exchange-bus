﻿using System;

namespace CSKA.Queue.Domain.Core
{
    [Serializable]
    public class ProviderNotConnectedException : Exception
    {
        public ProviderNotConnectedException()
        {
        }

        public ProviderNotConnectedException(string message) : base(message)
        {
        }
    }
}
