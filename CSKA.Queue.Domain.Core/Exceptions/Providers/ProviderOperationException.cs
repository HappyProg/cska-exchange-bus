﻿using System;

namespace CSKA.Queue.Domain.Core
{
    [Serializable]
    public class ProviderOperationException : ExceptionBase
    {
        public ProviderOperationException(string message, object entity = null) : base(message, entity)
        {
        }
    }
}
