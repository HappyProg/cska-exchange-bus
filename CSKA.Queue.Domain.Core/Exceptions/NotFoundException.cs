﻿using System;

namespace CSKA.Queue.Domain.Core
{
    [Serializable]
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
