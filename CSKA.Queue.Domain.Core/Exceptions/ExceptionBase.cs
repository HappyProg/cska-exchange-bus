﻿using System;

namespace CSKA.Queue.Domain.Core
{
    [Serializable]
    public abstract class ExceptionBase : Exception
    {
        public ExceptionBase(string message, object entity = null) : base(message)
        {
            Entity = entity;
        }

        public object Entity { get; }
    }
}
