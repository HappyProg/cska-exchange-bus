﻿namespace CSKA.Queue.Domain.Core
{
    public enum TransactionType
    {
        NotSet = -1,
        /// <summary>
        /// Информация по клиенту
        /// </summary>
        ClientInformation = 102,
        /// <summary>
        /// Билет
        /// </summary>
        Ticket = 1,
        /// <summary>
        /// Абонемент
        /// </summary>
        Subscription = 2,
        /// <summary>
        /// Бронь
        /// </summary>
        Reservation = 101
    }
}
