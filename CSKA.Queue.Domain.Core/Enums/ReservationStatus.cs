﻿namespace CSKA.Queue.Domain.Core
{
    public enum ReservationStatus
    {
        Reserve = 16,
        Cancel = 32,
        AutoCancel = 33
    }
}
