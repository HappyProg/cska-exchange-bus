﻿namespace CSKA.Queue.Domain.Core
{
    public enum Gender
    {
        Undefined,
        Male,
        Female
    }
}
