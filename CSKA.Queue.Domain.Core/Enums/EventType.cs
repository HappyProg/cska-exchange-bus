﻿namespace CSKA.Queue.Domain.Core
{
    public enum EventType
    {
        /// <summary>
        /// Матч
        /// </summary>
        Game,
        /// <summary>
        /// Абонемент
        /// </summary>
        Subscription,
        /// <summary>
        /// Пакет
        /// </summary>
        Batch
    }
}
