﻿namespace CSKA.Queue.Domain.Core
{
    public enum TicketType
    {
        Ticket,
        ElectronTicket,
        Undefined
    }
}
