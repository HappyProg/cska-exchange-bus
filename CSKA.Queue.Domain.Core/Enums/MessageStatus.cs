﻿namespace CSKA.Queue.Domain.Core
{
    public enum MessageStatus
    {
        Send,
        Undelivered,
        Delivered,
        Fail
    }
}
