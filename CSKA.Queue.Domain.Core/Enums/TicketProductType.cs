﻿namespace CSKA.Queue.Domain.Core
{
    public enum TicketProductType
    {
        Undefined = 0,
        Ticket = 1,
        Subscription = 2
    }
}