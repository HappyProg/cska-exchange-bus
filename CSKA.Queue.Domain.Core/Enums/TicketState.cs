﻿namespace CSKA.Queue.Domain.Core
{
    public enum TicketState
    {
        DeletedByEvent = 0,
        Deleted = 1,
        Sold = 2,
        Returned = 3,
        Undefined = 4
    }
}
