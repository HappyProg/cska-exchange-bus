﻿namespace CSKA.Queue.Domain.Core
{
    public class PaymentTypeItem
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public decimal Amount { get; set; }
    }
}
