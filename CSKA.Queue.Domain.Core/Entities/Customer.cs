﻿using System;

namespace CSKA.Queue.Domain.Core
{
    public class Customer : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime? Birth { get; set; }
        public string Phone { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
    }
}
