﻿using CSKA.Queue.Domain.Core.Entities;
using System.Collections.Generic;

namespace CSKA.Queue.Domain.Core
{
    public class Ticket : BaseProviderObject
    {
        public long Id { get; set; }
        public Event Event { get; set; }
        public Seat Seat { get; set; }
        public string Barcode { get; set; }      
        public decimal Price { get; set; }
        public IEnumerable<PaymentTypeItem> Payments { get; set; }
        public TicketType TicketType { get; set; }
        public TicketState TicketState { get; set; }
        public int? BookingId { get; set; }
    }
}
