﻿using System;

namespace CSKA.Queue.Domain.Core
{
    public class Passage
    {
        public DateTime? Date { get; set; }
        public Ticket Ticket { get; set; }
        public bool IsUsed { get; set; }
    }
}
