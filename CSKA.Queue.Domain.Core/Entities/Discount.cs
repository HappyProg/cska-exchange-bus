﻿namespace CSKA.Queue.Domain.Core
{
    public class Discount : BaseEntity
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }
}
