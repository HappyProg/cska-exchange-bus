﻿using CSKA.Queue.Domain.Core.Entities;

namespace CSKA.Queue.Domain.Core
{
    public abstract class BaseEntity : BaseProviderObject
    {
        public string Id { get; set; }
    }
}
