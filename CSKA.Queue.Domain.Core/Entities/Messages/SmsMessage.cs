﻿using CSKA.Queue.Domain.Core.Entities;

namespace CSKA.Queue.Domain.Core
{
    public class SmsMessage : BaseProviderObject
    {
        public string Id { get; set; }
        public string ProviderId { get; set; }
        /// <summary>
        /// Получатель, номер телефона, email и т.д.
        /// </summary>
        public string Receiver { get; set; }
        /// <summary>
        /// Отправитель
        /// </summary>
        public string Sender { get; set; }
        public string Text { get; set; }
        public string Provider { get; set; }
        public MessageStatus Status { get; set; }
    }
}
