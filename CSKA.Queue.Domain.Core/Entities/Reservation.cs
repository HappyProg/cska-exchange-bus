﻿namespace CSKA.Queue.Domain.Core
{
    public class Reservation
    {
        public int? Number { get; set; }
        public int? BookingId { get; set; }

        public long DocumentId { get; set; }
        public Seat Seat { get; set; }
        public bool IsDeleted { get; set; }
        public ReservationStatus Status { get; set; }
        /// <summary>
        /// Флаг проведения
        /// </summary>
        public bool IsPost { get; set; }
        public string Comment { get; set; }
        public Event Event { get; set; }
    }
}
