﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using CSKA.Queue.Domain.Core.Entities;

namespace CSKA.Queue.Domain.Core
{
    public class Event : BaseProviderObject, IEquatable<Event>
    {
        public int EventId { get; set; }
        public int SeatsCountFree { get; set; }
        public string EventName { get; set; }
        public DateTime? Date { get; set; }
        public string ArenaName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete{ get; set; }
        public int ArenaId { get; set; }
        public int SeatsCount { get; set; }
        public EventType EventType { get; set; }
        public Event[] LinkedEvents { get; set; } = new Event[] { };
        /// <summary>
        /// Дата окончания действия, на данный момент используется для абонементов
        /// TODO Вынести в отдельныую сущность, не нравится такой расклад
        /// </summary>
        public DateTime? DateEnd { get; set; }

        public bool Equals([AllowNull] Event other)
        {
            return EventId == other.EventId
                && EventName == other.EventName
                && Date == other.Date
                && ArenaName == other.ArenaName
                && ArenaId == other.ArenaId
                && SeatsCount == other.SeatsCount
                && EventType == other.EventType
                && LinkedEvents
                .OrderBy(o => o)
                .AsEnumerable()
                .SequenceEqual(other.LinkedEvents.OrderBy(o => o).AsEnumerable());

        }
    }
}
