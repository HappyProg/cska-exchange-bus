﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSKA.Queue.Domain.Core
{
    /// <summary>
    /// Место
    /// </summary>
    public class Seat
    {
        /// <summary>
        /// Номер места
        /// </summary>
        public string Num { get; set; }
        /// <summary>
        /// Ряд
        /// </summary>
        public string Row { get; set; }
        /// <summary>
        /// Сектор
        /// </summary>
        public string SectorName { get; set; }
        public string TribuneName { get; set; }
    }
}
