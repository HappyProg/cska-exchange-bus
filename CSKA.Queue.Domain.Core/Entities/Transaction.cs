﻿using System;
using System.Collections.Generic;

namespace CSKA.Queue.Domain.Core
{
    public class Transaction<T> : BaseEntity
    {     
        public DateTime? Date { get; set; }
        public Customer Customer { get; set; }
        public Discount Discount { get; set; }
        public TransactionType TransactionType { get; set; }
        public T Data { get; set; }
        public string Comment { get; set; }
        public bool IsDelete { get; set; }
    }
}
