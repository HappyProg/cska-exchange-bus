﻿
using Newtonsoft.Json;

namespace CSKA.Queue.Domain.Core
{
    public static class Serializer
    {
        public static class JSON
        {
            public static string Serialize<T>(T obj)
            {
                return JsonConvert.SerializeObject(obj);
            }

            public static T Deserialize<T>(string val)
            {
                return JsonConvert.DeserializeObject<T>(val);
            }

            public static bool TryDeserialize<T>(string val, out T obj)
            {              
                try
                {
                    obj = JsonConvert.DeserializeObject<T>(val);
                    return true;
                }
                catch
                {
                    obj = default;
                    return false;
                }
            }
        }
    }
}
