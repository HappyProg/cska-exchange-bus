﻿namespace CSKA.Queue.Domain.Core
{
    public class Bitrix24Configuration
    {
        public string Url { get; set; }

        public string SmsHook { get; set; }

        public string EvoToken { get; set; }
        /// <summary>
        /// Наименование скидки бонусами CityCard
        /// </summary>
        public string BonusDiscountName { get; set; }
    }
}
