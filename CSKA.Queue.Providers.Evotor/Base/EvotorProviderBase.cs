﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using Evotor.API;

namespace CSKA.Queue.Providers.Evotor
{
    public abstract class EvotorProviderBase<TResult> : IQueryProvider<TResult>
    {
        protected readonly IEvotorApiConnector ApiConnector;

        protected EvotorProviderBase(IEvotorApiConnector apiConnector)
        {
            ApiConnector = apiConnector;
        }

        protected IEnumerable<T> GetDifferenceByState<T>(string state, IEnumerable<T> collection, Func<T, T, bool> predicate)
        {
            if (string.IsNullOrWhiteSpace(state))
            {
                return collection;
            }

            var currentStores = DeserializeState<T[]>(state);

            var differences = collection
                .Where(rs => currentStores.All(cs => predicate(cs, rs)))
                .ToArray();

            return differences;
        }

        protected virtual QueryResult<TResult> CreateResult(IEnumerable<TResult> changedData, string state)
        {
            var result = new QueryResult<TResult>
            {
                Data = changedData?.ToList() ?? new List<TResult>(),
                State = state
            };
            return result;
        }

        protected string SerializeState<T>(T state)
        {
            return Serializer.JSON.Serialize(state);
        }

        protected T DeserializeState<T>(string state)
        {
            return string.IsNullOrWhiteSpace(state)
                ? default
                : Serializer.JSON.Deserialize<T>(state);
        }

        public abstract string Key { get; }
        public abstract Task<QueryResult<TResult>> GetDataAsync(int pageNum, int pageSize, string state = null);
    }
}
