﻿using System;
using System.Threading.Tasks;
using CSKA.Queue.Domain.Core;
using Evotor.API;

namespace CSKA.Queue.Providers.Evotor
{
    public class EvotorStoreProvider : EvotorProviderBase<EvotorStore>
    {
        public EvotorStoreProvider(IEvotorApiConnector apiConnector) : base(apiConnector)
        {
        }

        public override string Key => "EVOTOR_STORES";

        public override async Task<QueryResult<EvotorStore>> GetDataAsync(int pageNum, int pageSize, string state = null)
        {
            var entities = await ApiConnector
                .GetStoresV1Async()
                .ConfigureAwait(false);

            Func<EvotorStore, EvotorStore, bool> predicate = (s1, s2) 
                => s1.Name != s2.Name || s1.Address != s2.Address;

            var changedCollection = GetDifferenceByState(state, entities, predicate);
            state = SerializeState(entities);
            return CreateResult(changedCollection, state);
        }
    }
}
