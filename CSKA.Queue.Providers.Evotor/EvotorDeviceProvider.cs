﻿using System;
using System.Threading.Tasks;
using CSKA.Queue.Domain.Core;
using Evotor.API;

namespace CSKA.Queue.Providers.Evotor
{
    public class EvotorDeviceProvider : EvotorProviderBase<EvotorDevice>
    {
        public EvotorDeviceProvider(IEvotorApiConnector apiConnector) : base(apiConnector)
        {
        }

        public override string Key => "EVOTOR_DEVICES";


        public override async Task<QueryResult<EvotorDevice>> GetDataAsync(int pageNum, int pageSize, string state = null)
        {
            var entities = await ApiConnector
                .GetDevicesV1Async()
                .ConfigureAwait(false);

            Func<EvotorDevice, EvotorDevice, bool> predicate = (s1, s2)
                => s1.Name != s2.Name;

            var changedCollection = GetDifferenceByState(state, entities, predicate);
            state = SerializeState(entities);
            return CreateResult(changedCollection, state);
        }
    }
}
