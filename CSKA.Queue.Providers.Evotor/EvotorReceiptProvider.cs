﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Infrastructure;
using Evotor.API;

namespace CSKA.Queue.Providers.Evotor
{
    public class EvotorReceiptProvider : EvotorProviderBase<EvotorReceipt>
    {
        private readonly ICreatorReceiptService _creatorReceipt;

        public EvotorReceiptProvider(IEvotorApiConnector apiConnector, ICreatorReceiptService creatorReceipt) : base(apiConnector)
        {
            _creatorReceipt = creatorReceipt;
        }

        public override string Key => "EVOTOR_RECEIPTS";

        public override async Task<QueryResult<EvotorReceipt>> GetDataAsync(int pageNum, int pageSize, string state = null)
        {
            var beginDate = string.IsNullOrWhiteSpace(state) ? DateTime.Today : DeserializeState<DateTime>(state);
            var endDate = DateTime.Now;

            if (beginDate >= endDate)
            {
                throw new ProviderOperationException("BeginDate >= EndDate");
            }

            var stores = await ApiConnector
                .GetStoresV1Async()
                .ConfigureAwait(false);

            var sells = await GetDocuments(stores, DocumentType.SELL, beginDate, endDate)
                .ConfigureAwait(false);
            var payBack = await GetDocuments(stores, DocumentType.PAYBACK, beginDate, endDate)
                .ConfigureAwait(false);

            var receipts = _creatorReceipt.CreateReceipts(sells);
            receipts.AddRange(_creatorReceipt.CreateReceipts(payBack));

            return new QueryResult<EvotorReceipt>
            {
                Data = receipts,
                State = SerializeState(endDate)
            };
        }

        private async Task<EvotorDocumentList<EvotorDocumentSell>> GetDocuments(IEnumerable<EvotorStore> stores, DocumentType documentType, DateTime beginDate, DateTime endDate)
        {
            var sells = new EvotorDocumentList<EvotorDocumentSell>();
            foreach (var store in stores)
            {
                var storeReceipts = await ApiConnector.GetDocumentsAsync<EvotorDocumentSell>(documentType, store.Uuid, null, beginDate, endDate)
                    .ConfigureAwait(false);
                sells.Items.AddRange(storeReceipts.Items);
            }

            return sells;
        }
    }
}
