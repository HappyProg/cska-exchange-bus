﻿using CSKA.Queue.Data;
using CSKA.Queue.Services.Auth.Options;
using Microsoft.IdentityModel.Tokens;

namespace CSKA.Queue.Services.Auth.Services
{
    public interface IAuthService
    {
        bool IsTokenValid(IAuthOptions options, string token);

        string GenerateToken(Account account, IAuthOptions options);

        public TokenValidationParameters GetTokenValidationParameters(IAuthOptions options);
    }
}
