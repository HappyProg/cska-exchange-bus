﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using CSKA.Queue.Data;
using CSKA.Queue.Services.Auth.Options;
using Microsoft.IdentityModel.Tokens;

namespace CSKA.Queue.Services.Auth.Services.JWT
{
    public class JWTAuthService : IAuthService
    {
        public bool IsTokenValid(IAuthOptions options, string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentException("Given token is null or empty.");

            TokenValidationParameters tokenValidationParameters = GetTokenValidationParameters(options);

            JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            try
            {
                ClaimsPrincipal tokenValid = jwtSecurityTokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken validatedToken);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GenerateToken(Account account, IAuthOptions options)
        {
            var securityKey = options.GetSymmetricKey();
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, account.Login),
                new Claim(JwtRegisteredClaimNames.Sub, account.Id.ToString())
            };

            if (account.Role != null)
            {
                claims.Add(new Claim("role", account.Role.ToString()));
            }

            var token = new JwtSecurityToken(options.Issuer, 
                options.Audience, 
                claims, 
                signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public TokenValidationParameters GetTokenValidationParameters(IAuthOptions options)
        {
            return new TokenValidationParameters()
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = options.GetSymmetricKey(),
                ValidateLifetime = false
            };
        }
    }
}
