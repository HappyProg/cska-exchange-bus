﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace CSKA.Queue.Services.Auth.Options
{
    public class JWTAuthOptions : IAuthOptions
    {

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string SecretKey { get; set; }

        public SecurityKey GetSymmetricKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        }
    }
}
