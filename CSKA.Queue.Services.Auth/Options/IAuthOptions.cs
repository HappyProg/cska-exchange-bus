﻿using Microsoft.IdentityModel.Tokens;

namespace CSKA.Queue.Services.Auth.Options
{
    public interface IAuthOptions
    {
        string Issuer { get; set; }

        string Audience { get; set; }

        string SecretKey { get; set; }

        SecurityKey GetSymmetricKey();
    }
}
