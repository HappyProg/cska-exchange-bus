﻿using CSKA.Queue.Domain.Core.Entities;

namespace Timepad.API.Webhooks
{
    using Newtonsoft.Json;
    using System;

    public class TimepadOrderWebhook : BaseProviderObject
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("mail")]
        public string Mail { get; set; }

        [JsonProperty("payment")]
        public Payment Payment { get; set; }

        [JsonProperty("tickets")]
        public TimepadOrderWebhookTicket[] Tickets { get; set; }

        [JsonProperty("promocodes")]
        public Links Promocodes { get; set; }

        [JsonProperty("event")]
        public Event Event { get; set; }

        [JsonProperty("organization")]
        public Links Organization { get; set; }

        [JsonProperty("referrer")]
        public Referrer Referrer { get; set; }

        [JsonProperty("subscribed_to_newsletter")]
        public bool SubscribedToNewsletter { get; set; }

        [JsonProperty("meta")]
        public Meta Meta { get; set; }

        [JsonProperty("_links")]
        public Links Links { get; set; }

        [JsonProperty("hook_generated_at")]
        public DateTime HookGeneratedAt { get; set; }

        [JsonProperty("hook_guid")]
        public Guid HookGuid { get; set; }

        [JsonProperty("hook_triggered_at")]
        public DateTime HookTriggeredAt { get; set; }
    }

    public partial class Event : BaseProviderObject
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }

        [JsonProperty("starts_at")]
        public string StartsAt { get; set; }

        [JsonProperty("ends_at")]
        public string EndsAt { get; set; }
    }

    public partial class Location : BaseProviderObject
    {
        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }
    }

    public partial class Links : BaseProviderObject
    {
    }

    public partial class Meta : BaseProviderObject
    {
        [JsonProperty("request_snapshot")]
        public RequestSnapshot RequestSnapshot { get; set; }

        //[JsonProperty("registration_event_tikets_left")]
        //public RegistrationEventTiketsLeft RegistrationEventTiketsLeft { get; set; }

        [JsonProperty("policy_metadata_snapshot")]
        public Links PolicyMetadataSnapshot { get; set; }

        [JsonProperty("pricing_changes_snapshot")]
        public PricingChangesSnapshot PricingChangesSnapshot { get; set; }

        [JsonProperty("order_only_discount")]
        public long OrderOnlyDiscount { get; set; }

        [JsonProperty("stat_metadata")]
        public StatMetadata StatMetadata { get; set; }

        [JsonProperty("aux")]
        public Aux Aux { get; set; }

        [JsonProperty("used_promocodes")]
        public Links UsedPromocodes { get; set; }

        [JsonProperty("gaCid")]
        public string GaCid { get; set; }

        [JsonProperty("subscribe")]
        public bool Subscribe { get; set; }

        [JsonProperty("subscribe_digest")]
        public bool SubscribeDigest { get; set; }

        [JsonProperty("referringMAId")]
        public long ReferringMaId { get; set; }

        [JsonProperty("culture")]
        public string Culture { get; set; }

        [JsonProperty("second_phase_done")]
        public DateTime? SecondPhaseDone { get; set; }
    }

    public partial class Aux : BaseProviderObject
    {
        [JsonProperty("use_ticket_remind")]
        public string UseTicketRemind { get; set; }
    }

    public partial class PricingChangesSnapshot : BaseProviderObject
    {
        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("nominal")]
        public long Nominal { get; set; }

        [JsonProperty("total_discount")]
        public long TotalDiscount { get; set; }

        [JsonProperty("order_discount")]
        public long OrderDiscount { get; set; }

        [JsonProperty("ticket_types")]
        public TicketTypeElement[] TicketTypes { get; set; }
    }

    public partial class TicketTypeElement : BaseProviderObject
    {
        [JsonProperty("cat_id")]
        public long CatId { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }

        [JsonProperty("discount")]
        public long Discount { get; set; }

        [JsonProperty("nominal")]
        public long Nominal { get; set; }
    }

    //public partial class RegistrationEventTiketsLeft
    //{
    //    [JsonProperty("3395459")]
    //    public double The3395459 { get; set; }
    //}

    public partial class RequestSnapshot : BaseProviderObject
    {
        //[JsonProperty("res")]
        //public Res Res { get; set; }

        [JsonProperty("user_forms")]
        public /*Answers[]*/ object UserForms { get; set; }

        [JsonProperty("accepted_terms")]
        public string AcceptedTerms { get; set; }

        [JsonProperty("tickets")]
        public RequestSnapshotTicket[] Tickets { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }

        [JsonProperty("ga_cid")]
        public string GaCid { get; set; }

        [JsonProperty("aux")]
        public Aux Aux { get; set; }

        [JsonProperty("referer")]
        public Uri Referer { get; set; }

        [JsonProperty("stat_metadata")]
        public StatMetadata StatMetadata { get; set; }
    }

    //public partial class Res
    //{
    //    [JsonProperty("3395459")]
    //    public string The3395459 { get; set; }
    //}

    public partial class StatMetadata : BaseProviderObject
    {
        [JsonProperty("event_id")]
        public string EventId { get; set; }

        [JsonProperty("org_id")]
        public string OrgId { get; set; }

        [JsonProperty("event_cats")]
        public string EventCats { get; set; }

        [JsonProperty("reg_count")]
        public string RegCount { get; set; }

        [JsonProperty("questions_count")]
        public string QuestionsCount { get; set; }

        [JsonProperty("max_price")]
        public string MaxPrice { get; set; }

        [JsonProperty("from")]
        public string From { get; set; }

        [JsonProperty("widget_mode")]
        public string WidgetMode { get; set; }

        [JsonProperty("widget_id")]
        public string WidgetId { get; set; }

        [JsonProperty("widget_consumer_url")]
        public Uri WidgetConsumerUrl { get; set; }

        [JsonProperty("use_multireg")]
        public bool UseMultireg { get; set; }

        [JsonProperty("widget_use_multiank")]
        public bool WidgetUseMultiank { get; set; }
    }

    public partial class RequestSnapshotTicket : BaseProviderObject
    {
        [JsonProperty("re_id")]
        public string ReId { get; set; }
    }

    public partial class Answers : BaseProviderObject
    {
        [JsonProperty("mail")]
        public string Mail { get; set; }

        [JsonProperty("surname")]
        public string Surname { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class Payment : BaseProviderObject
    {
        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("discount")]
        public long Discount { get; set; }
    }

    public partial class Referrer : BaseProviderObject
    {
        [JsonProperty("campaign")]
        public string Campaign { get; set; }

        [JsonProperty("medium")]
        public string Medium { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }
    }

    public partial class Status
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }

    public partial class TimepadOrderWebhookTicket : BaseProviderObject
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("price_nominal")]
        public string PriceNominal { get; set; }

        [JsonProperty("answers")]
        public /*Answers*/ object Answers { get; set; }

        [JsonProperty("ticket_type")]
        public TicketTicketType TicketType { get; set; }

        [JsonProperty("attendance")]
        public Attendance Attendance { get; set; }

        [JsonProperty("place")]
        public Links Place { get; set; }

        [JsonProperty("codes")]
        public Codes Codes { get; set; }

        [JsonProperty("personal_link")]
        public bool PersonalLink { get; set; }

        [JsonProperty("check_in")]
        public bool CheckIn { get; set; }
    }

    public partial class Attendance : BaseProviderObject
    {
        [JsonProperty("starts_at")]
        public DateTime StartsAt { get; set; }
    }

    public partial class Codes : BaseProviderObject
    {
        [JsonProperty("ean13")]
        public string Ean13 { get; set; }

        [JsonProperty("ean8")]
        public string Ean8 { get; set; }

        [JsonProperty("printed_code")]
        public string PrintedCode { get; set; }
    }

    public partial class TicketTicketType : BaseProviderObject
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("buy_amount_min")]
        public long BuyAmountMin { get; set; }

        [JsonProperty("buy_amount_max")]
        public long BuyAmountMax { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("is_promocode_locked")]
        public bool IsPromocodeLocked { get; set; }

        [JsonProperty("sale_ends_at")]
        public DateTime SaleEndsAt { get; set; }

        [JsonProperty("is_active")]
        public bool IsActive { get; set; }

        [JsonProperty("ad_partner_profit")]
        public long AdPartnerProfit { get; set; }

        [JsonProperty("send_personal_links")]
        public bool SendPersonalLinks { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
