﻿using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;

namespace CSKA.Queue.AutoMapper
{
    public class B24ContactProfile : B24ContactProfileBase
    {
        public B24ContactProfile()
        {
            CreateMap<Customer, B24Contact>()
                .AfterMap((s, d) =>
                {
                    SetContactId(d, Const.Fields.B24ContactConst.IDs, s.Id);
                })
                .ForAllOtherMembers(opt => opt.Ignore())
                //.ForMember(x => x.Email, x => x.MapFrom(z => GetB24Email(z)))
                //.ForMember(x => x.Phone, x => x.MapFrom(s => GetB24Phones(s)))
                //.ForMember(x => x.LastName, x => x.Ignore())
                //.ForMember(x => x.SecondName, x => x.Ignore())
                //.ForMember(x => x.Id, x => x.Ignore())
                //.ForMember(x => x.Fields, x => x.Ignore())
                // Это поле для id клиента инфоматики
                // Более не используется
                //
                //.AfterMap((s, d) => d["UF_CRM_1593433206114"] = s.Id)
                // Множенственное поле для id инфоматики
                //

                ;
        }
    }
}
