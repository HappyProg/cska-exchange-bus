﻿using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using System.Collections.Generic;
using System.Linq;

namespace CSKA.Queue.AutoMapper
{
    public abstract class B24ContactProfileBase : Profile
    {
        protected void SetContactId<T>(T entity, string fieldName, string val)
            where T : EntityBase
        {
            var curVal = entity.GetValueDefault<List<string>>(fieldName) ?? new List<string>();
            
            if (curVal.Any(x => x == val) == false) 
                curVal.Add(val);

            entity[fieldName] = curVal;
        }

        protected EmailMultiField[] GetB24Email(Customer customer)
        {
            return string.IsNullOrWhiteSpace(customer.Email)
                ? null
                : new[]
                {
                    new EmailMultiField
                    {
                        Value = customer.Email,
                        ValueType = EmailType.WORK
                    }
                };
        }

        protected PhoneMultiField[] GetB24Phones(Customer customer)
        {
            return string.IsNullOrWhiteSpace(customer.Phone)
                ? null
                : new[]
                {
                    new PhoneMultiField
                    {
                        Value = customer.Phone,
                        ValueType = PhoneType.MOBILE
                    }
                };
        }
    }
}
