﻿using System;
using AutoMapper;
using CSKA.Queue.Domain.Core;
using Ticket = CSKA.Queue.Domain.Core.Ticket;

namespace CSKA.Queue.AutoMapper.B24.Profiles
{
    public class StorageProfile : Profile
    {
        public StorageProfile()
        {
            CreateMapForTicket(); 
            CreateMapForPassage();
        }

        private void CreateMapForTicket()
        {
            CreateMap<Transaction<Ticket>, PBI.Data.Models.Ticket>()
                .ForMember(x => x.Id, x => x.MapFrom(d => d.Data.Id))
                .ForMember(x => x.Date, x => x.MapFrom(d => d.Date ?? new DateTime()))
                .ForMember(x => x.Barcode, x => x.MapFrom(d => d.Data.Barcode))
                .ForMember(x => x.BookingId, x => x.MapFrom(d => d.Data.BookingId))
                .ForMember(x => x.Discount, x => x.MapFrom(d => d.Discount.Amount))
                .ForMember(x => x.DiscountName, x => x.MapFrom(d => d.Discount.Name))
                .ForMember(x => x.Price, x => x.MapFrom(d => d.Data.Price))
                .ForMember(x => x.TicketState, x => x.MapFrom(d => d.Data.TicketState))
                .ForMember(x => x.TicketType, x => x.MapFrom(d => d.Data.TicketType))
                .ForMember(x => x.Event, x => x.Ignore())
                .ForMember(x => x.Seat, x => x.Ignore())
                .ForMember(x => x.SeatId, x => x.Ignore());
        }
        private void CreateMapForPassage()
        {
            CreateMap<Transaction<Passage>, PBI.Data.Models.Passage>()
                .ForMember(x => x.Date, x => x.MapFrom(d => d.Date ?? new DateTime()))
                .ForMember(x => x.TicketBarcode,
                    x => x.MapFrom(d => d.Data.Ticket != null ? d.Data.Ticket.Barcode : null))
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.EventId, x => x.Ignore())
                .ForMember(x => x.Event, x => x.Ignore());
        }
    }
}
