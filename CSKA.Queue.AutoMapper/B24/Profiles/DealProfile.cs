﻿using System;
using System.ComponentModel;
using System.Linq;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using CSKA.Queue.Providers.B24.Helpers;
using Evotor.API;
using Timepad.API.Webhooks;

namespace CSKA.Queue.AutoMapper
{
    public class DealProfile : Profile
    {
        public DealProfile()
        {
            CreateMapForReservation();
            CreateMapForTicket();
            CreateMapForTimepadOrderWebhook();
            CreateMapForEvotorReceipt();
        }

        private void CreateMapForReservation()
        {
            CreateMap<Transaction<Reservation>, Deal>()
                .ForMember(x => x.Title, x => x.MapFrom(z => $"Документ бронирования от {z.Date.Value:dd.MM.yyyy}"))
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.ContactIds, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.DealConst.PROP_BOOKING_ID] = s.Data.BookingId;
                    d[Const.Fields.DealConst.PROP_NUMBER] = s.Data.Number;
                    d[Const.Fields.DealConst.PROP_EVENT_DATE] = s.Data.Event.Date;
                    d[Const.Fields.DealConst.PROP_COMMENTS] = s.Comment;
                    d[Const.Fields.DealConst.PROP_CATEGORY_ID] = Const.DealCategory.CATEGORY_RESERVATION;
                });
        }

        private void CreateMapForTicket()
        {
            CreateMap<Transaction<Ticket>, Deal>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.ContactIds, x => x.Ignore())
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Title, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.DealConst.PROP_TICKET_TYPE] = ConvertTicketType(s.Data.TicketType);
                    d[Const.Fields.DealConst.PROP_DISCOUNT_NAME] = s.Discount.Name;
                    d[Const.Fields.DealConst.PROP_DISCOUNT_AMOUNT] = s.Discount.Amount;
                    d[Const.Fields.DealConst.PROP_TICKET_ID] = s.Data.Id;
                    d[Const.Fields.DealConst.PROP_SALE_DATE] = s.Date;
                    d[Const.Fields.DealConst.PROP_TICKET_PRICE] = s.Data.Price;
                    d[Const.Fields.DealConst.PROP_TICKET_EVENT_ID] = s.Data.Event.EventId;
                    d[Const.Fields.DealConst.PROP_TICKET_BARCODE] = s.Data.Barcode;
                    d[Const.Fields.DealConst.PROP_EVENT_DATE] = s.Data.Event.Date;
                    d[Const.Fields.DealConst.PROP_TICKET_STATE] = TicketHelper.ConvertTicketSate(s.Data.TicketState);
                    d[Const.Fields.DealConst.PROP_CATEGORY_ID] = TicketHelper.ConvertCategory(s.TransactionType);
                    d[Const.Fields.DealConst.PROP_BOOKING_ID] = s.Data.BookingId;
                });
        }

        private long ConvertTicketType(TicketType type)
        {
            switch (type)
            {
                case TicketType.Ticket:
                    return Const.TICKET_TYPE.Ticket;
                case TicketType.ElectronTicket:
                    return Const.TICKET_TYPE.ETicket;
                default:
                    return Const.TICKET_TYPE.Ticket;
            }
        }

        private void CreateMapForTimepadOrderWebhook()
        {
            CreateMap<TimepadOrderWebhook, Deal>()
                .ForMember(x => x.Title, x => x.MapFrom(z => $"{z.Mail} / {z.Event.Name}"))
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.ContactIds, x => x.Ignore())
                .ForMember(x => x.Fields, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.DealConst.PROP_TICKET_ID] = s.Id;
                    d[Const.Fields.DealConst.PROP_SALE_DATE] = s.CreatedAt;
                    d[Const.Fields.DealConst.PROP_CAMPAIGN] = s.Referrer?.Campaign;
                    d[Const.Fields.DealConst.PROP_MEDIUM] = s.Referrer?.Medium;
                    d[Const.Fields.DealConst.PROP_SOURCE] = s.Referrer?.Source;
                    d[Const.Fields.DealConst.PROP_GACID] = s.Meta?.GaCid;
                    d[Const.Fields.DealConst.PROP_CATEGORY_ID] = Const.DealCategory.CATEGORY_TIMEPAD;
                    d[Const.Fields.DealConst.PROP_TIMEPAD_STATUS] = IsVisitedAllTickets(s) ? "visited" : s.Status.Name.ToLower();
                });
        }

        private void CreateMapForEvotorReceipt()
        {
            CreateMap<EvotorReceipt, Deal>()
                .ForMember(x => x.ContactIds, x => x.Ignore())
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Title, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.DealConst.PROP_SALE_DATE] = s.Data.DateTime;
                    d[Const.Fields.EvotorReceipts.RECEIPT_UUID] = s.Data.Id;
                    d[Const.Fields.EvotorReceipts.SHIFT] = s.Data.ShiftId;
                    d[Const.Fields.EvotorReceipts.PAYMENT_TYPE] = ConvertPaymentType(s.Data.PaymentSource);
                    d[Const.Fields.EvotorReceipts.RECEIPT_TYPE] = EvotorHelper.GetReceiptType(s.Data.Type);
                    d[Const.Fields.EvotorReceipts.SELLER] = s.Data.EmployeeId;
                    d[Const.Fields.DealConst.PROP_CATEGORY_ID] = Const.DealCategory.CATEGORY_OFFLINE_SALE;
                });
        }

        private int ConvertPaymentType(PaymentSource paymentSource)
        {
            switch (paymentSource)
            {
                case PaymentSource.PAY_CASH:
                    return 117;
                case PaymentSource.PAY_CARD:
                    return 118;
                case PaymentSource.OTHER:
                    return 117;
                default:
                    throw new InvalidEnumArgumentException($"Неизвестный тип оплаты: {paymentSource}");
            }
        }

        private bool IsVisitedAllTickets(TimepadOrderWebhook order)
        {
            return !order.Tickets.Any(x => x.Attendance?.StartsAt == default || x.Attendance?.StartsAt == DateTime.MinValue);
        }
    }
}
