﻿using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using System.Collections.Generic;

namespace CSKA.Queue.AutoMapper
{
    public class B24CompanyProfile : B24ContactProfileBase
    {
        public B24CompanyProfile()
        {
            CreateMap<Customer, B24Company>()
                .ForMember(x => x.Title, x => x.MapFrom(z => GetB24CompanyTitle(z)))
                .ForMember(x => x.Email, x => x.MapFrom(z => GetB24Email(z)))
                .ForMember(x => x.Phone, x => x.MapFrom(s => GetB24Phones(s)))
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Id, x => x.Ignore())
                // Более не используется
                //
                //.AfterMap((s, d) => d["UF_CRM_1593433269050"] = s.Id)
                // Множенственное поле для id инфоматики
                //
                .AfterMap((s, d) =>
                {
                    SetContactId(d, Const.Fields.B24CompanyConst.IDs, s.Id);
                })
                ; 
        }

        private string GetB24CompanyTitle(Customer customer)
        {
            return string.IsNullOrWhiteSpace(customer.Name)
                ? string.IsNullOrWhiteSpace(customer.Email)
                    ? "Неизвестная компания"
                    : customer.Email
                : customer.Name;
        }

    }
}
