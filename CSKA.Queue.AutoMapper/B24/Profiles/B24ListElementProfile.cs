﻿using System;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using Evotor.API;
using Timepad.API.Webhooks;
using Event = CSKA.Queue.Domain.Core.Event;

namespace CSKA.Queue.AutoMapper
{
    public class B24ListElementProfile : Profile
    {
        public B24ListElementProfile()
        {
            CreateMapForEvent();
            //CreateMapForSubscriptionEvent();
            //CreateMapForBatchEvent();
            CreateMapForPassage();
            CreateMapForReservation();
            CreateMapForSeats();
            CreateMapForTimepadOrderWebhook();
            CreateMapForEvotorStore();
            CreateMapForEvotorDevice();
        }

        private long GetEventBlockId(EventType type)
        {
            switch (type)
            {
                case EventType.Game:
                    return Const.UniversalLists.BLOCK_ID_EVENTS;
                case EventType.Subscription:
                    return Const.UniversalLists.BLOCK_ID_EVENTS_SUBSCRIPTION;
                case EventType.Batch:
                    return Const.UniversalLists.BLOCK_ID_EVENTS_GAME;
                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// value:64 (отменен)
        /// value:65 (тест)
        /// value:66 (действительный)
        /// value:67 (удален)
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private int GetEventsStatus(Event e)
        {
            if (e.IsDelete)
                return 67;

            return e.IsActive ? 66 : 65;
        }

        private void CreateMapForEvent()
        {
            CreateMap<Event, B24ListElement>()
                .ForMember(x => x.BlockId, x => x.MapFrom(z => GetEventBlockId(z.EventType)))
                .ForMember(x => x.Name, x => x.MapFrom(z => z.EventName))
                .ForMember(x => x.ElementCode, x => x.MapFrom(z => Guid.NewGuid().ToString()))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_EVENT_DATE], x => x.MapFrom(z => z.Date.Value.ToString("dd.MM.yyyy HH:mm")))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_EVENT_ID], x => x.MapFrom(z => z.EventId))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_EVENT_ARENA_NAME], x => x.MapFrom(z => z.ArenaName))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_EVENT_SEATS_COUNT], x => x.MapFrom(z => z.SeatsCount))
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.Fields, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.B24ListElement.PROP_NAME_EVENT_DATE] = s.Date.Value.ToString("dd.MM.yyyy HH:mm");
                    switch (s.EventType)
                    {
                        case EventType.Game:
                            d[Const.Fields.B24ListElement.PROP_NAME_EVENT_ID] = s.EventId;
                            d[Const.Fields.B24ListElement.PROP_NAME_EVENT_STATUS] = GetEventsStatus(s);
                            break;
                        case EventType.Subscription:
                            d[Const.Fields.B24ListElement.PROP_NAME_EVENT_ID_SUBSCRIPTION] = s.EventId;
                            d[Const.Fields.B24ListElement.PROP_NAME_DATE_BEGIN_SUBSCRIPTION] = s.Date;
                            d[Const.Fields.B24ListElement.PROP_NAME_DATE_END_SUBSCRIPTION] = s.DateEnd;
                            break;
                        case EventType.Batch:
                            d[Const.Fields.B24ListElement.PROP_NAME_EVENT_ID_BATCH] = s.EventId;
                            break;
                        default:
                            throw new NotSupportedException();
                    }

                    d[Const.Fields.B24ListElement.PROP_NAME_EVENT_ARENA_NAME] = s.ArenaName;
                    d[Const.Fields.B24ListElement.PROP_NAME_EVENT_SEATS_COUNT] = s.SeatsCount;                  
                });

        }

        //private void CreateMapForSubscriptionEvent()
        //{
        //    CreateMap<SubscriptionEvent, B24ListElement>()
        //        .ForMember(x => x.BlockId, x => x.MapFrom(z => Const.UniversalLists.BLOCK_ID_EVENTS_SUBSCRIPTION))
        //        .ForMember(x => x.Name, x => x.MapFrom(z => z.EventName))
        //        .ForMember(x => x.ElementCode, x => x.MapFrom(z => Guid.NewGuid().ToString()))
        //        //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_EVENT_ID_SUBSCRIPTION], x => x.MapFrom(z => z.EventId))
        //        //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_DATE_BEGIN_SUBSCRIPTION],
        //        //    x =>
        //        //    {
        //        //        x.PreCondition(z => z.Date != null);
        //        //        x.MapFrom(z => z.Date);
        //        //    })
        //        //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_DATE_END_SUBSCRIPTION],
        //        //    x =>
        //        //    {
        //        //        x.PreCondition(z => z.DateEnd != null);
        //        //        x.MapFrom(z => z.DateEnd);
        //        //    })
        //        .ForMember(x => x.Id, x => x.Ignore())
        //        .ForMember(x => x.Fields, x => x.Ignore())
        //        .AfterMap((s, d) =>
        //        {
        //            d[Const.Fields.B24ListElement.PROP_NAME_EVENT_ID_SUBSCRIPTION] = s.EventId;
        //            d[Const.Fields.B24ListElement.PROP_NAME_DATE_BEGIN_SUBSCRIPTION] = s.Date;
        //            d[Const.Fields.B24ListElement.PROP_NAME_DATE_END_SUBSCRIPTION] = s.DateEnd;
                   
        //        });
        //}

        //private void CreateMapForBatchEvent()
        //{
        //    CreateMap<BatchEvent, B24ListElement>()
        //        .ForMember(x => x.BlockId, x => x.MapFrom(z => Const.UniversalLists.BLOCK_ID_EVENTS_GAME))
        //        .ForMember(x => x.Name, x => x.MapFrom(z => z.EventName))
        //        .ForMember(x => x.ElementCode, x => x.MapFrom(z => Guid.NewGuid().ToString()))
        //        //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_EVENT_ID_BATCH],
        //        //    x => x.MapFrom(z => z.EventId))
        //        .ForMember(x => x.Id, x => x.Ignore())
        //        .ForMember(x => x.Fields, x => x.Ignore())
        //        .AfterMap((s, d) =>
        //        {
        //            d[Const.Fields.B24ListElement.PROP_NAME_EVENT_ID_BATCH] = s.EventId;

        //        });
        //}

        private void CreateMapForPassage()
        {
            CreateMap<Transaction<Passage>, B24ListElement>()
                .ForMember(x => x.BlockId, x => x.MapFrom(z => Const.UniversalLists.BLOCK_ID_PASSAGE))
                .ForMember(x => x.ElementCode, x => x.MapFrom(z => Guid.NewGuid().ToString()))
                .ForMember(x => x.Name, x => x.MapFrom(z => z.Data.Ticket.Event.EventName))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_TICKET_BARCODE],
                //    x => x.MapFrom(z => z.Data.Ticket.Barcode))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_TICKET_EVENT_ID],
                //    x => x.MapFrom(z => z.Data.Ticket.Event.EventId))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_DATE_PASSAGE],
                //    x => x.MapFrom(z => GetDateTimeForPassage(z)))
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Id, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.B24ListElement.PROP_NAME_TICKET_BARCODE] = s.Data.Ticket.Barcode;
                    d[Const.Fields.B24ListElement.PROP_NAME_TICKET_EVENT_ID] = s.Data.Ticket.Event.EventId;
                    d[Const.Fields.B24ListElement.PROP_NAME_DATE_PASSAGE] = s.Data.Date;
                });
        }

        private void CreateMapForReservation()
        {
            CreateMap<Transaction<Reservation>, B24ListElement>()
                .ForMember(x => x.BlockId, x => x.MapFrom(z => Const.UniversalLists.BLOCK_ID_SEATS))
                .ForMember(x => x.ElementCode, x => x.MapFrom(z => Guid.NewGuid().ToString()))
                .ForMember(x => x.Name, x => x.MapFrom(z => z.Data.Event.EventName))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_EVENT_ID],
                //    x => x.MapFrom(z => z.Data.Event.EventId))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_SECTOR],
                //    x => x.MapFrom(z => z.Data.Seat.SectorName))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_ROW],
                //    x => x.MapFrom(z => z.Data.Seat.Row))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_NUM],
                //    x => x.MapFrom(z => z.Data.Seat.Num))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_ARENA_NAME],
                //    x => x.MapFrom(z => z.Data.Event.ArenaName))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEATS_BOOKING],
                //    x => x.MapFrom(z => Const.UniversalLists.SEATS_BOOKING_BUY_FIELD_N))
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Id, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_EVENT_ID] = s.Data.Event.EventId;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_SECTOR] = s.Data.Seat.SectorName;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_ROW] = s.Data.Seat.Row;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_NUM] = s.Data.Seat.Num;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_ARENA_NAME] = s.Data.Event.ArenaName;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEATS_BOOKING] = Const.UniversalLists.SEATS_BOOKING_BUY_FIELD_N;
                });
        }

        private void CreateMapForSeats()
        {
            CreateMap<Ticket, B24ListElement>()
                .ForMember(x => x.BlockId, x => x.MapFrom(z => Const.UniversalLists.BLOCK_ID_SEATS))
                .ForMember(x => x.ElementCode, x => x.MapFrom(z => Guid.NewGuid().ToString()))
                .ForMember(x => x.Name, x => x.MapFrom(z => z.Event.EventName))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_TICKET_EVENT_ID],
                //    x => x.MapFrom(z => z.Event.EventId))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_SECTOR],
                //    x => x.MapFrom(z => z.Seat.SectorName))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_ROW],
                //    x => x.MapFrom(z => z.Seat.Row))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_NUM],
                //    x => x.MapFrom(z => z.Seat.Num))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_ARENA_NAME],
                //    x => x.MapFrom(z => z.Event.ArenaName))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEATS_BOOKING],
                //    x =>
                //    {
                //        x.PreCondition(z => z.BookingId > 0);
                //        x.MapFrom(z => Const.UniversalLists.SEATS_BOOKING_BUY_FIELD_Y);
                //    })
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Id, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_EVENT_ID] = s.Event.EventId;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_SECTOR] = s.Seat.SectorName;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_ROW] = s.Seat.Row;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_NUM] = s.Seat.Num;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_ARENA_NAME] = s.Event.ArenaName;
                    d[Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEATS_BOOKING] = Const.UniversalLists.SEATS_BOOKING_BUY_FIELD_N;
                });
        }

        private void CreateMapForTimepadOrderWebhook()
        {
            CreateMap<TimepadOrderWebhook, B24ListElement>()
                .ForMember(x => x.BlockId, x => x.MapFrom(z => Const.UniversalLists.BLOCK_ID_TIMEPAD_TICKETS))
                .ForMember(x => x.ElementCode, x => x.MapFrom(z => Guid.NewGuid().ToString()))
                .ForMember(x => x.Name, x => x.MapFrom(z => z.Event.Name))
                //.ForMember(x => x.Fields[Const.Fields.B24ListElement.PROP_NAME_TIMEPAD_ID],
                //    x => x.MapFrom(z => z.Id))
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Id, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.B24ListElement.PROP_NAME_TIMEPAD_ID] = s.Id;
                });
        }

        private void CreateMapForEvotorStore()
        {
            CreateMap<EvotorStore, B24ListElement>()
                .ForMember(x => x.BlockId, x => x.MapFrom(z => Const.UniversalLists.BITRIX_LIST_EVOTOR_STORE))
                .ForMember(x => x.ElementCode, x => x.MapFrom(z => z.Uuid.ToString()))
                .ForMember(x => x.Name, x => x.MapFrom(z => z.Name))
                //.ForMember(x => x.Fields[Const.Fields.EvotorStores.UUID],
                //    x => x.MapFrom(z => z.Uuid))
                //.ForMember(x => x.Fields[Const.Fields.EvotorStores.ADDRESS],
                //    x => x.MapFrom(z => z.Address))
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Id, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.EvotorStores.UUID] = s.Uuid;
                    d[Const.Fields.EvotorStores.ADDRESS] = s.Address;
                });
        }

        private void CreateMapForEvotorDevice()
        {
            CreateMap<EvotorDevice, B24ListElement>()
                .ForMember(x => x.BlockId, x => x.MapFrom(z => Const.UniversalLists.BITRIX_LIST_EVOTOR_DEVICE))
                .ForMember(x => x.ElementCode, x => x.MapFrom(z => z.Uuid.ToString()))
                .ForMember(x => x.Name, x => x.MapFrom(z => z.Name))
                //.ForMember(x => x.Fields[Const.Fields.EvotorStores.UUID],
                //    x => x.MapFrom(z => z.Uuid))
                .ForMember(x => x.Fields, x => x.Ignore())
                .ForMember(x => x.Id, x => x.Ignore())
                .AfterMap((s, d) =>
                {
                    d[Const.Fields.EvotorDevices.UUID] = s.Uuid;
                });
        }
    }
}
