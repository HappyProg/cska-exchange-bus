﻿using AutoMapper;
using CSKA.Queue.AutoMapper.B24.Profiles;
using Microsoft.Extensions.DependencyInjection;

namespace CSKA.Queue.AutoMapper
{
    public static class AutomapperExtension
    {
        public static IServiceCollection AddAutomapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(B24CompanyProfile), 
                typeof(B24ContactProfile), typeof(B24ListElementProfile),
                typeof(DealProfile), typeof(StorageProfile));

            return services;
        }
    }
}
