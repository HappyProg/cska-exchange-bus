﻿namespace Evotor.API
{
    public enum ReceiptItemType
    {
        NORMAL,
        ALCOHOL_MARKED,
        ALCOHOL_NOT_MARKED,
        SERVICE
    }
}
