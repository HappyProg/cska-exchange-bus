﻿namespace Evotor.API
{
    public enum PaymentSource
    {
        PAY_CASH,
        PAY_CARD,
        OTHER
    }
}
