﻿namespace Evotor.API
{
    public enum DocumentType
    {
        SELL,
        PAYBACK,
        CASH_INCOME,
        CASH_OUTCOME,
        OPEN_SESSION,
        FPRINT,
        CLOSE_SESSION,
        INVENTORY,
        ACCEPT,
        RETURN,
        WRITE_OFF,
        REVALUATION,
        OPEN_TARE
    }
}
