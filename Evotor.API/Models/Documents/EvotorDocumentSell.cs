﻿using Newtonsoft.Json;
using System;

namespace Evotor.API
{
    public partial class EvotorDocumentSell
    {
        [JsonProperty("positions")]
        public DocumentSellPosition[] Positions { get; set; }

        [JsonProperty("doc_discounts")]
        public object[] DocDiscounts { get; set; }

        [JsonProperty("payments")]
        public Payment[] Payments { get; set; }

        [JsonProperty("print_groups")]
        public PrintGroup[] PrintGroups { get; set; }

        [JsonProperty("pos_print_results")]
        public PosPrintResult[] PosPrintResults { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("result_sum")]
        public decimal ResultSum { get; set; }

        [JsonProperty("base_document_id")]
        public Guid? BaseDocumentId { get; set; }

        [JsonProperty("base_document_number")]
        public long? BaseDocumentNumber { get; set; }
    }

    public partial class Payment
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("sum")]
        public long Sum { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("parts")]
        public Part[] Parts { get; set; }

        [JsonProperty("app_info")]
        public AppInfo AppInfo { get; set; }
    }

    public partial class AppInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class PosPrintResult
    {
        [JsonProperty("receipt_number")]
        public long ReceiptNumber { get; set; }

        [JsonProperty("document_number")]
        public long DocumentNumber { get; set; }

        [JsonProperty("session_number")]
        public long SessionNumber { get; set; }

        [JsonProperty("receipt_date")]
        public string ReceiptDate { get; set; }

        [JsonProperty("receipt_time")]
        public string ReceiptTime { get; set; }

        [JsonProperty("fiscal_sign_doc_number")]
        public string FiscalSignDocNumber { get; set; }

        [JsonProperty("fiscal_document_number")]
        public string FiscalDocumentNumber { get; set; }

        [JsonProperty("fn_serial_number")]
        public string FnSerialNumber { get; set; }

        [JsonProperty("kkt_serial_number")]
        public string KktSerialNumber { get; set; }

        [JsonProperty("kkt_reg_number")]
        public string KktRegNumber { get; set; }

        [JsonProperty("print_group_id")]
        public Guid PrintGroupId { get; set; }

        [JsonProperty("check_sum")]
        public long CheckSum { get; set; }
    }

    public partial class PrintGroup
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
