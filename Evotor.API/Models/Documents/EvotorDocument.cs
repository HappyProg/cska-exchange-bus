﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using CSKA.Queue.Domain.Core.Entities;

namespace Evotor.API
{
    public class EvotorDocumentList<T> : BaseProviderObject
    {
        public EvotorDocumentList()
        {
            Items = new List<EvotorDocument<T>>();
        }

        [JsonProperty("items")]
        public List<EvotorDocument<T>> Items { get; set; }
    }
    public partial class EvotorDocument<T> : BaseProviderObject
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("extras")]
        public EvotorExtras Extras { get; set; }

        [JsonProperty("number")]
        public long Number { get; set; }

        [JsonProperty("close_date")]
        public DateTime CloseDate { get; set; }

        [JsonProperty("time_zone_offset")]
        public long TimeZoneOffset { get; set; }

        [JsonProperty("session_id")]
        public Guid SessionId { get; set; }

        [JsonProperty("session_number")]
        public long SessionNumber { get; set; }

        [JsonProperty("close_user_id")]
        public string CloseUserId { get; set; }

        [JsonProperty("device_id")]
        public Guid DeviceId { get; set; }

        [JsonProperty("store_id")]
        public Guid StoreId { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("body")]
        public T Body { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }

    public partial class Part
    {
        [JsonProperty("print_group_id")]
        public Guid PrintGroupId { get; set; }

        [JsonProperty("part_sum")]
        public decimal PartSum { get; set; }

        [JsonProperty("change")]
        public decimal Change { get; set; }
    }

    public partial class SettlementMethod
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }




}
