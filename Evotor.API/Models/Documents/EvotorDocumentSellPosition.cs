﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel;

namespace Evotor.API
{
    public partial class DocumentSellPosition
    {
        [JsonProperty("product_id")]
        public string ProductId { get; set; }

        [JsonProperty("quantity")]
        public decimal Quantity { get; set; }

        [JsonProperty("initial_quantity")]
        public decimal InitialQuantity { get; set; }

        [JsonProperty("product_type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ReceiptItemType ProductType { get; set; }

        [JsonProperty("alcohol_by_volume")]
        public decimal AlcoholByVolume { get; set; }

        [JsonProperty("alcohol_product_kind_code")]
        public decimal AlcoholProductKindCode { get; set; }

        [JsonProperty("tare_volume")]
        public decimal TareVolume { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("product_name")]
        public string ProductName { get; set; }

        [JsonProperty("measure_name")]
        public string MeasureName { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("uuid")]
        public Guid Uuid { get; set; }

        [JsonProperty("extra_keys")]
        public object[] ExtraKeys { get; set; }

        [JsonProperty("sub_positions")]
        public object[] SubPositions { get; set; }

        [JsonProperty("measure_precision")]
        public decimal MeasurePrecision { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("cost_price")]
        public decimal CostPrice { get; set; }

        [JsonProperty("result_price")]
        public decimal ResultPrice { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("tax")]
        public Tax Tax { get; set; }

        [JsonProperty("result_sum")]
        public decimal ResultSum { get; set; }

        [JsonProperty("print_group_id")]
        public Guid PrintGroupId { get; set; }

        [JsonProperty("settlement_method")]
        public SettlementMethod SettlementMethod { get; set; }
        [JsonProperty("position_discount")]
        public PositonDiscount PositonDiscount { get; set; }
        [JsonProperty("doc_distributed_discount")]
        public DocDistributedDiscount DocDistributedDiscount { get; set; }
    }

    public class DocDistributedDiscount
    {
        [JsonProperty("discount_sum")]
        public decimal DiscountSum { get; set; }
        [JsonProperty("discount_percent")]
        public decimal DiscountPercent { get; set; }
        [JsonProperty("discount_type")]
        public string DiscountType { get; set; }
    }

    public class PositonDiscount
    {
        [JsonProperty("discount_sum")]
        public decimal DiscountSum { get; set; }
        [JsonProperty("discount_percent")]
        public decimal DiscountPercent { get; set; }
        [JsonProperty("discount_type")]
        public string DiscountType { get; set; }
        [JsonProperty("discount_price")]
        public decimal DiscountPrice { get; set; }
    }

    public partial class Tax
    {
        [JsonProperty("type", DefaultValueHandling = DefaultValueHandling.Populate)]
        [JsonConverter(typeof(StringEnumConverter))]
        [DefaultValue(VatType.Unknown)]
        public VatType Type { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("result_sum")]
        public decimal ResultSum { get; set; }
    }
}
