﻿using System;
using CSKA.Queue.Domain.Core.Entities;

namespace Evotor.API
{

    public partial class EvotorReceipt : BaseProviderObject
    {
        public Guid Id { get; set; }
        public long Timestamp { get; set; }
        public string UserId { get; set; }
        public string Type { get; set; }
        public long Version { get; set; }
        public EvotorReceiptData Data { get; set; }
    }

    public partial class EvotorReceiptData
    {
        public Guid Id { get; set; }
        public Guid DeviceId { get; set; }
        public Guid StoreId { get; set; }
        public DateTimeOffset DateTime { get; set; }
        public ReceiptType Type { get; set; }
        public string ShiftId { get; set; }
        public string EmployeeId { get; set; }
        public PaymentSource PaymentSource { get; set; }
        /// <summary>
        /// Флаг, указывающий на фискальность документа
        /// </summary>
        public bool InfoCheck { get; set; }
        public bool Egais { get; set; }
        public EvotorReceiptItem[] Items { get; set; }
        public decimal TotalTax { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalAmount { get; set; }
        public EvotorExtras Extras { get; set; }
    }
    public partial class EvotorReceiptItem : BaseProviderObject
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ReceiptItemType ItemType { get; set; }
        /// <summary>
        /// valid values:
        /// "" "шт" "кг" "л" "м" "км" "м2" "м3" "компл" "упак" "ед" "дроб"
        /// </summary>
        public string MeasureName { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }
        public decimal SumPrice { get; set; }
        public decimal Tax { get; set; }
        public decimal TaxPercent { get; set; }
        public decimal Discount { get; set; }
    }
}
