﻿using System;
using CSKA.Queue.Domain.Core.Entities;

namespace Evotor.API
{
    public class EvotorStore : BaseProviderObject
    {
        public Guid Uuid { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
    }
}
