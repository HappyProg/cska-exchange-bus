﻿using System;
using CSKA.Queue.Domain.Core.Entities;

namespace Evotor.API
{
    public class EvotorDevice : BaseProviderObject
    {
        public Guid Uuid { get; set; }
        public string Name { get; set; }
        public Guid StoreUuid { get; set; }
        public int TimezoneOffset { get; set; }
    }
}
