﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Evotor.API.Helpers;
using Microsoft.Extensions.Logging;

namespace Evotor.API
{
    public sealed class EvotorApiConnector : IEvotorApiConnector
    {
        private readonly HttpClient _client;

        private readonly ILogger<EvotorApiConnector> _logger;

        public EvotorApiConnector(HttpClient client, ILogger<EvotorApiConnector> logger)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _logger = logger;
        }

        public async Task<EvotorDocument<T>> GetDocumentAsync<T>(Guid storeId, Guid docId)
        {
            var response = await _client
                .GetAsync($"stores/{storeId.ToString().ToUpper()}/documents/{docId.ToString().ToUpper()}")
                .ConfigureAwait(false);

            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<EvotorDocument<T>>(content);
        }

        public async Task<IEnumerable<EvotorStore>> GetStoresV1Async()
        {
            var query = "/api/v1/inventories/stores/search";
            var content = await GetContentAsync(query).ConfigureAwait(false);
            var stores = JsonConvert.DeserializeObject<List<EvotorStore>>(content);
            return stores;
        }

        public async Task<IEnumerable<EvotorDevice>> GetDevicesV1Async()
        {
            var query = "/api/v1/inventories/devices/search";
            var content = await GetContentAsync(query).ConfigureAwait(false);
            var devices = JsonConvert.DeserializeObject<List<EvotorDevice>>(content);
            return devices;
        }

        public async Task<IEnumerable<EvotorStore>> GetStoresAsync(string cursor = null)
        {
            List<EvotorStore> stores = new List<EvotorStore>();
            await FillStoresAsync(stores, cursor).ConfigureAwait(false);
            return stores;
        }

        private async Task FillStoresAsync(List<EvotorStore> stores,
            string cursor = null)
        {
            var query = cursor == null ? "stores" : $"stores?cursor={cursor}";
            var content = await GetContentAsync(query).ConfigureAwait(false);
            stores.AddRange(JsonConvert.DeserializeObject<List<EvotorStore>>(content));
            cursor = PaginationHelper.GetPaginationCursor(content);
            if (!string.IsNullOrEmpty(cursor))
            {
                await FillStoresAsync(stores, cursor).ConfigureAwait(false);
            }
        }

        private async Task<string> GetContentAsync(string query)
        {
            var response = await _client
                .GetAsync(query).ConfigureAwait(false);
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return content;
        }

        public async Task<EvotorDocumentList<T>> GetDocumentsAsync<T>(DocumentType type, Guid storeId, string cursor = null, DateTime? dateFrom = null,
            DateTime? dateTo = null)
        {
            EvotorDocumentList<T> documents = new EvotorDocumentList<T>();
            await FillDocumentsAsync(type, documents, storeId, cursor, dateFrom, dateTo).ConfigureAwait(false);
            return documents;
        }

        private async Task FillDocumentsAsync<T>(DocumentType type, EvotorDocumentList<T> documents,
             Guid storeId, string cursor = null, DateTime? dateFrom = null,
            DateTime? dateTo = null)
        {
            var typeName = Enum.GetName(typeof(DocumentType), type);
            var query = cursor == null ? $"stores/{storeId.ToString().ToUpper()}/documents?type={typeName}" 
                                            : $"stores/{storeId.ToString().ToUpper()}/documents?cursor={cursor}&type={typeName}";
            var partQueryWithDate = CreatePartQueryWithDate(dateFrom, dateTo);
            query += partQueryWithDate;
            var content = await GetContentAsync(query).ConfigureAwait(false);
            var receivedDocuments = JsonConvert.DeserializeObject<EvotorDocumentList<T>>(content);
            documents.Items.AddRange(receivedDocuments.Items);
            cursor = PaginationHelper.GetPaginationCursor(content);
            if (!string.IsNullOrEmpty(cursor))
            {
                await FillDocumentsAsync(type, documents, storeId, cursor, dateFrom, dateTo).ConfigureAwait(false);
            }
        }

        private string CreatePartQueryWithDate(DateTime? dateFrom = null,
            DateTime? dateTo = null)
        {
            var msFrom = DateHelper.ConvertToMilliseconds(dateFrom);
            var msTo = DateHelper.ConvertToMilliseconds(dateTo);
            if (dateFrom != null && dateTo != null)
            {
                return $"&since={msFrom}&until={msTo}";
            }
            if (dateFrom != null)
            {
                return $"&since={msFrom}";
            }
            return dateTo == null ? string.Empty : $"&until={msTo}";
        }
    }
}
