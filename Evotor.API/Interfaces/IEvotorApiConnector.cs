﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Evotor.API
{
    public interface IEvotorApiConnector
    {
        Task<EvotorDocument<T>> GetDocumentAsync<T>(Guid storeId, Guid docId);

        Task<IEnumerable<EvotorStore>> GetStoresAsync(string cursor = null);

        Task<EvotorDocumentList<T>> GetDocumentsAsync<T>(DocumentType type, Guid storeId, string cursor = null,
            DateTime? dateFrom = null, DateTime? dateTo = null);

        Task<IEnumerable<EvotorStore>> GetStoresV1Async();

        Task<IEnumerable<EvotorDevice>> GetDevicesV1Async();

    }
}