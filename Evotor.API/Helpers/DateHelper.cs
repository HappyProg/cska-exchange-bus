﻿using System;

namespace Evotor.API.Helpers
{
    public static class DateHelper
    {
        public static long ConvertToMilliseconds(DateTime? date)
        {
            
            if (date is null)
            {
                return default;
            }

            var utc = new DateTimeOffset(date.Value);

            return utc.ToUnixTimeMilliseconds();

            //if (date != null)
            //{
            //    return (long)(date.Value - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;
            //}
            //return default;
        }
    }
}
