﻿using Newtonsoft.Json.Linq;

namespace Evotor.API.Helpers
{
    public static class PaginationHelper
    {
        private const string PAGINATION_OBJECT_NAME = "paging";

        private const string NEX_CURSOR_KEY_NAME = "next_cursor";

        public static string GetPaginationCursor(string response)
        {
            var paging = JObject.Parse(response).SelectToken(PAGINATION_OBJECT_NAME);
            if (paging == null) return string.Empty;
            return paging.HasValues ? paging.Value<string>(NEX_CURSOR_KEY_NAME) : string.Empty;
        }
    }
}
