﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CSKA.Queue.Data.Migrations
{
    public partial class seedUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_RolesAccounts_RoleId",
                schema: "public",
                table: "Accounts");

            migrationBuilder.AlterColumn<Guid>(
                name: "RoleId",
                schema: "public",
                table: "Accounts",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.InsertData(
                schema: "public",
                table: "RolesAccounts",
                columns: new[] { "Id", "Role" },
                values: new object[,]
                {
                    { new Guid("8c38fc8b-24fb-4644-8494-d15fffd23b27"), 0 },
                    { new Guid("8c38fc8b-24fb-4644-8494-d15fffd23b23"), 1 }
                });

            migrationBuilder.InsertData(
                schema: "public",
                table: "Accounts",
                columns: new[] { "Id", "Login", "Password", "RoleId" },
                values: new object[] { new Guid("8c38fc8b-24fb-4644-8494-d15fffd21bfb"), "user@h0oKs", "AQAAAAEAACcQAAAAEBhXc/pohLflN7YWnn+aX/w1Fowsj/yk7vi0S3JwtWDQeza1cQNCh/Xmhz8ShASs5Q==", new Guid("8c38fc8b-24fb-4644-8494-d15fffd23b23") });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_RolesAccounts_RoleId",
                schema: "public",
                table: "Accounts",
                column: "RoleId",
                principalSchema: "public",
                principalTable: "RolesAccounts",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_RolesAccounts_RoleId",
                schema: "public",
                table: "Accounts");

            migrationBuilder.DeleteData(
                schema: "public",
                table: "Accounts",
                keyColumn: "Id",
                keyValue: new Guid("8c38fc8b-24fb-4644-8494-d15fffd21bfb"));

            migrationBuilder.DeleteData(
                schema: "public",
                table: "RolesAccounts",
                keyColumn: "Id",
                keyValue: new Guid("8c38fc8b-24fb-4644-8494-d15fffd23b27"));

            migrationBuilder.DeleteData(
                schema: "public",
                table: "RolesAccounts",
                keyColumn: "Id",
                keyValue: new Guid("8c38fc8b-24fb-4644-8494-d15fffd23b23"));

            migrationBuilder.AlterColumn<Guid>(
                name: "RoleId",
                schema: "public",
                table: "Accounts",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_RolesAccounts_RoleId",
                schema: "public",
                table: "Accounts",
                column: "RoleId",
                principalSchema: "public",
                principalTable: "RolesAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
