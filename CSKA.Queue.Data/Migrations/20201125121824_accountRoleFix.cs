﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CSKA.Queue.Data.Migrations
{
    public partial class accountRoleFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RolesAccounts_Accounts_AccountId",
                schema: "public",
                table: "RolesAccounts");

            migrationBuilder.DropIndex(
                name: "IX_RolesAccounts_AccountId",
                schema: "public",
                table: "RolesAccounts");

            migrationBuilder.DropColumn(
                name: "AccountId",
                schema: "public",
                table: "RolesAccounts");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                schema: "public",
                table: "Accounts",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Login",
                schema: "public",
                table: "Accounts",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RoleId",
                schema: "public",
                table: "Accounts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_RoleId",
                schema: "public",
                table: "Accounts",
                column: "RoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_RolesAccounts_RoleId",
                schema: "public",
                table: "Accounts",
                column: "RoleId",
                principalSchema: "public",
                principalTable: "RolesAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_RolesAccounts_RoleId",
                schema: "public",
                table: "Accounts");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_RoleId",
                schema: "public",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "RoleId",
                schema: "public",
                table: "Accounts");

            migrationBuilder.AddColumn<Guid>(
                name: "AccountId",
                schema: "public",
                table: "RolesAccounts",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                schema: "public",
                table: "Accounts",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Login",
                schema: "public",
                table: "Accounts",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_RolesAccounts_AccountId",
                schema: "public",
                table: "RolesAccounts",
                column: "AccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_RolesAccounts_Accounts_AccountId",
                schema: "public",
                table: "RolesAccounts",
                column: "AccountId",
                principalSchema: "public",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
