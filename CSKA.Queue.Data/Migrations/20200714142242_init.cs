﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CSKA.Queue.Data.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "ProviderStatuses",
                schema: "public",
                columns: table => new
                {
                    Key = table.Column<string>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProviderStatuses", x => x.Key);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProviderStatuses",
                schema: "public");
        }
    }
}
