﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace CSKA.Queue.Data
{
    internal class StatusRepository : IStatusRepository
    {
        private readonly ApplicationDbContext _database;

        public StatusRepository(ApplicationDbContext database)
        {
            _database = database ?? throw new ArgumentNullException(nameof(database));
        }

        public async Task<string> GetAsync(string key)
        {
            var result = await _database
                .ProviderStatuses
                .AsNoTracking()
                .SingleOrDefaultAsync(x => x.Key.ToLower() == key.ToLower())
                .ConfigureAwait(false);

            return result?.Status;
        }

        public async Task Delete(string key)
        {
            var entity = await _database
                .ProviderStatuses
                .SingleOrDefaultAsync(x => x.Key.ToLower() == key.ToLower())
                .ConfigureAwait(false);

            if (entity != null)
            {
                _database.ProviderStatuses.Remove(entity);
                await _database.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public async Task SaveAsync(string key, string status)
        {
            var entity = await _database
                .ProviderStatuses
                .SingleOrDefaultAsync(x => x.Key.ToLower() == key.ToLower())
                .ConfigureAwait(false);

            var dateUpdate = DateTime.Now;
            if (entity != null)
            {
                entity.Status = status;
                entity.LastUpdate = dateUpdate;
            }
            else
            {
                entity = new ProviderStatus
                {
                    Key = key,
                    Status = status,
                    LastUpdate = dateUpdate
                };

                await _database.AddAsync(entity);
            }

            await _database.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
