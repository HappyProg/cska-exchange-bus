﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Threading.Tasks;

namespace CSKA.Queue.Data
{
    public interface IStatusRepository
    {
        Task SaveAsync(string key, string status);
        Task<string> GetAsync(string key);
        Task Delete(string key);
    }
}
