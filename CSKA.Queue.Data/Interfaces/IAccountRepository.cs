﻿using System.Threading.Tasks;
using CSKA.Queue.Data.Enums;

namespace CSKA.Queue.Data.Interfaces
{
    public interface IAccountRepository
    {
        Task<Account> GetAccountAsync(string login);

        Task SaveAccountAsync(string login, string password, Role role);

        Task DeleteAccountAsync(string login);
    }
}
