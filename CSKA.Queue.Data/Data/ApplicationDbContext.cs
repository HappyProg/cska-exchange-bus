﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CSKA.Queue.Data
{
    internal class ApplicationDbContext : DbContext
    {
        public DbSet<ProviderStatus> ProviderStatuses { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<RoleAccount> RolesAccounts { get; set; }

        public ApplicationDbContext()
        {

        }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
            if (Database.GetPendingMigrations().Any())
            {
                Database.Migrate();
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured == false)
            {
                var connectionString = @"User ID=admin;Password=123;Host=postgres;Port=5432;Database=cska_queue;Pooling=true;";
                optionsBuilder.UseNpgsql(connectionString);
            }

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder.SeedUsers();
            base.OnModelCreating(modelBuilder);
        }
    }
}
