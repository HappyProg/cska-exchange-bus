﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CSKA.Queue.Data.Enums;
using CSKA.Queue.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CSKA.Queue.Data
{
    internal class AccountRepository : IAccountRepository
    {
        private readonly ApplicationDbContext _database;

        public AccountRepository(ApplicationDbContext database)
        {
            _database = database ?? throw new ArgumentNullException(nameof(database));
        }

        public Task<Account> GetAccountAsync(string login)
        {
            return _database
                .Accounts
                .AsNoTracking()
                .Include(i => i.Role)
                .SingleOrDefaultAsync(a => a.Login == login);
        }

        public async Task SaveAccountAsync(string login, string password, Role role)
        {
            var account = await _database
                .Accounts
                .SingleOrDefaultAsync(a => a.Login == login)
                .ConfigureAwait(false);

            var roleEntity = _database.RolesAccounts.Single(x => x.Role == role);

            if (account != null)
            {
                account.Login = login;
                account.Password = PasswordManager.HashPassword(password);
                account.Role = roleEntity;
                _database.Update(account);
            }
            else
            {
                var newAccount = new Account
                {
                    Id = Guid.NewGuid(),
                    Login = login,
                    Password = PasswordManager.HashPassword(password),
                    Role = roleEntity
                };
                await _database.AddAsync(newAccount).ConfigureAwait(false);
            }

            await _database.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAccountAsync(string login)
        {
            var account = await _database
                .Accounts
                .SingleOrDefaultAsync(a => a.Login == login)
                .ConfigureAwait(false);
            if (account != null)
            {
                _database.Remove(account);
                await _database.SaveChangesAsync().ConfigureAwait(false);
            }
        }
    }
}
