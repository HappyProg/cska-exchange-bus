﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CSKA.Queue.Data
{
    internal static class ModelBuilderExtensions
    {
        public static void SeedUsers(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                            .HasOne(u => u.Role)
                            .WithMany(r => r.Accounts)
                            .HasForeignKey(u => u.RoleId)
                            .OnDelete(DeleteBehavior.NoAction);

            var roles = InitRoles();
            var users = InitUsers();
            HasData(roles, modelBuilder);
            HasData(users, modelBuilder);
        }

        private static void HasData<TEntity>(IEnumerable<TEntity> data, ModelBuilder modelBuilder)
            where TEntity : class
        {
            foreach (var d in data)
            {
                modelBuilder.Entity<TEntity>().HasData(d);
            }
        }

        private static IEnumerable<Account> InitUsers()
        {
            var roles = InitRoles();
            var roleUser = roles.First(x => x.Role == Enums.Role.User);

            return new Account[]
            {
                new Account
                {
                    Id = Guid.Parse("8C38FC8B-24FB-4644-8494-D15FFFD21BFB"),
                    Login = "user@h0oKs",
                    Password = PasswordManager.HashPassword("denv1C@userHo0k"),
                    RoleId = roleUser.Id                  
                }
            };
        }

        private static IEnumerable<RoleAccount> InitRoles()
        {
            return new RoleAccount[]
            {
                new RoleAccount
                {
                    Id = Guid.Parse("8C38FC8B-24FB-4644-8494-D15FFFD23B27"),
                    Role = Enums.Role.Administrator
                },
                new RoleAccount
                {
                    Id = Guid.Parse("8C38FC8B-24FB-4644-8494-D15FFFD23B23"),
                    Role = Enums.Role.User
                }
            };
        }
    }
}
