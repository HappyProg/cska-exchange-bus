﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using CSKA.Queue.Data.Interfaces;

namespace CSKA.Queue.Data
{

    public static class CSKADataExtension
    {
        public static IServiceCollection AddAppDatabase(this IServiceCollection services, string connectionString)
        {
            services.AddEntityFrameworkNpgsql().AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });

            services.AddTransient<IStatusRepository, StatusRepository>();

            services.AddTransient<IAccountRepository, AccountRepository>();

            //MigrateAppDatabase(services);
            return services;
        }

        private static void MigrateAppDatabase(this IServiceCollection services)
        {
            var sp = services.BuildServiceProvider();
            var ctx = sp.GetService<ApplicationDbContext>();
            if (ctx.Database.GetPendingMigrations().Any())
            {
                ctx.Database.Migrate();
            }
            
        }
    }
}
