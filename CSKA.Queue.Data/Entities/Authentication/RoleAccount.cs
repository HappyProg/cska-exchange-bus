﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CSKA.Queue.Data.Enums;

namespace CSKA.Queue.Data
{
    public class RoleAccount
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public Role Role { get; set; }
        public ICollection<Account> Accounts { get; set; }
    }
}
