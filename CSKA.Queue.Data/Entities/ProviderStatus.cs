﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSKA.Queue.Data
{
    internal class ProviderStatus
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Key { get; set; }
        public string Status { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
