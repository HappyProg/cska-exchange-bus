﻿using Microsoft.AspNetCore.Identity;

namespace CSKA.Queue.Data
{
    public static class PasswordManager
    {
        public static bool VerifyPassword(string password, string hash)
        {
            var hasher = new PasswordHasher<Account>();
            var verify = hasher.VerifyHashedPassword(null, hash, password);
            return verify == PasswordVerificationResult.Success;
        }

        public static string HashPassword(string password)
        {
            var hasher = new PasswordHasher<Account>();
            return hasher.HashPassword(null, password);
        }
    }
}
