﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CSKA.Queue.Providers.PBI
{
    public class PbiTicketsProvider : ICommandProvider<Transaction<Ticket>>
    {
        private readonly HttpClient _client;

        public PbiTicketsProvider(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public Task SendAsync(Transaction<Ticket> data)
        {
            return Task.CompletedTask;
        }
    }
}
