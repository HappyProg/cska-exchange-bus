﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CSKA.Queue.Providers.PBI
{
    public class PbiPassageProvider : ICommandProvider<Transaction<Passage>>
    {
        private readonly HttpClient _client;

        public PbiPassageProvider(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public Task SendAsync(Transaction<Passage> data)
        {
            return Task.CompletedTask;
        }
    }
}
