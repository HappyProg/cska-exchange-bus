﻿using System;
using System.Collections.Generic;

namespace CSKA.Queue.Infrastructure
{
    public class SmsProviderConfigurationBuilder
    {
        private readonly List<ISmsProviderConfiguration> _cfgList;

        public SmsProviderConfigurationBuilder()
        {
            _cfgList = new List<ISmsProviderConfiguration>();
        }

        public SmsProviderConfigurationBuilder AddConfiguration(Action<ISmsProviderConfiguration> action)
        {
            var cfg = new SmsProviderConfiguration();
            action(cfg);
            _cfgList.Add(cfg);
            return this;
        }

        internal List<ISmsProviderConfiguration> Build()
        {
            return _cfgList;
        }
    }
}
