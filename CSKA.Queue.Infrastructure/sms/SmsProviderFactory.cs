﻿using CSKA.Queue.Domain.Interfaces;
using SkyMedia;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CSKA.Queue.Infrastructure
{
    public class SmsProviderFactory : ISmsProviderFactory
    {
        private static List<ISmsProviderConfiguration> _configList;
        private static SmsProviderConfigurationBuilder _builder;

        static SmsProviderFactory()
        {
            _builder = new SmsProviderConfigurationBuilder();
        }

        public static ISmsProviderFactory Create(Action<SmsProviderConfigurationBuilder> action)
        {
            action(_builder);
            _configList = _builder.Build();
            return new SmsProviderFactory();
        }

        public ISmsProvider CreateProvider(string providerName)
        {
            if (Enum.TryParse(providerName, true, out SmsProvider prov) == false)
            {
                throw new NotSupportedException($"Провайдер {providerName} не поддерживается!");
            }

            return CreateProvider(prov);
        }

        public ISmsProvider CreateProvider(SmsProvider provider)
        {
            var cfg = GetByProvider(provider);
            switch (provider)
            {
                case SmsProvider.SkyMedia:                  
                    return new SkyMediaProvider(cfg.Url, cfg.Username, cfg.Password);
                default:
                    throw new NotSupportedException($"Провайдер {provider} не поддерживается!");
            }
        }

        private ISmsProviderConfiguration GetByProvider(SmsProvider provider)
        {
            var result = _configList.FirstOrDefault(x => x.Provider == provider);
            if (result == null)
            {
                throw new InvalidOperationException($"Не заданы настройки для смс провайдера {provider}");
            }

            return result;
        }
    }
}
