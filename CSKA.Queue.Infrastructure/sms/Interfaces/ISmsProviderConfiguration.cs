﻿namespace CSKA.Queue.Infrastructure
{
    public interface ISmsProviderConfiguration
    {
        string Url { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        SmsProvider Provider { get; set; }
    }
}
