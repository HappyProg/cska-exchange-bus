﻿using CSKA.Queue.Domain.Interfaces;

namespace CSKA.Queue.Infrastructure
{
    public interface ISmsProviderFactory
    {
        ISmsProvider CreateProvider(SmsProvider provider);
        ISmsProvider CreateProvider(string providerName);
    }
}
