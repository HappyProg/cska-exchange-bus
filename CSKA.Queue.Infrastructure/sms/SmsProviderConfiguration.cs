﻿namespace CSKA.Queue.Infrastructure
{
    public class SmsProviderConfiguration : ISmsProviderConfiguration
    {
        public string Url { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Sender { get; set; }
        public SmsProvider Provider { get; set; }
    }
}
