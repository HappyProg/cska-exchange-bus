﻿using System.Runtime.Serialization;

namespace CSKA.Queue.Infrastructure
{
    public enum SmsProvider
    {
        [EnumMember(Value = "SKYMEDIA")]
        SkyMedia
    }
}
