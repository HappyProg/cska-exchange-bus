﻿using System;
using System.Collections.Generic;
using System.Linq;
using Evotor.API;


namespace CSKA.Queue.Infrastructure
{
    public class CreatorReceiptService : ICreatorReceiptService
    {
        public List<EvotorReceipt> CreateReceipts(EvotorDocumentList<EvotorDocumentSell> documents)
        {
            return documents.Items.Select(document => new EvotorReceipt
                {
                    Id = document.Id,
                    Timestamp = new DateTimeOffset(DateTime.Parse(document.CreatedAt)).ToUnixTimeSeconds(),
                    UserId = document.UserId,
                    Type = document.Type,
                    Version = 2,
                    Data = CreateReceiptData(document)
                })
                .ToList();
        }

        private EvotorReceiptData CreateReceiptData(EvotorDocument<EvotorDocumentSell> document)
        {
            var date = new DateTimeOffset(DateTime.Parse(document.CreatedAt),
                TimeSpan.FromMilliseconds(document.TimeZoneOffset));
            return new EvotorReceiptData
            {
                Id = document.Id,
                Type = Enum.Parse<ReceiptType>(document.Type),
                DeviceId = document.DeviceId,
                StoreId = document.StoreId,
                EmployeeId = document.UserId,
                ShiftId = document.SessionNumber.ToString(),
                DateTime = date,
                PaymentSource =
                    document.Body.Payments.FirstOrDefault()?.Type == "ELECTRON"
                        ? PaymentSource.PAY_CARD
                        : PaymentSource.PAY_CASH,
                InfoCheck = !string.IsNullOrEmpty(document.Body.PosPrintResults.FirstOrDefault()
                    ?.FiscalDocumentNumber),
                TotalTax = document.Body.Positions.Sum(p => p.Tax.ResultSum),
                TotalAmount = document.Body.ResultSum,
                TotalDiscount = document.Body.Sum - document.Body.ResultSum,
                Extras = document.Extras,
                Items = CreateReceiptItems(document.Body)
            };
        }

        private EvotorReceiptItem[] CreateReceiptItems(EvotorDocumentSell documentSell)
        {
            var positions = documentSell.Positions;

            return positions.Select(position => new EvotorReceiptItem
            {
                Id = position.ProductId ?? position.Id.ToString(),
                Name = position.ProductName,
                ItemType = position.ProductType,
                MeasureName = position.MeasureName,
                Quantity = position.Quantity,
                CostPrice = position.CostPrice,
                Price = position.Price,
                SumPrice = position.ResultPrice,
                Tax = position.Tax?.ResultSum ?? 0,              
                TaxPercent = position.Tax == null || position.ResultSum <= 0
                    ? 0
                    : Math.Round(position.Tax.ResultSum / (position.ResultSum / 100), 2),
                Discount = position.Sum - position.ResultSum
            }).ToArray();
        }
    }
}
