﻿using System;
using System.Collections.Generic;
using Evotor.API;

namespace CSKA.Queue.Infrastructure
{
    public interface ICreatorReceiptService
    {
        List<EvotorReceipt> CreateReceipts(EvotorDocumentList<EvotorDocumentSell> documents);
    }
}
