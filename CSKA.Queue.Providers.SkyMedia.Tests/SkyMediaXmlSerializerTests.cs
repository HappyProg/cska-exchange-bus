﻿using SkyMedia.Entities;
using SkyMedia.Serializers;
using Xunit;

namespace CSKA.Queue.Providers.SkyMedia.Tests
{
    public class SkyMediaXmlSerializerTests
    {
        private const string ERROR_TEXT = "Error text.";
        private const string INFORMATION_TEXT = "send";
        private static readonly string _errorXml = $"<?xml version=\"1.0\" encoding=\"utf-8\"?><response><error>{ERROR_TEXT}</error></response>";
        private static readonly string _sendSmsOutputXml = $"<?xml version=\"1.0\" encoding=\"utf-8\" ?><response><information number_sms=\"1\" id_sms=\"1\" parts=\"2\">{INFORMATION_TEXT}</information><information number_sms=\"2\" id_sms=\"2\" parts=\"2\">{INFORMATION_TEXT}</information><information number_sms=\"3\" id_sms=\"3\" parts=\"2\">{INFORMATION_TEXT}</information></response>";

        [Fact]
        public void DeserializeReturnsCorrectErrorEntity()
        {
            // Act
            var entity = SkyMediaXmlSerializer.Deserialize<Error>(_errorXml);

            // Assert
            Assert.Equal(ERROR_TEXT, entity.Text);
        }

        [Fact]
        public void DeserializeReturnsCorrectSendSmsOutputEntity()
        {
            // Act
            var entity = SkyMediaXmlSerializer.Deserialize<SendSmsOutput>(_sendSmsOutputXml);

            // Assert
            var counter = 1;
            foreach (var info in entity.Information)
            {
                Assert.True(info.Text == INFORMATION_TEXT && info.Parts == 2 && info.SmsNumber == counter && info.SmsId == counter);
                counter++;
            }
        }

        [Fact]
        public void DeserializeReturnsNullErrorTextIfResponseDataValid()
        {
            // Act
            var entity = SkyMediaXmlSerializer.Deserialize<Error>(_sendSmsOutputXml);

            // Assert
            Assert.Null(entity.Text);
        }
    }
}
