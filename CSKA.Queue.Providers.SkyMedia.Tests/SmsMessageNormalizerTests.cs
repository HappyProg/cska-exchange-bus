﻿using CSKA.Queue.Domain.Core;
using SkyMedia.Normalizers;
using Xunit;

namespace CSKA.Queue.Providers.SkyMedia.Tests
{
    public class SmsMessageNormalizerTests
    {
        [Fact]
        public void NormalizeProcessInputAsValidPhoneNumber()
        {
            // Arrange
            var normalizer = new SmsMessageNormalizer();
            var expectedPhoneNumber = "71234567890";
            var messagePhoneNumberWithBracketsWhitespaces = new SmsMessage { Receiver = "7 (123) 456 78 90" };
            var messagePhoneNumberWithBracketsWhitespacesPlus = new SmsMessage { Receiver = "+7 (123) 456 78 90" };
            var messagePhoneNumberWithBracketsWhitespacesDashPlus = new SmsMessage { Receiver = "+7 (123) 456-78-90" };
            var messagePhoneNumberWithoutCountryCode = new SmsMessage { Receiver = "(123) 456-78-90" };

            // Act
            normalizer.Normalize(messagePhoneNumberWithBracketsWhitespaces);
            normalizer.Normalize(messagePhoneNumberWithBracketsWhitespacesPlus);
            normalizer.Normalize(messagePhoneNumberWithBracketsWhitespacesDashPlus);
            normalizer.Normalize(messagePhoneNumberWithoutCountryCode);

            // Assert
            Assert.Equal(expectedPhoneNumber, messagePhoneNumberWithBracketsWhitespaces.Receiver);
            Assert.Equal(expectedPhoneNumber, messagePhoneNumberWithBracketsWhitespacesPlus.Receiver);
            Assert.Equal(expectedPhoneNumber, messagePhoneNumberWithBracketsWhitespacesDashPlus.Receiver);
            Assert.Equal(expectedPhoneNumber, messagePhoneNumberWithoutCountryCode.Receiver);
        }

        [Fact]
        public void NormalizeProcessInputAsValidText()
        {
            // Arrange
            var normalizer = new SmsMessageNormalizer();
            var expectedText = "Text, line 1.\nТекст, строка 2.";
            var messageTextWithWindowsNewline = new SmsMessage { Text = "Text, line 1.\r\nТекст, строка 2." };

            // Act
            normalizer.Normalize(messageTextWithWindowsNewline);

            // Assert
            Assert.Equal(expectedText, messageTextWithWindowsNewline.Text);
        }
    }
}
