﻿using SkyMedia.Extensions;
using Xunit;

namespace CSKA.Queue.Providers.SkyMedia.Tests
{
    public class StringExtensionsTests
    {
        [Fact]
        public void IsValidNumericPhoneNumberAcceptsValidValues()
        {
            // Arrange
            var shouldBeValidWithNineDigits = "123456789";
            var shouldBeValidWithFifteenDigits = "123456789000000";

            // Act & Assert
            Assert.True(shouldBeValidWithNineDigits.IsValidNumericPhoneNumber());
            Assert.True(shouldBeValidWithFifteenDigits.IsValidNumericPhoneNumber());
        }

        [Fact]
        public void IsValidNumericPhoneNumberRejectsInvalidValues()
        {
            // Arrange
            var shouldBeInvalidWithSixteenDigits = "1234567890000000";
            var shouldBeInvalidWithAlphanumeric = "123456Qq";
            var shouldBeInvalidWithAlphanumericCyrillic = "123456Пп";
            var shouldBeInvalidWithNonalphanumeric = "123&&**%%";

            // Act & Assert
            Assert.False(shouldBeInvalidWithSixteenDigits.IsValidNumericPhoneNumber());
            Assert.False(shouldBeInvalidWithAlphanumeric.IsValidNumericPhoneNumber());
            Assert.False(shouldBeInvalidWithAlphanumericCyrillic.IsValidNumericPhoneNumber());
            Assert.False(shouldBeInvalidWithNonalphanumeric.IsValidNumericPhoneNumber());
        }

        [Fact]
        public void IsValidAlphaNumericPhoneNumberAcceptsValidValues()
        {
            // Arrange
            var shouldBeValidWithNineDigits = "123456879";
            var shouldBeValidWithNineAlphanumeric = "123456QWe";
            var shouldBeValidWithElevenAlphanumeric = "123456789Qq";
            var shouldBeValidWithElevenAlphanumericCyrillic = "123456789Пп";

            // Act & Assert
            Assert.True(shouldBeValidWithNineDigits.IsValidAlphaNumericPhoneNumber());
            Assert.True(shouldBeValidWithNineAlphanumeric.IsValidAlphaNumericPhoneNumber());
            Assert.True(shouldBeValidWithElevenAlphanumeric.IsValidAlphaNumericPhoneNumber());
            Assert.True(shouldBeValidWithElevenAlphanumericCyrillic.IsValidAlphaNumericPhoneNumber());
        }

        [Fact]
        public void IsValidAlphaNumericPhoneNumberRejectsInvalidValues()
        {
            // Arrange
            var shouldBeInvalidWithNonalphanumeric = "123456Qq%%##";
            var shouldBeInvalidWithThirteenDigits = "1234567890000";

            // Act & Assert
            Assert.False(shouldBeInvalidWithNonalphanumeric.IsValidAlphaNumericPhoneNumber());
            Assert.False(shouldBeInvalidWithThirteenDigits.IsValidAlphaNumericPhoneNumber());
        }
    }
}
