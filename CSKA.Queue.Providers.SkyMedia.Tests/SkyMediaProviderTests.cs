﻿using CSKA.Queue.Domain.Core;
using SkyMedia;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Xunit;

namespace CSKA.Queue.Providers.SkyMedia.Tests
{
    public class SkyMediaProviderTests
    {
        private const string DEFAULT_URL = "https://lcab.sky-ms.ru/";
        private const string DEFAULT_VALID_LOGIN = "cskabasket";
        private const string DEFAULT_VALID_PASSWORD = "CderBG12";
        private const string DEFAULT_VALID_RECEIVER = "79048688030";
        private const string DEFAULT_VALID_SENDER = "cskabasket";
        private const string DEFAULT_VALID_TEXT = "Ваш абонемент продлен на все матчи плей-офф! Подробно: https://vk.cc/ccUs37";
        private const string DELIVERED_MESSAGE_ID = "2015183175";
        private const string UNDELIVERED_MESSAGE_ID = "2015183678";
        private readonly SkyMediaProvider _provider;

        public SkyMediaProviderTests()
        {
            _provider = new SkyMediaProvider(DEFAULT_URL, DEFAULT_VALID_LOGIN, DEFAULT_VALID_PASSWORD);
        }

        [Fact]
        public async Task SendAsyncSendsValidMessage()
        {
            // Arrange
            var message = new SmsMessage
            {
                Receiver = DEFAULT_VALID_RECEIVER,
                Sender = DEFAULT_VALID_SENDER,
                Text = DEFAULT_VALID_TEXT
            };

            // Act
            var exception = await Record.ExceptionAsync(() => _provider.SendAsync(message));

            // Assert
            Assert.Null(exception);
        }

        [Fact]
        public async Task SendAsyncSendsMessagePhoneNumberExpectedSymbols()
        {
            // Arrange
            var message = new SmsMessage
            {
                Receiver = "+7 (914) 175-35-36",
                Sender = DEFAULT_VALID_SENDER,
                Text = DEFAULT_VALID_TEXT
            };

            // Act
            var exception = await Record.ExceptionAsync(() => _provider.SendAsync(message));

            // Assert
            Assert.Null(exception);
        }

        [Fact]
        public async Task SendAsyncSendsMessagePhoneNumberWithoutCountryCode()
        {
            // Arrange
            var message = new SmsMessage
            {
                Receiver = "(914) 175-35-36",
                Sender = DEFAULT_VALID_SENDER,
                Text = DEFAULT_VALID_TEXT
            };

            // Act
            var exception = await Record.ExceptionAsync(() => _provider.SendAsync(message));

            // Assert
            Assert.Null(exception);
        }

        [Fact]
        public async Task SendAsyncSendsMessageTextWithWindowsNewlines()
        {
            // Arrange
            var message = new SmsMessage
            {
                Receiver = DEFAULT_VALID_RECEIVER,
                Sender = DEFAULT_VALID_SENDER,
                Text = "Test, line 1.\r\nТест, строка 2."
            };

            // Act
            var exception = await Record.ExceptionAsync(() => _provider.SendAsync(message));

            // Assert
            Assert.Null(exception);
        }

        [Fact]
        public async Task SendAsyncThrowsExceptionWithInvalidReceiver()
        {
            // Arrange
            var message = new SmsMessage
            {
                Receiver = "invalid_receiver",
                Sender = DEFAULT_VALID_SENDER,
                Text = DEFAULT_VALID_TEXT
            };

            // Act
            var exception = await Record.ExceptionAsync(() => _provider.SendAsync(message));

            // Assert
            Assert.NotNull(exception);
            if (exception != null)
            {
                Assert.True(exception is ValidationException);
            }
        }

        [Fact]
        public async Task SendAsyncThrowsExceptionWithInvalidSender()
        {
            // Arrange
            var message = new SmsMessage
            {
                Receiver = DEFAULT_VALID_RECEIVER,
                Sender = "invalid_sender_*#&",
                Text = DEFAULT_VALID_TEXT
            };

            // Act
            var exception = await Record.ExceptionAsync(() => _provider.SendAsync(message));

            // Assert
            Assert.NotNull(exception);
            if (exception != null)
            {
                Assert.True(exception is ValidationException);
            }
        }

        [Fact]
        public async Task GetMessageStatusAsyncReturnsDelivered()
        {
            // Act
            var status = await _provider.GetMessageStatusAsync(DELIVERED_MESSAGE_ID);

            // Assert
            Assert.Equal(MessageStatus.Delivered, status);
        }

        [Fact]
        public async Task GetMessageStatusAsyncReturnsUndelivered()
        {
            // Act
            var status = await _provider.GetMessageStatusAsync(UNDELIVERED_MESSAGE_ID);

            // Assert
            Assert.Equal(MessageStatus.Undelivered, status);
        }
    }
}
