﻿using System.Linq;
using CSKA.PBI.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace CSKA.PBI.Data.Context
{
    public sealed class StorageContext : DbContext
    {
        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<Seat> Seats { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<Passage> Passages { get; set; }

        public StorageContext()
        {

        }

        public StorageContext(DbContextOptions<StorageContext> options) : base(options)
        {
            if (Database.GetPendingMigrations().Any())
            {
                Database.Migrate();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder.Entity<Seat>().HasIndex(s => new {s.Num, s.Row, s.SectorName, s.TribuneName}).IsUnique();
            modelBuilder.Entity<Ticket>().HasIndex(t => t.Barcode).IsUnique();
            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.Event)
                .WithMany(e => e.Tickets)
                .HasForeignKey(t => t.EventId);
            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.Seat)
                .WithMany(s => s.Tickets)
                .HasForeignKey(t => t.SeatId);
            modelBuilder.Entity<Passage>()
                .HasOne(p => p.Event)
                .WithMany(e => e.Passages)
                .HasForeignKey(p => p.EventId);
            base.OnModelCreating(modelBuilder);
        }
    }
}
