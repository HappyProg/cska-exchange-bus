﻿using System.Threading.Tasks;
using CSKA.PBI.Data.Models;

namespace CSKA.PBI.Data.Repositories
{
    public interface IStorageRepository
    {
        Task AddOrUpdateTicket(Ticket ticket);

        Task<Event> GetEvent(long eventId);

        Task<Seat> GetSeat(Seat seat);

        Task AddOrUpdatePassage(Passage passage);

        Task<Event> GetNearestEvent(double matchProcessingTime);

        Task UpdateEvent(Event @event);
    }
}
