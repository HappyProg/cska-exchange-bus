﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CSKA.PBI.Data.Context;
using CSKA.PBI.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace CSKA.PBI.Data.Repositories
{
    public class StorageRepository : IStorageRepository
    {
        private readonly StorageContext _context;

        public StorageRepository(StorageContext context)
        {
            _context = context;
        }

        public async Task AddOrUpdateTicket(Ticket ticket)
        {
            var currentTicket = await _context.Tickets.FindAsync(ticket.Id);
            if (currentTicket != null)
            {
                _context.Tickets.Update(ticket);
            }
            else
            {
                await _context.Tickets.AddAsync(ticket).ConfigureAwait(false);
            }
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<Event> GetEvent(long eventId)
        {
            var @event = await _context.Events.FindAsync(eventId).ConfigureAwait(false);
            return @event;
        }

        public async Task<Seat> GetSeat(Seat seat)
        {
            return await _context.Seats
                .FirstOrDefaultAsync(s => s.Num == seat.Num 
                                          && s.SectorName == seat.SectorName 
                                          && s.Row == seat.Row 
                                          && s.TribuneName == seat.TribuneName)
                .ConfigureAwait(false);
        }

        public async Task AddOrUpdatePassage(Passage passage)
        {
            var currentPassage = await _context.Passages.FindAsync(passage.Id).ConfigureAwait(false);
            if (currentPassage != null)
            {
                _context.Passages.Update(currentPassage);
            }
            else
            {
                await _context.Passages.AddAsync(passage).ConfigureAwait(false);
            }

            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<Event> GetNearestEvent(double matchProcessingTime)
        {
            var date = DateTimeOffset.Now.Date;
            var nearestEvent = await _context.Events
                .Include(e => e.Tickets)
                .Include(e => e.Passages)
                .OrderBy(e => e.Date)
                .FirstOrDefaultAsync(e => e.Date.AddHours(matchProcessingTime) >= date)
                .ConfigureAwait(false);
            return nearestEvent;
        }

        public async Task UpdateEvent(Event @event)
        {
            _context.Update(@event);
            await _context.SaveChangesAsync();
        }
    }
}
