﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSKA.PBI.Data.Models
{
    public class Seat
    {
        public Seat()
        {
            Tickets = new HashSet<Ticket>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Num { get; set; }

        public string Row { get; set; }

        public string SectorName { get; set; }

        public string TribuneName { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
    }
}
