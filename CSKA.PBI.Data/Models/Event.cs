﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CSKA.Queue.Domain.Core;

namespace CSKA.PBI.Data.Models
{
    public class Event
    {
        public Event()
        {
            Tickets = new HashSet<Ticket>();
            Passages = new HashSet<Passage>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public string Name { get; set; }

        public int SeatsCountFree { get; set; }

        public DateTime Date { get; set; }

        public string ArenaName { get; set; }

        public int SeatsCount { get; set; }

        public EventType EventType { get; set; }

        public ICollection<Ticket> Tickets { get; set; }

        public ICollection<Passage> Passages { get; set; }
    }
}
