﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CSKA.Queue.Domain.Core;

namespace CSKA.PBI.Data.Models
{
    public class Ticket
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public string Barcode { get; set; }

        public decimal Price { get; set; }

        public decimal Discount { get; set; }

        public string DiscountName { get; set; }

        public TicketType TicketType { get; set; }

        public TicketState TicketState { get; set; }

        public int? BookingId { get; set; }

        public long EventId { get; set; }

        public Event Event { get; set; }

        public long SeatId { get; set; }

        public Seat Seat { get; set; }
    }
}
