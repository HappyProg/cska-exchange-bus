﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSKA.PBI.Data.Models
{
    public class Passage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public long EventId { get; set; }

        public Event Event { get; set; }

        public string TicketBarcode { get; set; }
    }
}
