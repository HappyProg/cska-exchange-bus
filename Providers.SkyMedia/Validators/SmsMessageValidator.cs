﻿using CSKA.Queue.Domain.Core;
using SkyMedia.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SkyMedia.Validators
{
    internal class SmsMessageValidator
    {
        private List<ValidationResult> _validationResults;

        public IEnumerable<ValidationResult> Validate(SmsMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            _validationResults = new List<ValidationResult>();
            ValidateText(message);
            ValidateReceiver(message);
            ValidateSender(message);
            return _validationResults;
        }

        private void ValidateText(SmsMessage message)
        {
            if (string.IsNullOrWhiteSpace(message.Text))
            {
                _validationResults.Add(new ValidationResult("Текст сообщения не задан."));
                return;
            }
            if (message.Text.Contains("\r\n"))
            {
                _validationResults.Add(new ValidationResult("Текст сообщения содержит переносы строк в Windows-формате."));
            }
        }

        private void ValidateReceiver(SmsMessage message)
        {
            if (string.IsNullOrWhiteSpace(message.Receiver))
            {
                _validationResults.Add(new ValidationResult("Получатель не задан."));
                return;
            }
            if (!message.Receiver.IsValidNumericPhoneNumber())
            {
                _validationResults.Add(new ValidationResult("Получатель некорректен."));
            }
        }

        private void ValidateSender(SmsMessage message)
        {
            if (string.IsNullOrWhiteSpace(message.Sender))
            {
                _validationResults.Add(new ValidationResult("Отправитель не задан."));
                return;
            }
            if (!message.Sender.IsValidAlphaNumericPhoneNumber())
            {
                _validationResults.Add(new ValidationResult("Отправитель некорректен."));
            }
        }
    }
}
