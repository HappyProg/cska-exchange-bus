﻿using SkyMedia.Entities;
using SkyMedia.Extensions;
using SkyMedia.Serializers;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SkyMedia
{
    internal class SkyMediaClient
    {
        private const string DEFAULT_MEDIA_TYPE = "text/xml";
        private readonly string _url;
        private readonly string _login;
        private readonly string _password;

        private string Url => string.IsNullOrWhiteSpace(_url) ? throw new InvalidOperationException("Адрес сервера не задан.") : _url.AsUrl();
        private string Login => string.IsNullOrWhiteSpace(_login) ? throw new InvalidOperationException("Логин не задан.") : _login;
        private string Password => string.IsNullOrWhiteSpace(_password) ? throw new InvalidOperationException("Пароль не задан.") : _password;

        public SkyMediaClient(string url, string login, string password)
        {
            _url = url;
            _login = login;
            _password = password;
        }

        public async Task<SendSmsOutput> SendSmsAsync(SendSmsInput request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            return await ProcessRequestAsync<SendSmsInput, SendSmsOutput>($"{Url}xml/", request);
        }

        public async Task<GetStateOutput> GetStateAsync(GetStateInput request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            return await ProcessRequestAsync<GetStateInput, GetStateOutput>($"{Url}xml/state.php", request);
        }

        #region private

        private async Task<TResponse> ProcessRequestAsync<TRequest, TResponse>(string url, TRequest request) where TRequest : RequestBase
        {
            AddAuthenticationData(request);
            var response = await HttpBuilder.Create(url)
                .SetMethod(HttpRequestMethod.POST)
                .SetBody(SkyMediaXmlSerializer.Serialize(request))
                .SetMediaType(DEFAULT_MEDIA_TYPE)
                .RequestAsync();
            VerifyResponseIsSuccess(response);
            try
            {
                return SkyMediaXmlSerializer.Deserialize<TResponse>(response.Data);
            }
            catch (Exception ex)
            {
                throw new Exception(response.Data + "\r\n" + ex.Message);
            }
        }

        private void AddAuthenticationData(RequestBase request)
        {
            request.Security = new Security
            {
                Login = new Login { Value = Login },
                Password = new Password { Value = Password }
            };
        }

        private void VerifyResponseIsSuccess(HttpResponse response)
        {
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException($"Сетевой запрос завершен с кодом ошибки {response.StatusCode}.");
            }
            if (string.IsNullOrWhiteSpace(response.Data))
            {
                throw new Exception("Ответ на сетевой запрос не содержит данных.");
            }
            // Если запрос выполнен неудачно, SkyMedia все равно возвращает код 200, но в теле ответа содержится объект типа Error.
            var error = SkyMediaXmlSerializer.Deserialize<Error>(response.Data);
            if (!string.IsNullOrWhiteSpace(error?.Text))
            {
                throw new Exception(error.Text);
            }
        }

        #endregion
    }
}
