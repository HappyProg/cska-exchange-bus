﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SkyMedia
{
    internal class HttpBuilder
    {
        private static readonly HttpClient _http = new HttpClient();
        private readonly HttpRequestMessage _message;
        private readonly string _url;
        private string _body;
        private string _mediaType;
        private Encoding _encoding;
        private MultipartFormDataContent _multipartForm;
        private FormUrlEncodedContent _encodedForm;

        public HttpBuilder(string url)
        {
            _message = string.IsNullOrWhiteSpace(url) ? throw new ArgumentNullException(nameof(url)) : new HttpRequestMessage(HttpMethod.Get, url);
            _url = url;
            _body = string.Empty;
            _mediaType = string.Empty;
            _encoding = Encoding.UTF8;
        }

        public static HttpBuilder Create(string url)
        {
            return new HttpBuilder(url);
        }

        public HttpBuilder SetEncoding(Encoding encoding)
        {
            if (encoding is null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            _encoding = encoding;
            return this;
        }

        public HttpBuilder SetMediaType(string mediaType)
        {
            if (string.IsNullOrWhiteSpace(nameof(mediaType)))
            {
                throw new ArgumentNullException(nameof(mediaType));
            }

            _mediaType = mediaType;
            return this;
        }

        public HttpBuilder SetBody(string body)
        {
            if (string.IsNullOrWhiteSpace(body))
            {
                throw new ArgumentNullException(nameof(body));
            }

            _body = body;
            return this;
        }

        public HttpBuilder SetFormField(string name, string value)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException(nameof(value));
            }

            var content = new StringContent(value);
            AddFormContent(content, name);
            return this;
        }

        public HttpBuilder SetEncodedForm(IEnumerable<KeyValuePair<string, string>> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            _encodedForm = new FormUrlEncodedContent(values);
            return this;
        }

        public HttpBuilder SetMethod(HttpRequestMethod requestMethod)
        {
            _message.Method = requestMethod switch
            {
                HttpRequestMethod.GET => HttpMethod.Get,
                HttpRequestMethod.POST => HttpMethod.Post,
                HttpRequestMethod.PUT => HttpMethod.Put,
                HttpRequestMethod.DELETE => HttpMethod.Delete,
                _ => throw new InvalidOperationException("Для сетевого запроса указан неизвестный метод."),
            };
            return this;
        }

        public HttpBuilder SetHeader(string headerName, string headerValue)
        {
            if (string.IsNullOrWhiteSpace(headerName))
            {
                throw new ArgumentNullException(nameof(headerName));
            }
            if (string.IsNullOrWhiteSpace(headerValue))
            {
                throw new ArgumentNullException(nameof(headerValue));
            }

            TryRemoveHeader(headerName);
            _message.Headers.Add(headerName, headerValue);
            return this;
        }

        public HttpBuilder SetBearerAuthentication(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                throw new ArgumentNullException(nameof(token));
            }

            var authorizationHeaderName = "Authorization";
            TryRemoveHeader(authorizationHeaderName);
            _message.Headers.Add(authorizationHeaderName, $"Bearer {token}");
            return this;
        }

        public async Task<HttpResponse> RequestAsync()
        {
            _message.Content = GetContent();
            HttpResponseMessage response;
            try
            {
                response = await _http.SendAsync(_message);
            }
            catch (TaskCanceledException e)
            {
                throw new HttpRequestException($"Превышен интервал ожидания для запроса к серверу {GetHost(_url)}.", e);
            }
            return await GetResult(response);
        }

        #region private

        private HttpContent GetContent()
        {
            if (_message.Method == HttpMethod.Get)
            {
                return null;
            }
            if (_multipartForm != null)
            {
                return _multipartForm;
            }
            if (_encodedForm != null)
            {
                return _encodedForm;
            }
            return string.IsNullOrWhiteSpace(_mediaType) ?
                new StringContent(content: _body, encoding: _encoding) :
                new StringContent(content: _body, encoding: _encoding, mediaType: _mediaType);
        }

        private void TryRemoveHeader(string headerName)
        {
            var header = _message.Headers.FirstOrDefault(h => h.Key.ToUpper() == headerName.ToUpper());
            if (!string.IsNullOrWhiteSpace(header.Key))
            {
                _http.DefaultRequestHeaders.Remove(header.Key);
            }
        }

        private async Task<HttpResponse> GetResult(HttpResponseMessage response)
        {
            var data = await response
                .Content
                .ReadAsStringAsync();
            return new HttpResponse
            {
                StatusCode = (int)response.StatusCode,
                IsSuccessStatusCode = response.IsSuccessStatusCode,
                Data = data,
                RequestMessage = response.RequestMessage
            };
        }

        private string GetHost(string url)
        {
            return new UriBuilder(url).Uri.Host;
        }

        private void AddFormContent(HttpContent content, string name)
        {
            if (_multipartForm == null)
            {
                _multipartForm = new MultipartFormDataContent();
            }
            _multipartForm.Add(content, name);
        }

        #endregion
    }
}
