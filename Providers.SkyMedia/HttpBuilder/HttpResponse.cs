﻿using System.Net.Http;

namespace SkyMedia
{
    internal class HttpResponse
    {
        public int StatusCode { get; set; }
        public bool IsSuccessStatusCode { get; set; }
        public string Data { get; set; }
        public HttpRequestMessage RequestMessage { get; set; }
    }
}
