﻿namespace SkyMedia
{
    internal enum HttpRequestMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
