﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SkyMedia.Serializers
{
    public static class SkyMediaXmlSerializer
    {
        public static string Serialize<TEntity>(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var serializer = new XmlSerializer(typeof(TEntity));
            using var memoryStream = new MemoryStream();
            using var xmlWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            serializer.Serialize(xmlWriter, entity, new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty }));
            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

        public static TEntity Deserialize<TEntity>(string xml)
        {
            if (string.IsNullOrWhiteSpace(xml))
            {
                throw new ArgumentNullException(nameof(xml));
            }

            var serializer = new XmlSerializer(typeof(TEntity));
            using var stringReader = new StringReader(xml);
            return (TEntity)serializer.Deserialize(stringReader);
        }
    }
}
