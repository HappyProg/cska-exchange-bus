﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using SkyMedia.Entities;
using SkyMedia.Extensions;
using SkyMedia.Normalizers;
using SkyMedia.Validators;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SkyMedia
{
    public class SkyMediaProvider : ISmsProvider
    {
        private const string SUCCESS_MESSAGE_SEND_STATUS = "send";
        private readonly SkyMediaClient _client;
        private readonly SmsMessageNormalizer _normalizer;
        private readonly SmsMessageValidator _validator;

        public SkyMediaProvider(string url, string login, string password)
        {
            _client = new SkyMediaClient(url, login, password);
            _normalizer = new SmsMessageNormalizer();
            _validator = new SmsMessageValidator();
        }

        public async Task<string> SendAsync(CSKA.Queue.Domain.Core.SmsMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            _normalizer.Normalize(message);
            VerifyMessageIsValid(message);
            var input = new SendSmsInput(message);
            var output = await _client.SendSmsAsync(input);
            VerifyMessageWasSend(output);

            return output.Information[0].SmsId.ToString();
        }

        public async Task<MessageStatus> GetMessageStatusAsync(string id)
        {
            if (int.TryParse(id, out int messageId) == false)
            {
                throw new ProviderOperationException($"Некорректный идентификатор для проверки статуса СМС skymedia - \"{id}\"");
            }

            if (messageId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(messageId));
            }

            var input = new GetStateInput(messageId);
            var output = await _client.GetStateAsync(input);
            VerifyReceivedStateNotEmpty(output);
            return output.State.First().SmsMessageState.ToMessageStatus();
        }

        #region private

        private void VerifyMessageIsValid(CSKA.Queue.Domain.Core.SmsMessage message)
        {
            var validationResults = _validator.Validate(message);
            if (validationResults.Any())
            {
                throw new ValidationException(validationResults.First().ErrorMessage);
            }
        }

        private void VerifyMessageWasSend(SendSmsOutput output)
        {
            if (output?.Information == null || output.Information.Length == 0)
            {
                throw new Exception("Статус отправленного SMS-сообщения не получен.");
            }
            if (output.Information[0].Text != SUCCESS_MESSAGE_SEND_STATUS)
            {
                throw new Exception(output.Information[0].Text);
            }
        }

        private void VerifyReceivedStateNotEmpty(GetStateOutput output)
        {
            if (output?.State == null || output.State.Length == 0)
            {
                throw new Exception("Статус отправленного SMS-сообщения не получен.");
            }
        }

        #endregion
    }
}
