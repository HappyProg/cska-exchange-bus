﻿using System;
using System.Text.RegularExpressions;

namespace SkyMedia.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Проверяет, является ли номер корректным цифровым номером (не более 15 символов).
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static bool IsValidNumericPhoneNumber(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return false;
            }
            return Regex.IsMatch(source, @"^\d{1,15}$");
        }

        /// <summary>
        /// Проверяет, является ли номер корректным буквенно-числовым номером (не более 11 символов).
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static bool IsValidAlphaNumericPhoneNumber(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return false;
            }
            return Regex.IsMatch(source, @"^\w{1,11}$");
        }

        public static string AsNumericPhoneNumber(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            var result = Regex.Replace(source, @"\(|\)|\-|\+|\ ", string.Empty);
            return result;
        }

        public static string AsUnixMultiline(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            return Regex.Replace(source, @"\r\n", "\n");
        }

        public static string AsUrl(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentNullException(nameof(source));
            }

            try
            {
                return new UriBuilder(source).Uri.AbsoluteUri;
            }
            catch (Exception ex)
            {
                throw new FormatException($"Значение {source} не является URL.", ex);
            }
        }
    }
}
