﻿using CSKA.Queue.Domain.Core;
using System;

namespace SkyMedia.Extensions
{
    public static class SmsMessageStateExtensions
    {
        public static MessageStatus ToMessageStatus(this SmsMessageState state)
        {
            return state switch
            {
                SmsMessageState.Send => MessageStatus.Send,
                SmsMessageState.NotDelivered => MessageStatus.Undelivered,
                SmsMessageState.Expired => MessageStatus.Undelivered,
                SmsMessageState.Delivered => MessageStatus.Delivered,
                SmsMessageState.PartlyDelivered => MessageStatus.Fail,
                _ => throw new Exception("Неизвестный статус SMS-сообщения."),
            };
        }
    }
}
