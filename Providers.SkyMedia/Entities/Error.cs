﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    [XmlRoot(ElementName = "response")]
    public class Error
    {
        [XmlElement(ElementName = "error")]
        public string Text { get; set; }
    }
}
