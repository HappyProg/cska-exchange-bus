﻿using CSKA.Queue.Domain.Core;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    [XmlRoot(ElementName = "request")]
    public class SendSmsInput : RequestBase
    {
        [XmlElement(ElementName = "message")]
        public SmsMessage[] Messages { get; set; }

        public SendSmsInput() { }

        public SendSmsInput(CSKA.Queue.Domain.Core.SmsMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            var abonents = new List<Abonent> { new Abonent(phone: message.Receiver, smsNumber: 1) };
            var messages = new List<SmsMessage> { new SmsMessage(sender: message.Sender, text: message.Text, abonents.ToArray()) };
            Messages = messages.ToArray();
        }
    }
}
