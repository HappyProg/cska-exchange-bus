﻿using System;
using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    public class Abonent
    {
        /// <summary>
        /// Номер абонента, которому адресовано SMS-сообщение, в международном формате. Например: 79000000001 для России, 380442589632 для Украины и т.д.
        /// </summary>
        [XmlAttribute(AttributeName = "phone")]
        public string Phone { get; set; }

        /// <summary>
        /// Номер SMS-сообщения в пределах отправляемого XML-документа.
        /// </summary>
        [XmlAttribute(AttributeName = "number_sms")]
        public long SmsNumber { get; set; }

        /// <summary>
        /// Необязательный параметр, позволяющий избежать повторной отправки. Если раннее с этого аккаунта уже было отправлено SMS-сообщение с таким номером, то повторная отправка не производится, а возвращается номер ранее отправленного сообщения.
        /// </summary>
        [XmlAttribute(AttributeName = "client_id_sms")]
        public long SmsClientId { get; set; }

        public bool ShouldSerializeSmsClientId() => SmsClientId != 0;

        /// <summary>
        /// Дата и время отправки. Если не задано, то SMS-сообщение отправляется сразу же.
        /// </summary>
        [XmlIgnore]
        public DateTime SendTime { get; set; }

        [XmlAttribute(AttributeName = "time_send")]
        public string SendTimeString { get => SendTime.ToString("yyyy-MM-dd hh:mm"); set => SendTime = DateTime.Parse(value); }

        public bool ShouldSerializeSendTimeString() => SendTime != DateTime.MinValue;

        /// <summary>
        /// Дата и время, после которых не будут делаться попытки доставить SMS-сообщение. Если не задано, то сообщение имеет максимальный срок жизни.
        /// </summary>        
        [XmlIgnore]
        public DateTime ValidityPeriod { get; set; }

        [XmlAttribute(AttributeName = "validity_period")]
        public string ValidityPeriodString { get => ValidityPeriod.ToString("yyyy-MM-dd hh:mm"); set => ValidityPeriod = DateTime.Parse(value); }

        public bool ShouldSerializeValidityPeriodString() => ValidityPeriod != DateTime.MinValue;

        public Abonent() { }

        public Abonent(string phone, int smsNumber)
        {
            Phone = phone;
            SmsNumber = smsNumber;
        }
    }
}
