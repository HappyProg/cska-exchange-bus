﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    [XmlRoot(ElementName = "response")]
    public class SendSmsOutput
    {
        [XmlElement(ElementName = "information")]
        public Information[] Information { get; set; }
    }
}
