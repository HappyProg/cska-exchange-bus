﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    /// <summary>
    /// SMS-сообщение.
    /// </summary>
    public class SmsMessage
    {
        /// <summary>
        /// Тип отправляемого SMS-сообщения.
        /// </summary>
        [XmlAttribute(AttributeName = "type")]
        public SmsMessageType Type { get; set; }

        /// <summary>
        /// Отправитель SMS-сообщения. Это значение будет отображено в поле отправителя SMS-сообщения в телефоне получателя.
        /// </summary>
        [XmlElement(ElementName = "sender")]
        public string Sender { get; set; }

        /// <summary>
        /// Текст SMS-сообщения.
        /// </summary>
        [XmlElement(ElementName = "text")]
        public string Text { get; set; }

        /// <summary>
        /// Транслитерация текста SMS-сообщения с кириллицы на латиницу (необязательный параметр).
        /// </summary>
        [XmlElement(ElementName = "translite")]
        public TranslitMode TranslitMode { get; set; }

        public bool ShouldSerializeTranslitMode() => TranslitMode == TranslitMode.Enabled;

        /// <summary>
        /// Название рассылки.
        /// </summary>
        [XmlElement(ElementName = "name_delivery")]
        public string DeliveryName { get; set; }

        /// <summary>
        /// Получатели SMS-сообщения.
        /// </summary>
        [XmlElement(ElementName = "abonent")]
        public Abonent[] Abonents { get; set; }

        public SmsMessage() { }

        public SmsMessage(string sender, string text, Abonent[] abonents)
        {
            Sender = sender;
            Text = text;
            Abonents = abonents;
        }
    }
}
