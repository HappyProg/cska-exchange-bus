﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    public class Information
    {
        /// <summary>
        /// Номер сообщения, указанный при отправке XML-документа.
        /// </summary>
        [XmlAttribute(AttributeName = "number_sms")]
        public long SmsNumber { get; set; }

        /// <summary>
        /// Номер SMS-сообщения. Используется для проверки статуса сообщения. Если в процессе отправки сообщения произошла ошибка, то значение не передается.
        /// </summary>
        [XmlAttribute(AttributeName = "id_sms")]
        public long SmsId { get; set; }

        public bool ShouldSerializeSmsId() => SmsId != 0;

        /// <summary>
        /// Количество частей SMS-сообщения.
        /// </summary>
        [XmlAttribute(AttributeName = "parts")]
        public long Parts { get; set; }

        /// <summary>
        /// Статус сообщения, если SMS-сообщение было отправлено, или сообщение об ошибке, если в процессе отправки SMS-сообщения произошла ошибка.
        /// </summary>
        [XmlText]
        public string Text { get; set; }
    }
}
