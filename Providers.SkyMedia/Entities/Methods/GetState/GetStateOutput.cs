﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    [XmlRoot(ElementName = "response")]
    public class GetStateOutput
    {
        [XmlElement(ElementName = "state")]
        public State[] State { get; set; }
    }
}
