﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    [XmlRoot(ElementName = "request")]
    public class GetStateInput : RequestBase
    {
        [XmlElement(ElementName = "get_state")]
        public GetState GetState { get; set; }

        public GetStateInput() { }

        public GetStateInput(long messageId)
        {
            if (messageId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(messageId));
            }

            var messagesIds = new List<long> { messageId };
            GetState = new GetState { MessagesIds = messagesIds.ToArray() };
        }
    }
}
