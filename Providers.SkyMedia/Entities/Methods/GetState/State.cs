﻿using System;
using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    public class State
    {
        /// <summary>
        /// Номер SMS-сообщения.
        /// </summary>
        [XmlAttribute(AttributeName = "id_sms")]
        public long SmsId { get; set; }

        /// <summary>
        /// Дата и время изменения статуса.
        /// </summary>
        [XmlIgnore]
        public DateTime StatusChangeTime { get; set; }

        [XmlAttribute(AttributeName = "time")]
        public string StatusChangeTimeString { get => StatusChangeTime.ToString("yyyy-MM-dd hh:mm"); set => StatusChangeTime = string.IsNullOrWhiteSpace(value) ? DateTime.MinValue : DateTime.Parse(value); }

        /// <summary>
        /// Количество частей SMS-сообщения.
        /// </summary>
        [XmlAttribute(AttributeName = "num_parts")]
        public int Parts { get; set; }

        /// <summary>
        /// Цена за одну часть SMS-сообщения.
        /// </summary>
        [XmlAttribute(AttributeName = "price")]
        public double Price { get; set; }

        /// <summary>
        /// Статус сообщения.
        /// </summary>
        [XmlText]
        public SmsMessageState SmsMessageState { get; set; }
    }
}
