﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    public class GetState
    {
        [XmlElement(ElementName = "id_sms")]
        public long[] MessagesIds { get; set; }
    }
}
