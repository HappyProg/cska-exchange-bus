﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    public class Security
    {
        [XmlElement(ElementName = "login")]
        public Login Login { get; set; }

        [XmlElement(ElementName = "password")]
        public Password Password { get; set; }
    }
}
