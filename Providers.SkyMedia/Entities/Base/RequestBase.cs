﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    public abstract class RequestBase
    {
        [XmlElement(ElementName = "security")]
        public Security Security { get; set; }
    }
}
