﻿using System.Xml.Serialization;

namespace SkyMedia.Entities
{
    public abstract class ValueEntityBase
    {
        [XmlAttribute(AttributeName = "value")]
        public string Value { get; set; }
    }
}
