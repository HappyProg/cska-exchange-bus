﻿using CSKA.Queue.Domain.Core;
using SkyMedia.Extensions;
using System.Text.RegularExpressions;

namespace SkyMedia.Normalizers
{
    public class SmsMessageNormalizer
    {
        private SmsMessage _message;

        public void Normalize(SmsMessage message)
        {
            _message = message;
            RemovePhoneNumberExpectedSymbols();
            AddPhoneNumberRussianCountryCode();
            RemoveTextWindowsNewlines();
        }

        private void RemovePhoneNumberExpectedSymbols()
        {
            if (string.IsNullOrWhiteSpace(_message.Receiver))
            {
                return;
            }
            _message.Receiver = _message.Receiver.AsNumericPhoneNumber();
        }

        private void AddPhoneNumberRussianCountryCode()
        {
            if (string.IsNullOrWhiteSpace(_message.Receiver))
            {
                return;
            }
            if (Regex.IsMatch(_message.Receiver, @"^\d{10}$"))
            {
                _message.Receiver = _message.Receiver.Insert(0, "7");
            }
        }

        private void RemoveTextWindowsNewlines()
        {
            if (string.IsNullOrWhiteSpace(_message.Text))
            {
                return;
            }
            _message.Text = _message.Text.AsUnixMultiline();
        }
    }
}
