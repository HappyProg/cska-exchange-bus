﻿using System.Xml.Serialization;

namespace SkyMedia
{
    /// <summary>
    /// Тип SMS-сообщения.
    /// </summary>
    public enum SmsMessageType
    {
        /// <summary>
        /// Обычная SMS.
        /// </summary>
        [XmlEnum(Name = "sms")]
        Sms
    }
}
