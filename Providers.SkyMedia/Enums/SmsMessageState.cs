﻿using System.Xml.Serialization;

namespace SkyMedia
{
    /// <summary>
    /// Статус SMS-сообщения.
    /// </summary>
    public enum SmsMessageState
    {
        /// <summary>
        /// Статус сообщения не получен.
        /// </summary>
        [XmlEnum(Name = "send")]
        Send,
        /// <summary>
        /// Сообщение не было доставлено.
        /// Конечный статус (не меняется со временем).
        /// </summary>
        [XmlEnum(Name = "not_deliver")]
        NotDelivered,
        /// <summary>
        /// Абонент находился не в сети в те моменты, когда делалась попытка доставки.
        /// Конечный статус (не меняется со временем).
        /// </summary>
        [XmlEnum(Name = "expired")]
        Expired,
        /// <summary>
        /// Сообщение доставлено.
        /// Конечный статус (не меняется со временем).
        /// </summary>
        [XmlEnum(Name = "deliver")]
        Delivered,
        /// <summary>
        /// Сообщение было отправлено, но статус так и не был получен.
        /// Конечный статус (не меняется со временем).
        /// Для разъяснения причин отсутствия статуса необходимо связаться со службой технической поддержки.
        /// </summary>
        [XmlEnum(Name = "partly_deliver")]
        PartlyDelivered
    }
}
