﻿using System.Xml.Serialization;

namespace SkyMedia
{
    /// <summary>
    /// Режим транслитерации текста SMS-сообщения с кириллицы на латиницу.
    /// </summary>
    public enum TranslitMode
    {
        /// <summary>
        /// Транслитерация отключена.
        /// </summary>
        [XmlEnum("0")]
        Disabled,
        /// <summary>
        /// Транслитерация включена.
        /// </summary>
        [XmlEnum("1")]
        Enabled
    }
}
