﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CSKA.PBI.Data.Repositories;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Infrastructure.BL.Consumers.PBI;
using CSKA.Queue.Providers.PBI;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    internal class PbiTicketsConsumer : BasePbiConsumer<Transaction<Ticket>>
    {


        public PbiTicketsConsumer(PbiTicketsProvider provider, ILogger<PbiTicketsConsumer> logger, IStorageRepository repository, IMapper mapper) : base(provider, logger, mapper, repository)
        {
        }

        public override async Task Consume(ConsumeContext<Transaction<Ticket>> context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            var ticket = Mapper.Map<Transaction<Ticket>, PBI.Data.Models.Ticket>(context.Message);
            var currentEvent = await Repository.GetEvent(context.Message.Data.Event.EventId).ConfigureAwait(false);
            var seat = CreateSeat(context.Message.Data.Seat);
            if (currentEvent != null)
            {
                ticket.Seat = await Repository.GetSeat(seat).ConfigureAwait(false) ?? seat;
                currentEvent.Tickets.Add(ticket);
                await Repository.UpdateEvent(currentEvent).ConfigureAwait(false);
            }
            else
            {
                ticket.Seat = await Repository.GetSeat(seat).ConfigureAwait(false) ?? seat;
                ticket.Event = CreateEvent(context.Message.Data.Event);
                await Repository.AddOrUpdateTicket(ticket).ConfigureAwait(false);
            }
            await base.Consume(context);
        }
    }
}
