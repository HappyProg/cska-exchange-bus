﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using CSKA.PBI.Data.Repositories;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Infrastructure.BL.Consumers.PBI;
using CSKA.Queue.Providers.PBI;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    //internal class PbiPassageConsumer : BasePbiConsumer<Transaction<Passage>>
    //{


    //    public PbiPassageConsumer(PbiPassageProvider provider, ILogger<PbiPassageConsumer> logger, IMapper mapper, IStorageRepository repository) : base(provider, logger, mapper, repository)
    //    {
    //    }

    //    public override async Task Consume(ConsumeContext<Transaction<Passage>> context)
    //    {
    //        if (context == null) throw new ArgumentNullException(nameof(context));
    //        var passage = Mapper.Map<Transaction<Passage>, PBI.Data.Models.Passage>(context.Message);
    //        var currentEvent =
    //            await Repository.GetEvent(context.Message.Data.Ticket.Event.EventId).ConfigureAwait(false);
    //        if (currentEvent != null)
    //        {
    //            currentEvent.Passages.Add(passage);
    //            await Repository.UpdateEvent(currentEvent).ConfigureAwait(false);
    //        }
    //        else
    //        {
    //            passage.Event = CreateEvent(context.Message.Data.Ticket.Event);
    //            await Repository.AddOrUpdatePassage(passage).ConfigureAwait(false);
    //        }
    //        await base.Consume(context);
    //    }
    //}
}
