﻿using System;
using AutoMapper;
using CSKA.PBI.Data.Repositories;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure.BL.Consumers.PBI
{
    internal abstract class BasePbiConsumer<T> : ConsumerBase<T> where T : class
    {
        protected readonly IStorageRepository Repository;

        protected readonly IMapper Mapper;

        protected BasePbiConsumer(ICommandProvider<T> provider, ILogger logger, IMapper mapper, IStorageRepository repository) : base(provider, logger)
        {
            Mapper = mapper;
            Repository = repository;
        }

        protected virtual CSKA.PBI.Data.Models.Event CreateEvent(Event @event)
        {
            if (@event == null) return null;
            return new CSKA.PBI.Data.Models.Event
            {
                Id = @event.EventId,
                Name = @event.EventName,
                SeatsCount = @event.SeatsCount,
                SeatsCountFree = @event.SeatsCountFree,
                ArenaName = @event.ArenaName,
                EventType = @event.EventType,
                Date = @event.Date ?? new DateTime()
            };
        }

        protected virtual CSKA.PBI.Data.Models.Seat CreateSeat(Seat seat)
        {
            if (seat == null) return null;
            return new CSKA.PBI.Data.Models.Seat
            {
                Num = seat.Num,
                Row = seat.Row,
                SectorName = seat.SectorName,
                TribuneName = seat.TribuneName
            };
        }
    }
}
