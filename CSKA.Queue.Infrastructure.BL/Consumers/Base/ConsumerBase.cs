﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CSKA.Queue.Infrastructure
{
    internal abstract class ConsumerBase<T> : IConsumer<T> where T : class
    {
        private readonly ICommandProvider<T> _provider;
        private readonly ILogger _logger;

        protected ConsumerBase(ICommandProvider<T> provider, ILogger logger)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public virtual async Task Consume(ConsumeContext<T> context)
        {
            try
            {
                var message = context.Message;
                await _provider
                    .SendAsync(message)
                    .ConfigureAwait(false);
            }
            catch(ProviderCriticalException ex)
            {
                _logger.LogCritical(ex, ex.Message);
                throw;
            }
            // TODO: Обработать вместо этого  - ProviderNotConnectedException
            // будет возможно только просле рефактора провайдеров Б24
            //
            catch (ProviderNotConnectedException ex)
            {
                _logger.LogError(ex, ex.Message);
                // Если не можем соединится то пробуем попытку чуть позже
                //
                throw;
            }
            catch (ProviderOperationException ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, ex.Message);
                throw;
            }
        }
    }
}
