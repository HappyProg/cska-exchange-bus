﻿using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Providers.B24;
using Evotor.API;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24EvotorStoresConsumer : ConsumerBase<EvotorStore>
    {
        public Bitrix24EvotorStoresConsumer(Bitrix24EvotorStoreProvider provider, ILogger<Bitrix24EvotorStoresConsumer> logger) : base(provider, logger)
        {
        }
    }
}
