﻿using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Providers.B24;
using Evotor.API;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24EvotorDevicesConsumer : ConsumerBase<EvotorDevice>
    {
        public Bitrix24EvotorDevicesConsumer(Bitrix24EvotorTerminalProvider provider, ILogger<Bitrix24EvotorDevicesConsumer> logger) : base(provider, logger)
        {
        }
    }
}
