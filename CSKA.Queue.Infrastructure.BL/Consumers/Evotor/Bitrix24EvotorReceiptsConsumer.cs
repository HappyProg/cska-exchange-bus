﻿using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Providers.B24;
using Evotor.API;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24EvotorReceiptsConsumer : ConsumerBase<EvotorReceipt>
    {
        public Bitrix24EvotorReceiptsConsumer(Bitrix24EvotorReceiptsProvider provider, ILogger<Bitrix24EvotorReceiptsConsumer> logger) : base(provider, logger)
        {
        }
    }
}
