﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24EventsConsumer : ConsumerBase<Event>
    {
        public Bitrix24EventsConsumer(Bitrix24EventProvider provider, ILogger<Bitrix24EventsConsumer> logger) : base(provider, logger)
        {
        }
    }
}
