﻿using CSKA.Queue.Domain.Core;
using MassTransit;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using CSKA.Queue.Infrastructure.BL.Mailer;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24TicketsFaultConsumer : IConsumer<Fault<Transaction<Ticket>>>
    {
        private readonly ILogger _logger;

        private readonly IMailerService _mailerService;

        private const string SUBJECT = "Ошибка обработки билета";

        public Bitrix24TicketsFaultConsumer(ILogger<Bitrix24TicketsFaultConsumer> logger, IMailerService mailerService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mailerService = mailerService;
        }

        public async Task Consume(ConsumeContext<Fault<Transaction<Ticket>>> context)
        {
            try
            {
                var errorMessage = CreateErrorMessage(context.Message);
                await _mailerService.Send(SUBJECT + " " + context.Message.Timestamp.ToLocalTime().ToString(), errorMessage).ConfigureAwait(false);

            }
            catch (ProviderNotConnectedException ex)
            {
                _logger.LogError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, ex.Message);
            }
        }

        private string CreateErrorMessage(Fault<Transaction<Ticket>> fault)
        {
            var message = "Текст ошибок : \r\n";
            message = fault.Exceptions.Aggregate(message, (current, exception) => current + (exception.Message + "\r\n\r\n"));
            message += $"Билет: ШК - {fault.Message.Data.Barcode}, ID - {fault.Message.Data.Id}\r\n";
            message +=
                $"Матч: {fault.Message.Data.Event.EventName}, {fault.Message.Data.Event.Date}, ID - {fault.Message.Data.Event.EventId}\r\n";
            message += $"Клиент: {fault.Message.Customer.Id}";
            return message;
        }
    }
}
