﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24ReservationConsumer : ConsumerBase<Transaction<Reservation>>
    {
        public Bitrix24ReservationConsumer(Bitrix24ReservationProvider provider, ILogger<Bitrix24ReservationConsumer> logger) : base(provider, logger)
        {
        }
    }
}
