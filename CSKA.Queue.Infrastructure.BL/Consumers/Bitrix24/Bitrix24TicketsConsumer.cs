﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24TicketsConsumer : ConsumerBase<Transaction<Ticket>>
    {
        public Bitrix24TicketsConsumer(Bitrix24TicketProvider provider, ILogger<Bitrix24TicketsConsumer> logger) : base(provider, logger)
        {
        }
    }
}
