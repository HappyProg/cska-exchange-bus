﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24SmsConsumer : ConsumerBase<SmsMessage>
    {
        public Bitrix24SmsConsumer(Bitrix24SmsProvider provider, ILogger<Bitrix24SmsConsumer> logger) : base(provider, logger)
        {
        }
    }
}
