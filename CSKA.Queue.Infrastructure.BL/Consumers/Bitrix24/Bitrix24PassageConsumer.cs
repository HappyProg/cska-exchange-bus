﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24PassageConsumer : ConsumerBase<Transaction<Passage>>
    {
        public Bitrix24PassageConsumer(Bitrix24PassageProvider provider, ILogger<Bitrix24PassageConsumer> logger) : base(provider, logger)
        {
        }
    }
}
