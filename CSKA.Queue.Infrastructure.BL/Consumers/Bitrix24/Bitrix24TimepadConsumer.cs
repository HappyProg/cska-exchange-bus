﻿using CSKA.Queue.Providers.B24;
using Microsoft.Extensions.Logging;
using Timepad.API.Webhooks;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24TimepadConsumer : ConsumerBase<TimepadOrderWebhook>
    {
        public Bitrix24TimepadConsumer(Bitrix24TimepadProvider provider, ILogger<Bitrix24TimepadConsumer> logger) : base(provider, logger)
        {
        }
    }
}
