﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24CustomersConsumer : ConsumerBase<Customer>
    {
        public Bitrix24CustomersConsumer(Bitrix24CustomerProvider provider, ILogger<Bitrix24CustomersConsumer> logger) : base(provider, logger)
        {
        }
    }
}
