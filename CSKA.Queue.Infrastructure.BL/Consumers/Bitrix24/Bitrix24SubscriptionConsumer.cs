﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure
{
    internal class Bitrix24SubscriptionConsumer : ConsumerBase<Transaction<Ticket>>
    {
        public Bitrix24SubscriptionConsumer(Bitrix24TicketProvider provider, ILogger<Bitrix24SubscriptionConsumer> logger) : base(provider, logger)
        {
        }
    }
}
