﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Infrastructure.BL.Configuration;
using MimeKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.Infrastructure.BL.Mailer
{
    internal class MailerService : IMailerService
    {
        private readonly string _host;
        private readonly int? _port;
        private readonly bool _enableSsl;
        private readonly string _fromAddress;
        private readonly string _login;
        private readonly string _password;
        private readonly List<string> _toAddresses;
        private readonly ILogger<MailerService> _logger;

        public MailerService(IConfiguration configuration, ILogger<MailerService> logger)
        {
            var smtp = GetSmtpConfiguration(configuration);
            _logger = logger;
            _host = smtp.Host;
            _port = smtp.Port;
            _enableSsl = smtp.EnableSsl;
            _login = smtp.Login;
            _password = smtp.Password;
            _fromAddress = smtp.FromAddress;
            _toAddresses = smtp.ToAddresses;
            CheckCorrectConfiguration();
            var emails = new List<string> {_fromAddress};
            emails.AddRange(_toAddresses);
            CheckEmails(emails);
        }

        private SmtpConfiguration GetSmtpConfiguration(IConfiguration configuration)
        {
            var section = configuration.GetSection("Smtp");
            if (section == null)
            {
                throw new Exception("Smtp section is not exist");
            }
            var smtp = section.Get<SmtpConfiguration>();
            if (smtp == null)
            {
                throw new Exception("Smtp section is not valid");
            }

            return smtp;
        }

        private void CheckCorrectConfiguration()
        {
            if (_host != null && _port != null && _fromAddress != null && _login != null && _password != null && _fromAddress.Any()) return;
            var message = "Incorrect MailService configuration!";
            _logger.LogError(message);
            throw new Exception(message);
        }

        public async Task Send(string msgTitle, string msgBody)
        {
            var message = CreateMessage(msgTitle, msgBody);
            try
            {
                await Send(message).ConfigureAwait(false);
            }
            catch (SmtpCommandException e)
            {
                throw new ProviderNotConnectedException(e.Message);
            }
            catch (SmtpProtocolException e)
            {
                throw new ProviderNotConnectedException(e.Message);
            }
        }

        private async Task Send(MimeMessage message)
        {
            using var client = new SmtpClient();
            if (_port == null) throw new Exception("Port not specified");
            await client.ConnectAsync(_host, _port.Value, _enableSsl).ConfigureAwait(false);
            await client.AuthenticateAsync(_login, _password).ConfigureAwait(false);
            await client.SendAsync(message).ConfigureAwait(false);
            await client.DisconnectAsync(true).ConfigureAwait(false);
        }

        private MimeMessage CreateMessage(string msgTitle, string msgBody)
        {
            var message = new MimeMessage();
            message.Subject = msgTitle;
            message.From.Add(new MailboxAddress("", _fromAddress));
            message.To.AddRange(_toAddresses.Select(t => new MailboxAddress("", t)));
            message.Body = new TextPart()
            {
                Text = msgBody
            };
            return message;
        }

        private void CheckEmails(List<string> emails)
        {
            var isValid =  emails.TrueForAll(IsValidEmail);
            if (!isValid)
            {
                throw new Exception("Not valid emails");
            }
        }

        private bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                    RegexOptions.None, TimeSpan.FromMilliseconds(200));

                string DomainMapper(Match match)
                {
                    var idn = new IdnMapping();

                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }
            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
