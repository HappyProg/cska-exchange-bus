﻿using System.Threading.Tasks;

namespace CSKA.Queue.Infrastructure.BL.Mailer
{
    public interface IMailerService
    {
        Task Send(string msgTitle, string msgBody);
    }
}
