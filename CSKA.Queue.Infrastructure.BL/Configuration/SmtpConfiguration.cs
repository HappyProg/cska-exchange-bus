﻿
using System.Collections.Generic;

namespace CSKA.Queue.Infrastructure.BL.Configuration
{
    public class SmtpConfiguration
    {
        public string Host { get; set; }

        public int? Port { get; set; }

        public bool EnableSsl { get; set; }

        public string FromAddress { get; set; }

        public List<string> ToAddresses { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }


    }
}
