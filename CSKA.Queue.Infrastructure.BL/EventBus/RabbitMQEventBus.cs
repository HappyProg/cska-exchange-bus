﻿using MassTransit;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CSKA.Queue.Infrastructure
{
    public class RabbitMQEventBus : IHostedService
    {
        private readonly IBusControl _busControl;
        private readonly ILogger _loger;

        public RabbitMQEventBus(ILogger<RabbitMQEventBus> loger, IBusControl busControl)
        {
            _loger = loger ?? throw new ArgumentNullException(nameof(loger));
            _busControl = busControl ?? throw new ArgumentNullException(nameof(busControl));
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                _loger.LogInformation("Запуск шины RabitMQ...");
                await _busControl
                    .StartAsync(cancellationToken)
                    .ConfigureAwait(false);

                _loger.LogInformation("Запуск шины RabitMQ...Ok");
            }
            catch(Exception ex)
            {
                _loger.LogCritical(ex, ex.Message);
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _loger.LogInformation("Остановка шины RabitMQ...");

            await _busControl
                .StopAsync(cancellationToken)
                .ConfigureAwait(false);

            _loger.LogInformation("Шина RabitMQ остановлена!");
        }
    }
}
