﻿using CSKA.Queue.Domain.Interfaces;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace CSKA.Queue.Infrastructure
{
    public class EventBus : IEventBus
    {
        private readonly IBusControl _busControl;
        private readonly IMessageScheduler _scheduler;
        public EventBus(IBusControl busControl, IMessageScheduler scheduler)
        {
            _busControl = busControl ?? throw new ArgumentNullException(nameof(busControl));
            _scheduler = scheduler ?? throw new ArgumentNullException(nameof(scheduler));
        }

        public Task Publish<T>(T message)
        {
            return _busControl.Publish(message);
        }

        public Task PublishDelay<T>(T message, TimeSpan delay) where T: class
        {
            var sendTime = DateTime.UtcNow + delay;
            return _scheduler
                .SchedulePublish(sendTime, message);
        }
    }
}
