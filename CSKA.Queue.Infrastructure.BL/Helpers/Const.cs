﻿namespace CSKA.Queue.Infrastructure
{
    internal static class Const
    {
        public const string QUEUE_TICKETS_NAME = "tickets";
        public const string QUEUE_RESERVATION_NAME = "reservation";
        public const string QUEUE_PASSAGE_NAME = "passage";
        public const string QUEUE_SUBSCRIPTIONS_NAME = "subscriptions";
    }
}
