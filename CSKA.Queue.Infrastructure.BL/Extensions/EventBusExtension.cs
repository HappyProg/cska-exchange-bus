﻿using Microsoft.Extensions.DependencyInjection;
using MassTransit;
using System;
using GreenPipes;
using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Infrastructure.BL.Configuration;

namespace CSKA.Queue.Infrastructure
{
    public static class EventBusExtension
    {
        public static IServiceCollection AddRabbitMQEventBus(this IServiceCollection services, RabbitMqConfiguration rabbitMq)
        {
            var rabbitUri = new Uri(rabbitMq.Uri);

            services.AddMassTransit(x =>
            {
                x.AddRabbitMqMessageScheduler();
                x.SetKebabCaseEndpointNameFormatter();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.UseDelayedExchangeMessageScheduler();
                    cfg.UseRetry(r =>
                    {
                        r.Interval(1000, TimeSpan.FromSeconds(30));
                        r.Handle<ProviderNotConnectedException>();
                    });
  
                    cfg.Host(rabbitUri, host =>
                    {
                        host.Username(rabbitMq.UserName);
                        host.Password(rabbitMq.Password);
                    });
                    cfg.AutoDelete = false;
                    cfg.PurgeOnStartup = false;


                    cfg.ConfigureEndpoints(context);
                });

                x.AddConsumer<Bitrix24SmsConsumer>(c =>
                {
                    c.UseScheduledRedelivery(r =>
                    {
                        r.Intervals(TimeSpan.FromMinutes(3), TimeSpan.FromMinutes(5),
                            TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(30),
                            TimeSpan.FromHours(1), TimeSpan.FromHours(3),
                            TimeSpan.FromHours(3), TimeSpan.FromHours(5),
                            TimeSpan.FromHours(5), TimeSpan.FromHours(6),
                            TimeSpan.FromHours(12), TimeSpan.FromHours(12));
                        r.Ignore<ProviderNotConnectedException>();
                    });
                });

                x.AddConsumer<Bitrix24TimepadConsumer>(c =>
                {
                    c.UseScheduledRedelivery(r =>
                    {
                        r.Intervals(TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10),
                            TimeSpan.FromMinutes(30), TimeSpan.FromHours(1),
                            TimeSpan.FromHours(5), TimeSpan.FromHours(10),
                            TimeSpan.FromHours(24));
                        r.Ignore<ProviderNotConnectedException>();
                    });
                });

                #region PBI
                //x.AddConsumer<PbiPassageConsumer>(cfg =>
                //{
                //    cfg.UseConcurrencyLimit(1);
                //    cfg.UseRetry(r =>
                //    {
                //        r.Ignore<ProviderCriticalException>();
                //        r.Interval(5, TimeSpan.FromSeconds(5));
                //    });
                //});

                //x.AddConsumer<PbiTicketsConsumer>(cfg =>
                //{
                //    cfg.UseConcurrencyLimit(1);
                //    cfg.UseRetry(r =>
                //    {
                //        r.Ignore<ProviderCriticalException>();
                //        r.Interval(5, TimeSpan.FromSeconds(5));
                //    });
                //});

                
                #endregion

                #region Bitrix_Evotor

                x.AddConsumer<Bitrix24EvotorStoresConsumer>(cfg =>
                {
                    cfg.UseConcurrencyLimit(1);
                    cfg.UseRetry(r => r.Interval(30, TimeSpan.FromSeconds(10)));
                });

                x.AddConsumer<Bitrix24EvotorDevicesConsumer>(cfg =>
                {
                    cfg.UseConcurrencyLimit(1);
                    cfg.UseRetry(r => r.Interval(30, TimeSpan.FromSeconds(10)));
                });

                x.AddConsumer<Bitrix24EvotorReceiptsConsumer>(cfg =>
                {
                    cfg.UseConcurrencyLimit(1);
                    cfg.UseRetry(r => r.Interval(30, TimeSpan.FromSeconds(10)));
                });
                
                #endregion

                x.AddConsumer<Bitrix24EventsConsumer>(cfg => cfg.UseConcurrencyLimit(1));
                
                x.AddConsumer<Bitrix24TicketsConsumer>(cfg =>
                {
                    cfg.UseConcurrencyLimit(1);
                    cfg.UseRetry(r =>
                    {
                        r.Ignore<ProviderCriticalException>();
                        r.Interval(6, TimeSpan.FromSeconds(20));
                    });
                });
                x.AddConsumer<Bitrix24TicketsFaultConsumer>(cfg =>
                {
                    cfg.UseConcurrencyLimit(1);
                });

                x.AddConsumer<Bitrix24ReservationConsumer>(cfg => cfg.UseConcurrencyLimit(1));
                x.AddConsumer<Bitrix24PassageConsumer>(c =>
                {
                    c.UseConcurrencyLimit(1);
                    c.UseRetry(r => r.Interval(10, TimeSpan.FromSeconds(30)));
                });
                x.AddConsumer<Bitrix24CustomersConsumer>(c =>
                {
                    c.UseConcurrentMessageLimit(1);
                    c.UseRetry(r => r.Interval(10, TimeSpan.FromSeconds(30)));
                });
            });


            services.AddHostedService<RabbitMQEventBus>();
            services.AddSingleton<IEventBus, EventBus>();
            return services;
        }
    }
}
