﻿using CSKA.Queue.Infrastructure.BL.Mailer;
using Microsoft.Extensions.DependencyInjection;

namespace CSKA.Queue.Infrastructure.BL.Extensions
{
    public static class MailerExtension
    {
        public static IServiceCollection AddMailer(this IServiceCollection services)
        {
            services.AddSingleton<IMailerService, MailerService>();
            return services;
        }
    }
}
