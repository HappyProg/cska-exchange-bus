﻿using CSKA.Queue.Data;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Providers.Evotor;
using CSKA.Queue.Providers.Infomatika;
using Evotor.API;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Globalization;

namespace CSKA.Queue.Infrastructure
{
    public static class QueryDispatcherExtension
    {
        public static IServiceCollection AddEvotorQueryProviders(this IServiceCollection services)
        {
            services.AddTransient<ICreatorReceiptService, CreatorReceiptService>();
            services.AddTransient<EvotorDeviceProvider>();
            services.AddTransient<EvotorReceiptProvider>();
            services.AddTransient<EvotorStoreProvider>();

            services.AddEvotorHostedServices<EvotorReceiptProvider, EvotorReceipt>();
            services.AddEvotorHostedServices<EvotorDeviceProvider, EvotorDevice>();
            services.AddEvotorHostedServices<EvotorStoreProvider, EvotorStore>();
            return services;
        }

        public static IServiceCollection AddInfomatikaQueryProviders(this IServiceCollection services, InfomatikaConfiguration infomatika)
        {
            // Конфигурация Инфоматики
            //
            services.AddTransient(sp => infomatika);

            var dateBegin = DateTime.ParseExact("20.09.2020 00:00:00", "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture);

            services
            // Загрузка билетов / пакетов(билет на несколько матчей)
            //
            .AddInfomatikaHostedService<InfomatikaTicketProvider, Transaction<Ticket>>(dateBegin)
            // Загрузка абонементов
            //
            .AddInfomatikaHostedService<InfomatikaSubscriptionProvider, Transaction<Ticket>>(dateBegin)
            // Загрузка документов бронирования
            //
            .AddInfomatikaHostedService<InfomatikaReservationProvider, Transaction<Reservation>>(dateBegin)
            // Загрузка проходов билеты
            //            
            .AddInfomatikaHostedService<InfomatikaPassageTicketProvider, Transaction<Passage>>(dateBegin)
            // Загрузка проходов абонементы
            //            
            .AddInfomatikaHostedService<InfomatikaPassageSubscriptionsProvider, Transaction<Passage>>(dateBegin)
            // Загрузка клиентов / компаний
            //            
            .AddInfomatikaHostedService<InfomatikaCustomerProvider, Customer>(dateBegin)
            //Загрузка мероприятий
            //
            .AddInfomatikaHostedService<InfomatikaEventProvider, Event>(dateBegin)
            ;
            return services;
        }

        private static IServiceCollection AddProviderHostedService<TProvider, TEvent>(this IServiceCollection services,
            Func<IServiceProvider, IQueryProvider<TEvent>> implementationFactory) where TProvider : class
        {
            services.AddHostedService(sp =>
            {
                var scopeFactory = sp.GetService<IServiceScopeFactory>();
                var scope = scopeFactory.CreateScope();
                var repo = scope.ServiceProvider.GetRequiredService<IStatusRepository>();

                var eventBus = sp.GetService<IEventBus>();
                var logger = sp.GetService<ILoggerFactory>().CreateLogger<QueryDispatcher<TProvider, TEvent>>();

                var provider = implementationFactory.Invoke(sp);
                return new QueryDispatcher<TProvider, TEvent>(provider, eventBus, logger, repo);
            });

            return services;
        }

        private static IServiceCollection AddEvotorHostedServices<TProvider, TEvent>(this IServiceCollection services)
            where TProvider : class, IQueryProvider<TEvent>
        {
            return services.AddProviderHostedService<TProvider, TEvent>(sp =>
            {
                return sp.GetService<TProvider>();
            });
        }

        private static IServiceCollection AddInfomatikaHostedService<TProvider, TEvent>(this IServiceCollection services,  DateTime? dateBegin = null, DateTime? dateEnd = null)
            where TProvider : class
        {
            return services.AddProviderHostedService<TProvider, TEvent>(sp =>
            {
                var cfg = sp.GetService<InfomatikaConfiguration>();
                var provider = (InfomatikaProviderBase<TEvent>)Activator.CreateInstance(typeof(TProvider), new object[] { cfg });
                provider.DateBegin = dateBegin;
                provider.DateEnd = dateEnd;

                return provider;
            });
        }
    }
}
