﻿using Bitrix24.Connector;
using CSKA.Queue.Providers.B24;
using Evotor.API;
using Microsoft.Extensions.DependencyInjection;
using System;
using CSKA.Queue.Providers.B24.Handlers;
using CSKA.Queue.Providers.B24.Repositories;
using CSKA.Queue.Infrastructure.BL.Configuration;
using CSKA.Queue.Providers.B24.Queries;
using CSKA.Queue.Domain.Core;

namespace CSKA.Queue.Infrastructure
{
    public static class CommandDispatcherExtension
    {
        public static IServiceCollection AddCommandProviders(this IServiceCollection services, Bitrix24Configuration bitrix24)
        {
            services.AddHttpClient<Bitrix24SmsProvider>(cfg =>
            {
                cfg.BaseAddress = new Uri(bitrix24.SmsHook);
            });

            services.AddHttpClient<IEvotorApiConnector, EvotorApiConnector>(cfg =>
            {
                cfg.BaseAddress = new Uri("https://api.evotor.ru/");
                cfg.DefaultRequestHeaders.Add("Authorization", bitrix24.EvoToken);
            });

            services.AddTransient(sp => new B24Connector(bitrix24.Url));

            services
                .AddBitrix24CommandProviders();

            return services;
        }

        private static IServiceCollection AddBitrix24CommandProviders(this IServiceCollection services)
        {
            services.AddTransient<CustomerCompanyHandler>();
            services.AddTransient<CustomerContactHandler>();
            services.AddTransient<EventsHandler>();
            services.AddTransient<PassageHandler>();
            services.AddTransient<ReservationHandler>();
            services.AddTransient<TicketHandler>();
            services.AddTransient<TimepadHandler>();
            services.AddTransient<EvotorReceiptHandler>();
            services.AddTransient<EvotorDeviceHandler>();
            services.AddTransient<EvotorStoreHandler>();
            services.AddTransient<B24CompanyRepository>();
            services.AddTransient<B24ContactRepository>();
            services.AddTransient<B24ListElementRepository>();
            services.AddTransient<DealRepository>();
            services.AddTransient<B24ProductRepository>();
            services.AddTransient<Bitrix24EventProvider>();
            services.AddTransient<Bitrix24TicketProvider>();
            services.AddTransient<Bitrix24ReservationProvider>();
            services.AddTransient<Bitrix24PassageProvider>();
            services.AddTransient<Bitrix24CustomerProvider>();
            services.AddTransient<Bitrix24TimepadProvider>();
            services.AddTransient<Bitrix24EvotorTerminalProvider>();
            services.AddTransient<Bitrix24EvotorStoreProvider>();
            services.AddTransient<Bitrix24EvotorReceiptsProvider>();
            services.AddTransient<B24PbiQuery>();
            //services.AddTransient(sp => GetConfigurationAutoMapper());
            return services;
        }

        //private static MapperConfiguration GetConfigurationAutoMapper()
        //{
        //    var config = new MapperConfiguration(InitConfigurationAutoMapper);
        //    config.AssertConfigurationIsValid();
        //    return config;
        //}

        //private static void InitConfigurationAutoMapper(IMapperConfigurationExpression cfg)
        //{
        //    var assembly = Assembly.GetCallingAssembly();
        //    var types = assembly.DefinedTypes;
        //    var profilesTypes = types
        //        .Where(t => typeof(Profile).GetTypeInfo().IsAssignableFrom(t)
        //                    && !t.IsAbstract);
        //    var profiles = profilesTypes.Select(t => t.AsType());
        //    foreach (var profile in profiles)
        //    {
        //        cfg.AddProfile(profile);
        //    }
        //}
    }
}
