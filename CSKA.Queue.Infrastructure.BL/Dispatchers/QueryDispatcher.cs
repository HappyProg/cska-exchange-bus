﻿using CSKA.Queue.Data;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CSKA.Queue.Infrastructure
{
    internal class QueryDispatcher<TProvider, TEvent> : BackgroundService
    {
        private readonly IQueryProvider<TEvent> _provider;
        private readonly IEventBus _eventBus;
        private readonly ILogger _logger;
        private readonly IStatusRepository _statusRepository;
        private readonly TimeSpan DELAY_TIMEOUT = TimeSpan.FromMinutes(2);

        public QueryDispatcher(IQueryProvider<TEvent> provider,
            IEventBus eventBus, ILogger<QueryDispatcher<TProvider, TEvent>> logger, IStatusRepository statusRepository)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _statusRepository = statusRepository ?? throw new ArgumentNullException(nameof(statusRepository));
        }

        private async Task CreateEventOnBus(IEnumerable<TEvent> events)
        {
            try
            {
                foreach (var e in events)
                {
                    await _eventBus
                        .Publish(e)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, ex.Message);
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            int resultLength = 0;
            //await _statusRepository.Delete(_provider.Key);
            //string state = "{\"DateFrom\":\"0001-01-01T00:00:00\",\"DateTo\":\"0001-01-01T00:00:00\",\"States\":[{\"EventId\":632,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":680,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":682,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":683,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":671,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":696,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":695,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":697,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":679,\"RowVersion\":\"AAAAABo17EA=\",\"EventRowVersion\":null},{\"EventId\":674,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":604,\"RowVersion\":\"AAAAABos8Oo=\",\"EventRowVersion\":null},{\"EventId\":694,\"RowVersion\":\"AAAAABoT6o4=\",\"EventRowVersion\":null},{\"EventId\":605,\"RowVersion\":\"AAAAABos8Os=\",\"EventRowVersion\":null},{\"EventId\":668,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":685,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":684,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":665,\"RowVersion\":\"AAAAABmwlgU=\",\"EventRowVersion\":null},{\"EventId\":701,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":687,\"RowVersion\":\"AAAAAB0Bq30=\",\"EventRowVersion\":null},{\"EventId\":700,\"RowVersion\":\"AAAAAB0Bq40=\",\"EventRowVersion\":null},{\"EventId\":666,\"RowVersion\":\"AAAAABmwlTQ=\",\"EventRowVersion\":null},{\"EventId\":675,\"RowVersion\":\"AAAAABpOGBA=\",\"EventRowVersion\":null},{\"EventId\":688,\"RowVersion\":\"AAAAAB0Bq4E=\",\"EventRowVersion\":null},{\"EventId\":672,\"RowVersion\":\"AAAAABo2P9k=\",\"EventRowVersion\":null},{\"EventId\":692,\"RowVersion\":\"AAAAAB0Bq4Q=\",\"EventRowVersion\":null},{\"EventId\":689,\"RowVersion\":\"AAAAAB0Bq4c=\",\"EventRowVersion\":null},{\"EventId\":676,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":693,\"RowVersion\":\"AAAAABoT6o0=\",\"EventRowVersion\":null},{\"EventId\":699,\"RowVersion\":\"AAAAAB0Bq4o=\",\"EventRowVersion\":null},{\"EventId\":673,\"RowVersion\":\"AAAAABygUxI=\",\"EventRowVersion\":null},{\"EventId\":698,\"RowVersion\":\"AAAAAByh1nI=\",\"EventRowVersion\":null},{\"EventId\":690,\"RowVersion\":\"AAAAABtMlm8=\",\"EventRowVersion\":null},{\"EventId\":702,\"RowVersion\":\"AAAAABtem4Q=\",\"EventRowVersion\":null},{\"EventId\":691,\"RowVersion\":\"AAAAABvkb/g=\",\"EventRowVersion\":null},{\"EventId\":705,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":703,\"RowVersion\":\"AAAAAB0Bq5A=\",\"EventRowVersion\":null},{\"EventId\":706,\"RowVersion\":\"AAAAABuMaKs=\",\"EventRowVersion\":null},{\"EventId\":707,\"RowVersion\":null,\"EventRowVersion\":null},{\"EventId\":708,\"RowVersion\":\"AAAAAB0Bq5M=\",\"EventRowVersion\":null},{\"EventId\":709,\"RowVersion\":\"AAAAAB0Bq5c=\",\"EventRowVersion\":null},{\"EventId\":710,\"RowVersion\":\"AAAAABzrBqQ=\",\"EventRowVersion\":null},{\"EventId\":711,\"RowVersion\":\"AAAAAB0Mjqs=\",\"EventRowVersion\":null},{\"EventId\":712,\"RowVersion\":\"AAAAAB1XVu8=\",\"EventRowVersion\":null},{\"EventId\":713,\"RowVersion\":\"AAAAAB2BwWo=\",\"EventRowVersion\":null},{\"EventId\":714,\"RowVersion\":\"AAAAAB2Bo94=\",\"EventRowVersion\":null}]}";
            //string state = null;
            while (stoppingToken.IsCancellationRequested == false)
            {
                do
                {
                    // получаем текущее состояние
                    // специально внесено в цикл чтобы оперативно получать 
                    // актуальное состояние при каждой итерации, ведь его можно изменить из внешних источников
                    //
                    var state = await GetState(_provider.Key).ConfigureAwait(false);
                    //var state = "[{\"Uuid\":\"20171219-fa9a-40a1-802c-09df8ae6358f\",\"Address\":\"Россия, Москва, Ленинградский проспект, 39с3 \",\"Name\":\"Синяя касса NEW 1\"},{\"Uuid\":\"20180111-25c9-40a3-8081-dad7018c6a3b\",\"Address\":\"Россия, Москва, Ленинградский проспект, 39с3 \",\"Name\":\"Синяя касса NEW 2\"},{\"Uuid\":\"20180111-4d84-40fa-800c-9905f5fafea5\",\"Address\":\"Россия, Москва, Ленинградский проспект, 39с3 \",\"Name\":\"Отдел продаж - old\"},{\"Uuid\":\"20180118-be26-40e8-800d-3e23cbc1ef2d\",\"Address\":\"Россия, Москва, Ленинградский проспект, 39с3 \",\"Name\":\"Красная касса NEW 1\"},{\"Uuid\":\"20191018-e346-4024-8046-580c159cca9e\",\"Address\":\"Россия, Москва, Ленинградский проспект, 39с3\",\"Name\":\"Красная касса NEW 2\"},{\"Uuid\":\"20200204-3684-405a-80b4-1ee4ac5e0d22\",\"Address\":\"-\",\"Name\":\"Касса 428_\"}]";
                    //string state = null;
                   
                    try
                    {
                        var data = await _provider
                            .GetDataAsync(1, 1000, state)
                            .ConfigureAwait(false);

                        resultLength = data.Count;
                        state = data.State;

                        // Кладем в очередь...
                        //
                        await CreateEventOnBus(data.Data).ConfigureAwait(false);

                        if (string.IsNullOrWhiteSpace(state) == false)
                        {
                            // Сохраняем состояние для провайдера
                            //
                            await SaveState(_provider.Key, state).ConfigureAwait(false);
                        }
                    }
                    catch (ProviderOperationException ex)
                    {
                        _logger.LogError(ex, ex.Message);
                    }
                    catch (ProviderNotConnectedException ex)
                    {
                        _logger.LogError(ex, ex.Message);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogCritical(ex, ex.Message);
                    }

                    await Task
                        .Delay(TimeSpan.FromSeconds(10), stoppingToken)
                        .ConfigureAwait(false);
                }
                while (resultLength > 0);               

                // Ожидание следующей итерации
                //
                await Task
                    .Delay(DELAY_TIMEOUT, stoppingToken)
                    .ConfigureAwait(false);
            }
        }

        private Task SaveState(string providerKey, string state)
        {
            return _statusRepository.SaveAsync(providerKey, state);
        }

        private Task<string> GetState(string providerKey)
        {
            return _statusRepository.GetAsync(providerKey);
        }
    }
}
