﻿using System;
using CSKA.Queue.Domain.Core;

namespace CSKA.Queue.Providers.B24.Helpers
{
    public static class TicketHelper
    {
        public static string ConvertTicketSate(TicketState ticketState)
        {
            switch (ticketState)
            {
                case TicketState.DeletedByEvent:
                    return Const.TICKET_STATE.DELETED;
                case TicketState.Deleted:
                    return Const.TICKET_STATE.DELETED;
                case TicketState.Sold:
                    return Const.TICKET_STATE.SOLD;
                case TicketState.Returned:
                    return Const.TICKET_STATE.RETURNED;
                default:
                    return "";
            }
        }

        public static int ConvertCategory(TransactionType transactionType)
        {
            switch (transactionType)
            {
                case TransactionType.Ticket:
                    return Const.DealCategory.CATEGORY_TICKET_SALE;
                case TransactionType.Subscription:
                    return Const.DealCategory.CATEGORY_SUBSCRIPTION;
                default:
                    throw new InvalidCastException($"Неизвестный тип транзакции: {transactionType}");
            }
        }
    }
}
