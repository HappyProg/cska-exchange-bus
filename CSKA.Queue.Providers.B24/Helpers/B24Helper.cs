﻿using System;
using System.Globalization;
using Bitrix24.Connector;

namespace CSKA.Queue.Providers.B24.Helpers
{
    public static class B24Helper
    {
        public static DateTimeOffset GetDateTime(B24ListElement element)
        {
            var date = element[Const.Fields.B24ListElement.PROP_NAME_EVENT_DATE];
            if (DateTimeOffset.TryParseExact(date.ToString(), "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTimeOffset result))
            {
                return result;
            }
            return DateTimeOffset.Now;
        }

        public static double GetPrice(Deal deal)
        {
            var price = deal[Const.Fields.DealConst.PROP_TICKET_PRICE];
            if (double.TryParse(price.ToString(), NumberStyles.Any,
                CultureInfo.InvariantCulture, out double result))
            {
                return result;
            }
            return 0;
        }
    }
}
