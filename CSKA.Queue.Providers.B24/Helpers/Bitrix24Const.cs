﻿namespace CSKA.Queue.Providers.B24
{
    public static class Const
    {
        public static class DealCategory
        {
            /// <summary>
            /// Offline Продажа атрибутики через эвотор 
            /// </summary>
            public const int CATEGORY_OFFLINE_SALE = 1;
            /// <summary>
            /// Бронирование билетов
            /// </summary>
            public const int CATEGORY_RESERVATION = 2;
            /// <summary>
            /// Продажа билетов
            /// </summary>
            public const int CATEGORY_TICKET_SALE = 4;
            /// <summary>
            /// Пакеты мероприятий
            /// </summary>
            public const int CATEGORY_SUBSCRIPTION = 5;
            /// <summary>
            /// Абонементы
            /// </summary>
            public const int CATEGORY_TIMEPAD = 6;
        }

        public static class UniversalLists
        {
            /// <summary>
            /// Список магазинов Эвотор
            /// </summary>
            public const int BITRIX_LIST_EVOTOR_STORE = 28;
            /// <summary>
            /// Список терминалов Эвотор
            /// </summary>
            public const int BITRIX_LIST_EVOTOR_DEVICE = 29;

            public const long BLOCK_ID_EVENTS = 15;

            public const long BLOCK_ID_EVENTS_GAME = 22;

            public const long BLOCK_ID_EVENTS_SUBSCRIPTION = 23;

            public const long BLOCK_ID_PASSAGE = 19;

            public const long BLOCK_ID_SEATS = 20;

            public const int SEATS_BOOKING_BUY_FIELD_N = 50;

            public const int SEATS_BOOKING_BUY_FIELD_Y = 51;

            public const long BLOCK_ID_TIMEPAD_TICKETS = 26;
        }

        public static class TICKET_STATE
        {
            public const string DELETED = "66";
            public const string SOLD = "67";
            public const string RETURNED = "68";
        }

        public static class TICKET_TYPE 
        {
            public const long ETicket = 726996;
            public const long Ticket = 726995;
        }

        /// <summary>
        /// Тип чека
        /// </summary>
        public static class RECEIPT_TYPE
        {
            /// <summary>
            /// Наличные
            /// </summary>
            public const int SELL = 115;
            /// <summary>
            /// Карта
            /// </summary>
            public const int REFUND = 116;
        }
        /// <summary>
        /// Тип оплаты
        /// </summary>
        public static class PAYMENT_TYPE
        {
            /// <summary>
            /// Наличные
            /// </summary>
            public const int CASH = 117;
            /// <summary>
            /// Карта
            /// </summary>
            public const int CARD = 118;
        }
        /// <summary>
        /// Единицы измерения
        /// </summary>
        public static class MEASURE
        {
            /// <summary>
            /// Метры
            /// </summary>
            public const int M = 1;
            /// <summary>
            /// Литры
            /// </summary>
            public const int L = 2;
            /// <summary>
            /// Граммы
            /// </summary>
            public const int G = 3;
            /// <summary>
            /// Килограммы
            /// </summary>
            public const int KG = 4;
            /// <summary>
            /// Штука
            /// </summary>
            public const int PS = 5;
        }
        public static class Fields
        {
            /// <summary>
            /// Товары
            /// </summary>
            public static class Products
            {
                /// <summary>
                /// Внешний идентификатор
                /// </summary>
                public const string EXT_ID = "XML_ID";
            }

            /// <summary>
            /// Поля для списка Магазинов Эвотор
            /// </summary>
            public static class EvotorStores
            {
                public const string UUID = "PROPERTY_102";
                public const string NAME = "NAME";
                public const string ADDRESS = "PROPERTY_97";
            }
            /// <summary>
            /// Поля для списка терминалов Эвотор
            /// </summary>
            public static class EvotorDevices
            {
                public const string UUID = "PROPERTY_101";
                public const string NAME = "NAME";
            }
            /// <summary>
            /// Поля для сделки - Чеки Эвотор
            /// </summary>
            public static class EvotorReceipts
            {
                /// <summary>
                /// {"NAME":"Продажа","VALUE":115},{"NAME":"Возврат","VALUE":116}
                /// </summary>
                public const string RECEIPT_TYPE = "UF_CRM_1601634879243";
                /// <summary>
                /// {"NAME":"Наличные","VALUE":117},{"NAME":"Карта","VALUE":118}
                /// </summary>
                public const string PAYMENT_TYPE = "UF_CRM_1601634904326";
                /// <summary>
                /// Номер смены
                /// </summary>
                public const string SHIFT = "UF_CRM_1601634966632";
                /// <summary>
                /// Продавец
                /// </summary>
                public const string SELLER = "UF_CRM_1601635252288";
                /// <summary>
                /// Привязка к магазину
                /// </summary>
                public const string STORE = "UF_CRM_1601636709";
                /// <summary>
                /// Привязка к терминалу
                /// </summary>
                public const string DEVICE = "UF_CRM_1601636678";
                /// <summary>
                /// id чека Эвотор
                /// </summary>
                public const string RECEIPT_UUID = "UF_CRM_1602159561";
                /// <summary>
                /// Базовый чек, например документ основание для чека возврата
                /// </summary>
                public const string RECEIPT_BASE_UUID = "UF_CRM_1602164091";

            }

            public static class B24CompanyConst
            {
                //public const string ID = "UF_CRM_1593433269050";
                public const string EMAIL_NAME = "EMAIL";
                public const string TITLE_NAME = "Title";
                public const string IDs = "UF_CRM_1606390433";
            }

            public static class B24ContactConst
            {
                //public const string CODE = "UF_CRM_1593433206114";
                public const string EMAIL_NAME = "EMAIL";
                public const string PROP_NAME = "NAME";
                public const string PROP_SECOND_NAME = "SECOND_NAME";
                public const string PROP_LAST_NAME = "LAST_NAME";
                public const string IDs = "UF_CRM_1605622747";
            }

            public static class B24ListElement
            {
                public const string PROP_NAME_EVENT_DATE = "PROPERTY_52";
                public const string PROP_NAME_EVENT_ID = "PROPERTY_67";
                public const string PROP_NAME_EVENT_ARENA_NAME = "PROPERTY_61";
                public const string PROP_NAME_EVENT_SEATS_COUNT = "PROPERTY_53";
                public const string PROP_NAME_EVENT_SEATS_COUNT_FREE = "PROPERTY_111";
                public const string PROP_NAME_EVENT_ID_BATCH = "PROPERTY_82";
                public const string PROP_NAME_LINKED_EVENTS_BATCH = "PROPERTY_78";
                public const string PROP_NAME_EVENT_STATUS = "PROPERTY_114";
                public const string PROP_NAME_EVENT_ID_SUBSCRIPTION = "PROPERTY_85";
                public const string PROP_NAME_LINKED_EVENTS_SUBSCRIPTION = "PROPERTY_79";
                public const string PROP_NAME_DATE_BEGIN_SUBSCRIPTION = "PROPERTY_80";
                public const string PROP_NAME_DATE_END_SUBSCRIPTION = "PROPERTY_81";
                public const string PROP_NAME_TICKET_BARCODE = "PROPERTY_86";
                public const string PROP_NAME_TICKET_EVENT_ID = "PROPERTY_69";
                public const string PROP_NAME_TICKET_ID = "PROPERTY_88";
                public const string PROP_NAME_DEAL_ID = "PROPERTY_65";
                public const string PROP_NAME_DATE_PASSAGE = "PROPERTY_66";
                public const string PROP_NAME_CONTACT_OR_COMPANY_ID = "PROPERTY_68";
                public const string PROP_NAME_RESERVATION_EVENT_ID = "PROPERTY_75";
                public const string PROP_NAME_RESERVATION_SEAT_SECTOR = "PROPERTY_71";
                public const string PROP_NAME_RESERVATION_SEAT_ROW = "PROPERTY_72";
                public const string PROP_NAME_RESERVATION_SEAT_NUM = "PROPERTY_73";
                public const string PROP_NAME_RESERVATION_SEAT_DEAL_ID = "PROPERTY_74";
                public const string PROP_NAME_RESERVATION_ARENA_NAME = "PROPERTY_70";
                public const string PROP_NAME_RESERVATION_SEATS_BOOKING = "PROPERTY_83";
                public const string PROP_NAME_LINKED_DEAL_ID = "PROPERTY_84";
                public const string PROP_NAME_TIMEPAD_ID = "PROPERTY_92";
                public const string PROP_NAME_TIMEPAD_DEAL_ID = "PROPERTY_90";
                public const string PROP_NAME_START_AT = "PROPERTY_94";
            }

            public static class DealConst
            {
                public const string PROP_TICKET_BARCODE = "UF_CRM_1587987572475";
                public const string PROP_CODE = "UF_CRM_1588856641";
                public const string PROP_CONTACT_ID = "CONTACT_ID";
                public const string PROP_CONTACT_IDS = "CONTACT_IDS";
                public const string PROP_CATEGORY_ID = "CATEGORY_ID";
                public const string PROP_COMPANY_ID = "COMPANY_ID";
                public const string PROP_BOOKING_ID = "UF_CRM_1586950663668";
                public const string TITLE = "TITLE";
                public const string PROP_NUMBER = "UF_CRM_1594277861516";
                public const string PROP_EVENT_DATE = "UF_CRM_1605693306";
                public const string PROP_COMMENTS = "COMMENTS";
                public const string PROP_RESERVATION_STATUS = "UF_CRM_1594026250330";
                public const string PROP_TICKET_ID = "UF_CRM_1593764863601";
                public const string PROP_TICKET_STATE = "UF_CRM_1587987605011";
                public const string PROP_DISCOUNT_NAME = "UF_CRM_1588251070";
                public const string PROP_TICKET_TYPE = "UF_CRM_1605686182";
                public const string PROP_CITYCARD_BONUS_AMOUNT = "UF_CRM_1611851064111";
                public const string PROP_DISCOUNT_AMOUNT = "UF_CRM_1587987320134";
                public const string PROP_TICKET_PRICE = "OPPORTUNITY";
                public const string PROP_SALE_DATE = "UF_CRM_1587987347268";
                public const string PROP_TICKET_EVENT_ID = "UF_CRM_1587987558247";
                public const string PROP_EVENT_ID = "UF_CRM_1592387908";
                public const string PROP_EVENT_SUBSCRIPTION_DATE_END = "UF_CRM_1594036398300";
                public const string PROP_EVENT_SUBSCRIPTION_ID = "UF_CRM_1592315426";
                public const string PROP_EVENT_BATCH_DATE_END = "UF_CRM_1594036398300";
                public const string PROP_EVENT_BATCH_ID = "UF_CRM_1592387234";
                public const string PROP_CAMPAIGN = "UTM_CAMPAIGN";
                public const string PROP_MEDIUM = "UTM_MEDIUM";
                public const string PROP_SOURCE = "UTM_SOURCE";
                public const string PROP_GACID = "UF_CRM_1594823191646";
                public const string PROP_TIMEPAD_STATUS = "UF_CRM_1594888198";
            }
        }
    }
}
