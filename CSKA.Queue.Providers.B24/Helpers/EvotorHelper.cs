﻿using System;
using Evotor.API;

namespace CSKA.Queue.Providers.B24.Helpers
{
    public static class EvotorHelper
    {

        public static int GetReceiptType(ReceiptType receiptType)
        {
            return receiptType switch
            {
                ReceiptType.PAYBACK => Const.RECEIPT_TYPE.REFUND,
                ReceiptType.SELL => Const.RECEIPT_TYPE.SELL,
                _ => throw new NotSupportedException($"Тип не поддерживается {receiptType}"),
            };
        }

        public static string ParseId(string id)
        {
            return id.Replace("_", "#");
            //var index = id.IndexOf('_');
            //return index > 0 ? id.Substring(0, index) : id;
        }

        public static int GetPaymentType(PaymentSource paymentSource)
        {
            return paymentSource switch
            {
                PaymentSource.PAY_CASH => Const.PAYMENT_TYPE.CASH,
                PaymentSource.PAY_CARD => Const.PAYMENT_TYPE.CARD,
                PaymentSource.OTHER => Const.PAYMENT_TYPE.CASH,
                _ => throw new NotSupportedException($"Тип не поддерживается {paymentSource}"),
            };
        }
    }
}
