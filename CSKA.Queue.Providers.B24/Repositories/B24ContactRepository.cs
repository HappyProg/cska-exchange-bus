﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;
using Bitrix24.Connector.Core;

namespace CSKA.Queue.Providers.B24.Repositories
{
    public class B24ContactRepository : B24Repository<B24Contact>
    {
        public B24ContactRepository(B24Connector connector) : base(connector)
        {
        }

        public override Task<B24Contact> GetEntityAsync(B24Query query, long id = default)
        {
            return Connector
                .Contacts
                .FirstAsync(query);
        }

        public override Task<long> CreateEntityAsync(B24Contact entity)
        {
            return Connector
                .Contacts
                .CreateAsync(entity);
        }

        public override Task<bool> UpdateEntityAsync(B24Contact entity)
        {
            return Connector
                .Contacts
                .UpdateAsync(entity);
        }

        public override Task<bool> DeleteAsync(long id, string code = null)
        {
            return Connector.Contacts.DeleteAsync(id);
        }

        protected override async Task<B24Response<IEnumerable<B24Contact>>> GetB24Response(long id, B24Query query)
        {
            var res = await Connector
                .Contacts
                .ListAsync(query)
                .ConfigureAwait(false);
            return res;
        }
    }
}
