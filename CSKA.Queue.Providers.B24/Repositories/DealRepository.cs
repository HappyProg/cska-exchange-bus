﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;
using Bitrix24.Connector.Core;

namespace CSKA.Queue.Providers.B24
{
    public class DealRepository : B24Repository<Deal>, IDealProductAddition
    {
        private readonly List<Deal> _records;

        public DealRepository(B24Connector connector) : base(connector)
        {
            _records = new List<Deal>();
        }

        public override Task<Deal> GetEntityAsync(B24Query query, long id = default)
        {
            return Connector
                .Deals
                .FirstAsync(query);
        }

        public override Task<long> CreateEntityAsync(Deal entity)
        {
            return Connector
                .Deals
                .CreateAsync(entity);
        }

        public override Task<bool> UpdateEntityAsync(Deal entity)
        {
            return Connector
                .Deals
                .UpdateAsync(entity);
        }

        public override Task<bool> DeleteAsync(long id, string code = null)
        {
            return Connector.Deals.DeleteAsync(id);
        }

        public Task<bool> AddProducts(long dealId, IEnumerable<DealProduct> dealProducts)
        {
            return Connector.Deals
                .AddProducts(dealId, dealProducts);
        }

        protected override async Task<B24Response<IEnumerable<Deal>>> GetB24Response(long id, B24Query query)
        {
            var res = await Connector
                .Deals
                .ListAsync(query)
                .ConfigureAwait(false);

            return res;
        }
    }
}
