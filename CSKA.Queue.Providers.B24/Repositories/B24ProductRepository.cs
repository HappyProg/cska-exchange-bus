﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;
using Bitrix24.Connector.Core;

namespace CSKA.Queue.Providers.B24.Repositories
{
    public class B24ProductRepository : B24Repository<B24Product>
    {
        public B24ProductRepository(B24Connector connector) : base(connector)
        {
        }

        public override async Task<B24Product> GetEntityAsync(B24Query query, long id = default)
        {
            return await Connector.Products
                .FirstAsync(query)
                .ConfigureAwait(false);
        }

        public override async Task<long> CreateEntityAsync(B24Product entity)
        {
            return await Connector.Products
                .CreateAsync(entity)
                .ConfigureAwait(false);
        }

        public override async Task<bool> UpdateEntityAsync(B24Product entity)
        {
            return await Connector.Products
                .UpdateAsync(entity)
                .ConfigureAwait(false);
        }

        public override async Task<bool> DeleteAsync(long id, string code = null)
        {
            return await Connector.Products.DeleteAsync(id).ConfigureAwait(false);
        }

        protected override async Task<B24Response<IEnumerable<B24Product>>> GetB24Response(long id, B24Query query)
        {
            var res = await Connector
                .Products
                .ListAsync(query)
                .ConfigureAwait(false);
            return res;
        }
    }
}
