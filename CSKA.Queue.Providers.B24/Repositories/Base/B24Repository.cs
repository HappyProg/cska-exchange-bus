﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bitrix24.Connector;
using Bitrix24.Connector.Core;

namespace CSKA.Queue.Providers.B24
{
    public abstract class B24Repository<T> : IB24Repository<T> where T : EntityBase
    {
        protected readonly B24Connector Connector;

        protected B24Repository(B24Connector connector)
        {
            Connector = connector ?? throw new ArgumentNullException(nameof(connector));
        }

        public abstract Task<T> GetEntityAsync(B24Query query, long id = default);
        public abstract Task<long> CreateEntityAsync(T entity);
        public abstract Task<bool> UpdateEntityAsync(T entity);
        public abstract Task<bool> DeleteAsync(long id, string code = null);
        protected abstract Task<B24Response<IEnumerable<T>>> GetB24Response(long id, B24Query query);

        public virtual async Task<B24Response<IEnumerable<T>>> GetListEntityAsync(long id = default, B24Query query = null)
        {
            var res = await GetB24Response(id, query);
            return res;
        }

        public async Task<IEnumerable<T>> GetAllRecords(long id = default, B24Query query = null, List<T> records = null)
        {
            var response = await GetB24Response(id, query).ConfigureAwait(false);
            if (!response.Result.Any()) return new List<T>();
            if (records == null) records = new List<T>();
            records.AddRange(response.Result);
            if (records.Count == response.Total) return records;
            query = query.SetStart(records.Count + 1);
            await GetAllRecords(id, query, records).ConfigureAwait(false);
            return records;
        }

        public virtual async Task<long> GetCountRecords(long id = default, B24Query query = null)
        {
            var res = await GetB24Response(id, query).ConfigureAwait(false);
            return res.Total;
        }
    }
}
