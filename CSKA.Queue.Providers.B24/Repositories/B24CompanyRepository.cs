﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;
using Bitrix24.Connector.Core;

namespace CSKA.Queue.Providers.B24.Repositories
{
    public class B24CompanyRepository : B24Repository<B24Company>
    {
        public B24CompanyRepository(B24Connector connector) : base(connector)
        {
        }

        public override Task<B24Company> GetEntityAsync(B24Query query, long id = default)
        {
            return Connector
                .Companies
                .FirstAsync(query);
        }

        public override Task<long> CreateEntityAsync(B24Company entity)
        {
            return Connector
                .Companies
                .CreateAsync(entity); ;
        }

        public override Task<bool> UpdateEntityAsync(B24Company entity)
        {
            return Connector
                .Companies
                .UpdateAsync(entity);
        }

        public override Task<bool> DeleteAsync(long id, string code = null)
        {
            return Connector.Companies.DeleteAsync(id);
        }

        protected override async Task<B24Response<IEnumerable<B24Company>>> GetB24Response(long id, B24Query query)
        {
            var res = await Connector
                .Companies
                .ListAsync(query)
                .ConfigureAwait(false);
            return res;
        }
    }
}
