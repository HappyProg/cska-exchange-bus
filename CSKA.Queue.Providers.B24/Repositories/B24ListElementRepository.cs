﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;
using Bitrix24.Connector.Core;

namespace CSKA.Queue.Providers.B24.Repositories
{
    public class B24ListElementRepository : B24Repository<B24ListElement>
    {
        private readonly List<B24ListElement> _records;

        public B24ListElementRepository(B24Connector connector) : base(connector)
        {
            _records = new List<B24ListElement>();
        }

        public override Task<B24ListElement> GetEntityAsync(B24Query query, long id = default)
        {
            return Connector
                .UniversalLists
                .FirstAsync(id, query);
        }

        public override Task<long> CreateEntityAsync(B24ListElement entity)
        {
            return Connector
                .UniversalLists
                .CreateAsync(entity);
        }

        public override Task<bool> UpdateEntityAsync(B24ListElement entity)
        {
            return Connector
                .UniversalLists
                .UpdateAsync(entity);
        }

        public override Task<bool> DeleteAsync(long id, string code = null)
        {
            return Connector.UniversalLists.DeleteAsync(id, code);
        }

        protected override async Task<B24Response<IEnumerable<B24ListElement>>> GetB24Response(long id, B24Query query)
        {
            var res = await Connector
                .UniversalLists
                .ListAsync(id, query)
                .ConfigureAwait(false);

            return res;
        }
    }
}
