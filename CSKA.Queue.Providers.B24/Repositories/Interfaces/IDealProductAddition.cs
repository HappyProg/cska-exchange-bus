﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;

namespace CSKA.Queue.Providers.B24
{
    public interface IDealProductAddition : IB24Repository<Deal>
    {
        Task<bool> AddProducts(long dealId, IEnumerable<DealProduct> dealProducts);
    }
}
