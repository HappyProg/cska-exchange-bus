﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;
using Bitrix24.Connector.Core;

namespace CSKA.Queue.Providers.B24
{
    public interface IB24Repository<T> where T : EntityBase
    {
        Task<T> GetEntityAsync(B24Query query, long id = default);

        Task<long> CreateEntityAsync(T entity);

        Task<bool> UpdateEntityAsync(T entity);

        Task<bool> DeleteAsync(long id, string code = null);

        Task<B24Response<IEnumerable<T>>> GetListEntityAsync(long id = default, B24Query query = null);

        Task<IEnumerable<T>> GetAllRecords(long id = default, B24Query query = null, List<T> records = null);

        Task<long> GetCountRecords(long id = default, B24Query query = null);
    }
}
