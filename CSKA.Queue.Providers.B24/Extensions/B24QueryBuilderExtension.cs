﻿using System.Collections.Generic;
using Bitrix24.Connector;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Extensions
{
    public static class B24QueryBuilderExtension
    {
        public static void AddFilters(this B24QueryBuilder builder, IEnumerable<B24CustomFilter> customFilters)
        {
            if (customFilters == null) return;
            foreach (var customFilter in customFilters)
            {
                builder.AddFilter(customFilter.FieldName, customFilter.FieldValue, customFilter.Comparer);
            }
        }

        public static void AddSelects(this B24QueryBuilder builder, IEnumerable<string> selects)
        {
            if (selects == null) return;
            foreach (var select in selects)
            {
                builder.AddSelect(select);
            }
        }
    }
}
