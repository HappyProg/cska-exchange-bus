﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bitrix24.Connector;
using CSKA.Queue.Providers.B24.Helpers;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24.Queries
{
    public class B24PbiQuery
    {
        private readonly DealRepository _dealRepository;

        private readonly B24ListElementRepository _elementRepository;

        public B24PbiQuery(DealRepository dealRepository, B24ListElementRepository elementRepository)
        {
            _dealRepository = dealRepository;
            _elementRepository = elementRepository;
        }

        public Task<IEnumerable<Deal>> GetDeals(long eventId)
        {
            var query = B24QueryBuilder
                .Create()
                .AddSelect(Const.Fields.DealConst.PROP_TICKET_PRICE)
                .AddFilter(Const.Fields.DealConst.PROP_EVENT_ID, eventId)
                .AddFilter(Const.Fields.DealConst.PROP_CATEGORY_ID, Const.DealCategory.CATEGORY_OFFLINE_SALE)
                .AddFilter(Const.Fields.EvotorReceipts.RECEIPT_TYPE, Const.RECEIPT_TYPE.SELL)
                .Build();

            return _dealRepository.GetAllRecords(default, query);
        }

        public async Task<B24ListElement> GetNearestEvent(double matchProcessingTime)
        {
            var query = B24QueryBuilder
                .Create()
                .AddFilter(Const.Fields.B24ListElement.PROP_NAME_EVENT_DATE, DateTime.Now.AddHours(matchProcessingTime * -1), B24Comparer.Greater)
                .Build();
            var events = await _elementRepository
                .GetAllRecords(Const.UniversalLists.BLOCK_ID_EVENTS, query)
                .ConfigureAwait(false);

            return GetNearestEvent(events, matchProcessingTime);
        }

        private B24ListElement GetNearestEvent(IEnumerable<B24ListElement> events, double matchProcessingTime)
        {
            var nearestEvent = events
                .OrderBy(B24Helper.GetDateTime)
                .FirstOrDefault(e =>
                    B24Helper.GetDateTime(e).AddHours(matchProcessingTime) >= DateTimeOffset.Now.DateTime);
            return nearestEvent;
        }


        public Task<IEnumerable<Deal>> GetTickets(long eventId)
        {
            var query = B24QueryBuilder
                .Create()
                .AddFilter(Const.Fields.DealConst.PROP_EVENT_ID, eventId)
                .AddFilter(Const.Fields.DealConst.PROP_TICKET_STATE, Const.TICKET_STATE.SOLD)
                .AddFilter(Const.Fields.DealConst.PROP_CATEGORY_ID, new[] { Const.DealCategory.CATEGORY_TICKET_SALE, Const.DealCategory.CATEGORY_SUBSCRIPTION })
                .Build();
            return _dealRepository.GetAllRecords(default, query);
        }

        public Task<long> GetCountPassages(long eventId)
        {
            var query = B24QueryBuilder
                .Create()
                .AddFilter(Const.Fields.B24ListElement.PROP_NAME_TICKET_EVENT_ID, eventId)
                .Build();
            return _elementRepository.GetCountRecords(Const.UniversalLists.BLOCK_ID_PASSAGE, query);
        }
    }
}
