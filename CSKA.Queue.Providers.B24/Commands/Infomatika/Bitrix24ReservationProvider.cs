﻿using Bitrix24.Connector.Core;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using System;
using System.Threading.Tasks;
using CSKA.Queue.Providers.B24.Handlers;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24ReservationProvider : ICommandProvider<Transaction<Reservation>>
    {
        private readonly IBaseEntityHandler<Transaction<Reservation>> _handler;

        public Bitrix24ReservationProvider(ReservationHandler handler)
        {
            _handler = handler;
        }

        public async Task SendAsync(Transaction<Reservation> data)
        {
            try
            {
                if (data is null)
                {
                    throw new ArgumentNullException(nameof(data));
                }

                await _handler.Handle(data).ConfigureAwait(false);
            }
            catch (B24NotConnectedException ex)
            {
                throw new ProviderNotConnectedException(ex.Message);
            }
        }
    }
}
