﻿using Bitrix24.Connector.Core;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using System.Threading.Tasks;
using CSKA.Queue.Providers.B24.Handlers;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24CustomerProvider : ICommandProvider<Customer>
    {
        private readonly IBaseEntityHandler<Customer> _companyHandler;

        private readonly IBaseEntityHandler<Customer> _contactHandler;

        public Bitrix24CustomerProvider(CustomerCompanyHandler companyHandler, CustomerContactHandler contactHandler)
        {
            _companyHandler = companyHandler;
            _contactHandler = contactHandler;
        }

        public async Task SendAsync(Customer customer)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(customer.Email))
                {
                    await _companyHandler.Handle(customer).ConfigureAwait(false);
                }
                else
                {
                    await _contactHandler.Handle(customer).ConfigureAwait(false);
                }
            }
            catch (B24NotConnectedException ex)
            {
                throw new ProviderNotConnectedException(ex.Message);
            }
        }
    }
}
