﻿using Bitrix24.Connector.Core;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using System.Threading.Tasks;
using CSKA.Queue.Providers.B24.Handlers;
using App.Metrics;
using CSKA.Queue.WEB.Metrics;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24PassageProvider : ICommandProvider<Transaction<Passage>>
    {

        private readonly IBaseEntityHandler<Transaction<Passage>> _passageHandler;
        private readonly IMetrics _metrics;

        public Bitrix24PassageProvider(PassageHandler passageHandler, IMetrics metrics)
        {
            _passageHandler = passageHandler ?? throw new System.ArgumentNullException(nameof(passageHandler));
            _metrics = metrics ?? throw new System.ArgumentNullException(nameof(metrics));
        }

        public async Task SendAsync(Transaction<Passage> data)
        {
            try
            {
                // Метрика, просто увеличиваем счетчик 
                //
                _metrics.Measure.Counter.Increment(CskaMetrics.PassageCounter);
            }
            catch { }

            try
            {
                await _passageHandler.Handle(data).ConfigureAwait(false);
            }
            catch (B24NotConnectedException ex)
            {
                throw new ProviderNotConnectedException(ex.Message);
            }
        }
    }
}
