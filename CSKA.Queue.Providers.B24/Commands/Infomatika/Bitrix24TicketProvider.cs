﻿using Bitrix24.Connector.Core;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using System.Threading.Tasks;
using CSKA.Queue.Providers.B24.Handlers;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24TicketProvider : ICommandProvider<Transaction<Ticket>>
    {
        private readonly IBaseEntityHandler<Transaction<Ticket>> _handler;

        public Bitrix24TicketProvider(TicketHandler handler)
        {
            _handler = handler;
        }

        public async Task SendAsync(Transaction<Ticket> data)
        {
            try
            {
                await _handler.Handle(data).ConfigureAwait(false);
            }
            catch (B24NotConnectedException ex)
            {
                throw new ProviderNotConnectedException(ex.Message);
            }
        }

    }
}
