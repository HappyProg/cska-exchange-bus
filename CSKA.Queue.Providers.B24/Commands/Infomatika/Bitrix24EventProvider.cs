﻿using Bitrix24.Connector.Core;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using System.Threading.Tasks;
using CSKA.Queue.Providers.B24.Handlers;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24EventProvider : ICommandProvider<Event>
    {

        private readonly IEventHandler<Event> _eventHandler;

        public Bitrix24EventProvider( EventsHandler eventHandler)
        {
            _eventHandler = eventHandler;
        }

        public async Task SendAsync(Event data)
        {
            try
            {
                await _eventHandler.Handle(data).ConfigureAwait(false);
            }
            catch (B24NotConnectedException ex)
            {
                throw new ProviderNotConnectedException(ex.Message);
            }
        }
    }
}
