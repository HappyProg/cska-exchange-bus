﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;
using Timepad.API.Webhooks;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class TimepadHandler : IBaseEntityHandler<TimepadOrderWebhook>
    {
        private readonly IEntityGetter<TimepadOrderWebhook, B24Company> _companyGetter;

        private readonly IEntityGetter<TimepadOrderWebhook, B24Contact> _contactGetter;

        private readonly IEntityGetter<TimepadOrderWebhook, Deal> _dealGetter;

        private readonly IEntityGetter<TimepadOrderWebhookTicket, B24ListElement> _listElementGetter;

        private readonly IB24Repository<B24ListElement> _listElementRepository;

        private readonly IB24Repository<Deal> _dealRepository;

        private readonly IMapper _mapper;

        public TimepadHandler(IMapper mapper, B24CompanyRepository companyRepository, B24ContactRepository contactRepository, DealRepository dealRepository, B24ListElementRepository listElementRepository)
        {
            _listElementRepository = listElementRepository;
            _dealRepository = dealRepository;
            _contactGetter = new EntityGetter<TimepadOrderWebhook, B24Contact, TimepadContactQuerySettings>(contactRepository);
            _companyGetter = new EntityGetter<TimepadOrderWebhook, B24Company, TimepadCompanyQuerySettings>(companyRepository);
            _dealGetter = new EntityGetter<TimepadOrderWebhook, Deal, TimepadDealQuerySettings>(dealRepository);
            _listElementGetter = new EntityGetter<TimepadOrderWebhookTicket, B24ListElement, TimepadTicketB24ListSettingsQuery>(listElementRepository);
            _mapper = mapper;
        }

        public async Task Handle(TimepadOrderWebhook entity)
        {
            B24Company company = null;
            var contact = await _contactGetter.GetEntity(entity).ConfigureAwait(false);
            if (contact == null)
            {
                company = await _companyGetter.GetEntity(entity).ConfigureAwait(false);
            }
            if (contact == null && company == null)
            {
                throw new ProviderOperationException($"[Timepad]: Не удалось определить контакт / компанию для: \"{entity.Mail}\"");
            }
            var newDeal = await HandleDeal(entity, company, contact).ConfigureAwait(false);
            await HandleTicket(entity, newDeal).ConfigureAwait(false);
        }

        private async Task<Deal> HandleDeal(TimepadOrderWebhook entity, B24Company company, B24Contact contact)
        {
            var deal = await _dealGetter.GetEntity(entity).ConfigureAwait(false);
            var newDeal = _mapper.Map<TimepadOrderWebhook, Deal>(entity);
            if (contact != null && contact.Id > 0)
            {
                // Контакт
                //
                newDeal.SetValue(Const.Fields.DealConst.PROP_CONTACT_IDS, new[] { contact.Id });
            }

            if (company != null && company.Id > 0)
            {
                newDeal.SetValue(Const.Fields.DealConst.PROP_COMPANY_ID, company.Id);
            }

            if (deal == null)
            {
                newDeal = await CreateDeal(newDeal).ConfigureAwait(false);
            }
            else
            {
                newDeal.Id = deal.Id;
                newDeal = await UpdateDeal(newDeal).ConfigureAwait(false);
            }

            return newDeal;
        }

        private async Task HandleTicket(TimepadOrderWebhook entity, Deal deal)
        {

            foreach (var ticket in entity.Tickets)
            {
                var b24ListElement = await _listElementGetter
                    .GetEntity(ticket, Const.UniversalLists.BLOCK_ID_TIMEPAD_TICKETS).ConfigureAwait(false);
                var newB24ListElement = _mapper.Map<TimepadOrderWebhook, B24ListElement>(entity);
                newB24ListElement.SetValue(Const.Fields.B24ListElement.PROP_NAME_TICKET_ID, ticket.Id);
                newB24ListElement.SetValue(Const.Fields.B24ListElement.PROP_NAME_TIMEPAD_DEAL_ID, deal.Id);

                if (ticket.Attendance?.StartsAt != default && ticket.Attendance?.StartsAt != DateTime.MinValue)
                {
                    newB24ListElement.SetValue(Const.Fields.B24ListElement.PROP_NAME_START_AT, ticket.Attendance.StartsAt);
                }
                await CreateOrUpdateTicket(newB24ListElement, b24ListElement).ConfigureAwait(false);
            }
        }

        private async Task CreateOrUpdateTicket(B24ListElement newB24ListElement, B24ListElement b24ListElement)
        {
            if (b24ListElement == null)
            {
                newB24ListElement.Id = await _listElementRepository.CreateEntityAsync(newB24ListElement)
                    .ConfigureAwait(false);
            }
            else
            {
                newB24ListElement.Id = b24ListElement.Id;
                newB24ListElement.BlockId = Const.UniversalLists.BLOCK_ID_TIMEPAD_TICKETS;
                var result = await _listElementRepository.UpdateEntityAsync(newB24ListElement)
                    .ConfigureAwait(false);
                if (result == false)
                {
                    throw new ProviderOperationException("[Timepad]: Не удалось обновить элемент списка билеты!");
                }
            }
        }

        private async Task<Deal> UpdateDeal(Deal deal)
        {
            var result = await _dealRepository.UpdateEntityAsync(deal).ConfigureAwait(false);

            if (result == false)
            {
                throw new ProviderOperationException($"Не удалось обновить сделку. id {deal.Id}, {deal.Title}");
            }

            return deal;

        }

        private async Task<Deal> CreateDeal(Deal deal)
        {
            deal.Id = await _dealRepository.CreateEntityAsync(deal).ConfigureAwait(false);

            if (deal.Id <= 0)
            {
                throw new ProviderOperationException($"Не удалось создать сделку. id {deal.Id}, {deal.Title}");
            }

            return deal;
        }
    }
}
