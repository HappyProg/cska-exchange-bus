﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Helpers;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class TicketHandler : IBaseEntityHandler<Transaction<Ticket>>
    {
        private readonly IEntityGetter<Transaction<Ticket>, Deal> _refundDealGetter;

        private readonly IEntityGetter<Transaction<Ticket>, Deal> _dealGetter;

        private readonly IEntityGetter<Customer, B24Company> _companyGetter;

        private readonly IEntityGetter<Customer, B24Contact> _contactGetter;

        private readonly IEntityGetter<Transaction<Ticket>, B24ListElement> _b24ListSeatGetter;

        private readonly IEntityGetter<Transaction<Ticket>, B24ListElement> _b24ListPassageGetter;

        private readonly IB24Repository<Deal> _dealRepository;

        private readonly IB24Repository<B24ListElement> _b24ListRepository;

        private readonly IEventHandler<Event> _eventsHandler;

        private readonly IMapper _mapper;
        private readonly Bitrix24Configuration _cfg;


        public TicketHandler(Bitrix24Configuration cfg, IMapper mapper, DealRepository dealRepository, B24ListElementRepository b24ListRepository, B24CompanyRepository companyRepository, B24ContactRepository contactRepository,  EventsHandler eventsHandler)
        {
            _mapper = mapper;
            _eventsHandler = eventsHandler;
            _dealRepository = dealRepository;
            _b24ListRepository = b24ListRepository;
            _companyGetter = new EntityGetter<Customer, B24Company, TicketCompanySettingsQuery>(companyRepository);
            _contactGetter = new EntityGetter<Customer, B24Contact, TicketContactSettingsQuery>(contactRepository);
            _dealGetter = new EntityGetter<Transaction<Ticket>, Deal, TicketDealSettingsQuery>(dealRepository);
            _refundDealGetter = new EntityGetter<Transaction<Ticket>,Deal, TicketRefundDealSettingsQuery>(dealRepository);
            _b24ListSeatGetter = new EntityGetter<Transaction<Ticket>,B24ListElement, TicketSeatB24ListSettingsQuery>(b24ListRepository);
            _b24ListPassageGetter = new EntityGetter<Transaction<Ticket>,B24ListElement, TicketPassageB24ListSettingsQuery>(b24ListRepository);
            _cfg = cfg ?? throw new System.ArgumentNullException(nameof(cfg));
        }

        public async Task Handle(Transaction<Ticket> entity)
        {
            // Костыль: исключаем тестовые мероприятия по наименованию
            // TODO: Найти способ как исключить их всех, 
            // даже если в имени нет слова тест
            //
            if (entity.Data.Event.EventName.ToUpper().Contains("ТЕСТ"))
            {
                return;
            }

            var transaction = entity;
            var ticket = transaction.Data;
            if (entity.Data.TicketState != TicketState.Sold)
            {
                await Refund(transaction).ConfigureAwait(false);
                return;
            }
            if (string.IsNullOrWhiteSpace(ticket.Barcode))
            {
                throw new ProviderCriticalException($"Отсутсвует ШК билета: матч {ticket.Event.EventId}, клиент: {transaction.Customer?.Id}");
            }
            B24Company company = null;
            var contact = await _contactGetter.GetEntity(entity.Customer).ConfigureAwait(false);
            if (contact == null)
            { 
                company = await _companyGetter.GetEntity(entity.Customer).ConfigureAwait(false);
            }

            if (company == null && contact == null)
            {
                throw new ProviderOperationException($"Не удалось определить контакт / компанию для: \"{transaction.Customer.Name}\"", transaction.Customer);
            }

            var eventId = await _eventsHandler.GetEventId(ticket.Event).ConfigureAwait(false);
            var deal = await HandleDeal(transaction, eventId, contact, company).ConfigureAwait(false);
            await HandleSeat(transaction, deal).ConfigureAwait(false);
            await HandlePassage(transaction, deal).ConfigureAwait(false);
        }

        private async Task HandlePassage(Transaction<Ticket> transaction, Deal deal)
        {
            var passage = await _b24ListPassageGetter.GetEntity(transaction, Const.UniversalLists.BLOCK_ID_PASSAGE)
                .ConfigureAwait(false);
            if (passage != null &&
                passage.GetValueDefault<long>(Const.Fields.B24ListElement.PROP_NAME_DEAL_ID) != deal.Id)
            {
                passage.SetValue(Const.Fields.B24ListElement.PROP_NAME_DEAL_ID, deal.Id);
                passage.BlockId = Const.UniversalLists.BLOCK_ID_PASSAGE;
                var result = await _b24ListRepository.UpdateEntityAsync(passage).ConfigureAwait(false);
                if (result == false)
                {
                    throw new ProviderOperationException($"Ошибка обновления прохода для билета с ШК {transaction.Data.Barcode}, Матч - {transaction.Data.Event.EventName} (id - {transaction.Data.Event.EventId})");
                }
            }
        }

        private async Task Refund(Transaction<Ticket> ticket)
        {
            var deal = await _refundDealGetter.GetEntity(ticket).ConfigureAwait(false);
            if (deal == null)
            {
                throw new ProviderCriticalException($"Ошибка обработки возврата: не удалось найти сделку ticket id - \"{ticket.Id}\"");
            }
            deal.SetValue(Const.Fields.DealConst.PROP_TICKET_STATE, TicketHelper.ConvertTicketSate(ticket.Data.TicketState));
            var result = await _dealRepository.UpdateEntityAsync(deal).ConfigureAwait(false);

            if (result == false)
            {
                throw new ProviderOperationException($"Ошибка обработки возврата / удаления транзакции: не удалось обновить сделку Id - \"{deal.Id}\"");
            }
            
        }

        private decimal GetCityCardBonusAmount(Ticket ticket)
        {
            if (string.IsNullOrWhiteSpace(_cfg.BonusDiscountName))
            {
                return 0;
            }

            return ticket.Payments
                .FirstOrDefault(x => x.Name.ToUpper() == _cfg.BonusDiscountName.ToUpper())
                ?.Amount ?? 0;
        }

        private async Task<Deal> HandleDeal(Transaction<Ticket> ticket, long eventId, B24Contact contact,
            B24Company company)
        {
            var deal = await _dealGetter.GetEntity(ticket).ConfigureAwait(false);
            var newDeal = _mapper.Map<Transaction<Ticket>, Deal>(ticket);
            newDeal.Title = GetTitleForDeal(ticket, contact, company);
            newDeal[Const.Fields.DealConst.PROP_CITYCARD_BONUS_AMOUNT] = GetCityCardBonusAmount(ticket.Data);
            if (contact != null && contact.Id > 0)
            {
                // Контакт
                //
                newDeal.SetValue(Const.Fields.DealConst.PROP_CONTACT_IDS, new[] { contact.Id });
            }

            if (company != null && company.Id > 0)
            {
                newDeal.SetValue(Const.Fields.DealConst.PROP_COMPANY_ID, company.Id);
            }

            SetEventDateEndForDeal(newDeal, ticket.Data.Event, eventId);

            if (deal == null)
            {
                newDeal.Id = await _dealRepository.CreateEntityAsync(newDeal).ConfigureAwait(false);
            }
            else
            {
                newDeal.Id = deal.Id;
                await _dealRepository.UpdateEntityAsync(newDeal).ConfigureAwait(false);
            }

            return newDeal;
        }


        private async Task HandleSeat(Transaction<Ticket> ticket, Deal deal)
        {
            var newB24List = _mapper.Map<Ticket, B24ListElement>(ticket.Data);
            var b24List = await _b24ListSeatGetter.GetEntity(ticket, Const.UniversalLists.BLOCK_ID_SEATS).ConfigureAwait(false);
            if (b24List == null || b24List.Id < 1)
            {
                newB24List[Const.Fields.B24ListElement.PROP_NAME_LINKED_DEAL_ID] = deal.Id;
                newB24List.Id = await _b24ListRepository.CreateEntityAsync(newB24List).ConfigureAwait(false);
            }
            else
            {
                newB24List.Id = b24List.Id;
                var linkedDealId = b24List.GetValueDefault<long>(Const.Fields.B24ListElement.PROP_NAME_LINKED_DEAL_ID);
               
                var isPurchased = b24List.GetValueDefault<long>(Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEATS_BOOKING);
                if (linkedDealId != deal.Id || (ticket.Data.BookingId > 0 && isPurchased != Const.UniversalLists.SEATS_BOOKING_BUY_FIELD_Y))
                {
                    await UpdateSeat(b24List, deal).ConfigureAwait(false);
                }
            }

            if (newB24List.Id < 1)
            {
                throw new ProviderOperationException($"Не удалось создать / обновить елемент списка \"Место мероприятия для билета\"!", newB24List);
            }
        }

        private async Task UpdateSeat(B24ListElement b24List, Deal deal)
        {
            b24List.SetValue(Const.Fields.B24ListElement.PROP_NAME_LINKED_DEAL_ID, deal.Id);
            b24List.SetValue(Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEATS_BOOKING, Const.UniversalLists.SEATS_BOOKING_BUY_FIELD_Y);
            b24List.BlockId = Const.UniversalLists.BLOCK_ID_SEATS;
            var result = await _b24ListRepository.UpdateEntityAsync(b24List).ConfigureAwait(false);

            if (result == false)
            {
                throw new ProviderOperationException($"Ошибка обновления места для сделки {deal.Id}!");
            }
        }

        private void SetEventDateEndForDeal(Deal newDeal, Event ev, long eventId)
        {
            switch (ev.EventType)
            {
                case EventType.Game:
                    newDeal.SetValue(Const.Fields.DealConst.PROP_EVENT_ID, eventId);
                    break;
                case EventType.Subscription:
                    // Дата окончания мероприятия
                    //
                    newDeal.SetValue(Const.Fields.DealConst.PROP_EVENT_SUBSCRIPTION_DATE_END, ev.DateEnd);
                    newDeal.SetValue(Const.Fields.DealConst.PROP_EVENT_SUBSCRIPTION_ID, eventId);
                    break;
                case EventType.Batch:
                    // Дата окончания мероприятия
                    //
                    newDeal.SetValue(Const.Fields.DealConst.PROP_EVENT_BATCH_DATE_END, ev.DateEnd);
                    newDeal.SetValue(Const.Fields.DealConst.PROP_EVENT_BATCH_ID, eventId);
                    break;
            }
        }

        private string GetTitleForDeal(Transaction<Ticket> ticket, B24Contact contact, B24Company company)
        {
            return (contact == null ? company.Title : contact.GetValue(Const.Fields.B24ContactConst.PROP_LAST_NAME)
                                                      + " " + contact.GetValue(Const.Fields.B24ContactConst.PROP_NAME) + " "
                                                      + contact.GetValue(Const.Fields.B24ContactConst.PROP_SECOND_NAME)) + " / " + ticket.Data.Event.EventName;
        }
    }
}
