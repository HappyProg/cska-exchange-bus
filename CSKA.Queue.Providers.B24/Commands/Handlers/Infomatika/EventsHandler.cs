﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class EventsHandler : IEventHandler<Event>
    {
        private readonly IEntityGetter<Event, B24ListElement> _eventGetter;

        private readonly IEntityGetter<Event, B24ListElement> _batchGetter;

        private readonly IEntityGetter<Event, B24ListElement> _subscriptionGetter;

        private readonly IB24Repository<B24ListElement> _repository;

        private readonly Dictionary<Event, long> _currentEvents;

        private readonly IMapper _mapper;

        public EventsHandler(IMapper mapper, B24ListElementRepository repository)
        {
            _repository = repository;
            _mapper = mapper;
            _eventGetter = new EntityGetter<Event, B24ListElement, EventSettingsQuery>(repository);
            _batchGetter = new EntityGetter<Event, B24ListElement, EventBatchSettingsQuery>(repository);
            _subscriptionGetter = new EntityGetter<Event,B24ListElement, EventSubscriptionSettingsQuery>(repository);
            _currentEvents = new Dictionary<Event, long>();
        }

        public Task Handle(Event entity)
        {
            // Костыль: исключаем тестовые мероприятия по наименованию
            // TODO: Найти способ как исключить их всех, 
            // даже если в имени нет слова тест
            //
            if (entity.EventName.ToUpper().Contains("ТЕСТ"))
            {
                return Task.CompletedTask;
            }
            return GetEventId(entity);
        }

        public async Task<long> GetEventId(Event entity)
        { 
            switch (entity.EventType)
            {
                case EventType.Batch:
                    return await HandleEventBatch(entity).ConfigureAwait(false);
                case EventType.Subscription:
                    return await HandleSubscriptionBatch(entity).ConfigureAwait(false);
                default:
                    return await HandleEvent(entity).ConfigureAwait(false);
            }
        }

        private async Task<long> HandleEventBatch(Event e)
        {
            var element = await _batchGetter.GetEntity(e, Const.UniversalLists.BLOCK_ID_EVENTS_GAME).ConfigureAwait(false);
            var linkedEvents = await HandleLinkedEvents(e.LinkedEvents).ConfigureAwait(false);
            var newElement = _mapper.Map<B24ListElement>(e);
            newElement.SetValue(Const.Fields.B24ListElement.PROP_NAME_LINKED_EVENTS_BATCH, linkedEvents);
            return await CreateOrUpdatesEvents(element, newElement).ConfigureAwait(false);
        }

        private async Task<long> HandleSubscriptionBatch(Event e)
        {
            var element = await _subscriptionGetter.GetEntity(e, Const.UniversalLists.BLOCK_ID_EVENTS_SUBSCRIPTION).ConfigureAwait(false);
            var linkedEvents = await HandleLinkedEvents(e.LinkedEvents).ConfigureAwait(false);
            var newElement = _mapper.Map<B24ListElement>(e);
            newElement.SetValue(Const.Fields.B24ListElement.PROP_NAME_LINKED_EVENTS_SUBSCRIPTION, linkedEvents);
            return await CreateOrUpdatesEvents(element, newElement).ConfigureAwait(false);
        }

        private async Task<long> CreateOrUpdatesEvents(B24ListElement element, B24ListElement newElement)
        {
            if (element == null)
            {
                var id = await _repository.CreateEntityAsync(newElement).ConfigureAwait(false);
                return id;
            }
            newElement.Id = element.Id;
            newElement.ElementCode = element.ElementCode;
            var res = await _repository.UpdateEntityAsync(newElement).ConfigureAwait(false);
            return newElement.Id;
        }

        /// <summary>
        /// Сравнение статусов Б24 и инфоматики
        /// </summary>
        /// <param name="e"></param>
        /// <param name="el"></param>
        /// <returns></returns>
        private bool EventStateEquals(Event e, int status)
        {
            // value:64 (отменен)
            // value:65 (тест)
            // value:66 (действительный)
            // value:67 (удален)
            //
            switch (status)
            {
                case 67:
                    return e.IsDelete;
                case 65:
                case 64:
                    return e.IsActive == false;
                case 66:
                    return e.IsActive;
                default:
                    throw new NotSupportedException($"Неизвестный тип значения статус билета - {status}!"); ;
            }
        }

        private bool Equals(Event e, B24ListElement element)
        {
            // Текущее кол-во свободных мест 
            //
            var seatsCountFreeElement = element?.GetValueDefault<int>(Const.Fields.B24ListElement.PROP_NAME_EVENT_SEATS_COUNT_FREE) ?? 0;
            // Текущий статус матча
            //
            var statusElement = element?.GetValueDefault<int>(Const.Fields.B24ListElement.PROP_NAME_EVENT_STATUS) ?? 66;

            return element != null && seatsCountFreeElement >= e.SeatsCountFree
                && element.Name == e.EventName && EventStateEquals(e, statusElement);
        }

        private async Task<long> HandleEvent(Event e)
        {
            var element = await _eventGetter.GetEntity(e, Const.UniversalLists.BLOCK_ID_EVENTS).ConfigureAwait(false);

            //if (element != null && element.GetValueDefault<int>(Const.Fields.B24ListElement.PROP_NAME_EVENT_ID) == 749)
            //{
            //}

            //if (element != null && element.GetValueDefault<int>(Const.Fields.B24ListElement.PROP_NAME_EVENT_SEATS_COUNT_FREE) >= e.SeatsCountFree)
            //{
            //    return element.Id;
            //}

            // Если элемент не существует или требуется обновление
            //
            if (Equals(e, element) == false)
            {
                var newElement = _mapper.Map<Event, B24ListElement>(e);

                // Если текущее значение в Б24 уже заполнено то счтаем что это начальное кол-во мест
                // и больше не заполняем
                //
                var curFreeCount = element == null ? 0 : element.GetValueDefault<int>(Const.Fields.B24ListElement.PROP_NAME_EVENT_SEATS_COUNT_FREE);
                // Берем наибольшее и считаем что это всего свободных мест т.к.
                // в инфоатике это значение постоянно меняется по мере продажи билетов
                //
                newElement[Const.Fields.B24ListElement.PROP_NAME_EVENT_SEATS_COUNT_FREE] = Math.Max(curFreeCount, e.SeatsCountFree);

                var id = await CreateOrUpdatesEvents(element, newElement);

                return id;
            }

            return element.Id;
        }

        private async Task<long[]> HandleLinkedEvents(IEnumerable<Event> events)
        {
            var result = new List<long>();
            foreach (var e in events)
            {
                long id;
                var (key, value) = _currentEvents
                    .FirstOrDefault(x => x.Key.EventId == e.EventId);

                if (key == null)
                {
                    id = await HandleEvent(e).ConfigureAwait(false);
                    _currentEvents.Add(e, id);

                }
                else
                {
                    id = value;
                }
                result.Add(id);
            }

            return result.ToArray();
        }
    }
}
