﻿using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Query;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class CustomerCompanyHandler : IBaseEntityHandler<Customer>
    {
        private readonly IEntityGetter<Customer, B24Company> _getter;

        private readonly IB24Repository<B24Company> _repository;

        private readonly IMapper _mapper;

        public CustomerCompanyHandler(IMapper mapper, B24CompanyRepository repository)
        {
            _repository = repository;
            _mapper = mapper;
            _getter = new EntityGetter<Customer, B24Company, CustomerCompanySettingsQuery>(repository);
        }

        public async Task Handle(Customer entity)
        {
            var b24Company = await _getter.GetEntity(entity);
            if (b24Company == null)
            {
                await CreateCompany(entity).ConfigureAwait(false);
            }
            else
            {
                await UpdateCompany(b24Company, entity).ConfigureAwait(false);
            }
        }

        private async Task CreateCompany(Customer customer)
        {
            var newB24Company = _mapper.Map<Customer, B24Company>(customer);
            var id = await _repository.CreateEntityAsync(newB24Company).ConfigureAwait(false);
            if (id < 1)
            {
                throw new ProviderOperationException($"Ошибка при добавлении новой компании: {customer.Name}",
                    customer);
            }
        }

        private async Task UpdateCompany(B24Company company, Customer customer)
        {
            company = _mapper.Map(customer, company);
            var result = await _repository.UpdateEntityAsync(company).ConfigureAwait(false);
            if (!result)
            {
                throw new ProviderOperationException($"Ошибка при добавлении новой компании: {customer.Name}", customer);
            }
        }
    }
}
