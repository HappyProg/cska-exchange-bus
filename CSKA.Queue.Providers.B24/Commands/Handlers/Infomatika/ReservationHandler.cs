﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class ReservationHandler : IBaseEntityHandler<Transaction<Reservation>>
    {
        private readonly IEntityGetter<Transaction<Reservation>, Deal> _dealGetter;

        private readonly IEntityGetter<Transaction<Reservation>, B24ListElement> _reservationGetter;

        private readonly IEntityGetter<Transaction<Reservation>, B24ListElement> _cancelReservationGetter;

        private readonly IEntityGetter<Transaction<Reservation>, B24ListElement> _linkedSeatsGetter;

        private readonly IB24Repository<B24ListElement> _b24ListRepository;

        private readonly IB24Repository<Deal> _dealRepository;

        private readonly IMapper _mapper;

        public ReservationHandler(IMapper mapper, B24ListElementRepository b24ListRepository, DealRepository dealRepository)
        {
            _dealGetter = new EntityGetter<Transaction<Reservation>, Deal, ReservationDealSettingsQuery>(dealRepository);
            _reservationGetter = new EntityGetter<Transaction<Reservation>, B24ListElement, ReservationB24ListSettingsQuery>(b24ListRepository);
            _linkedSeatsGetter = new EntityGetter<Transaction<Reservation>, B24ListElement, LinkedSeatsSettingsQuery>(b24ListRepository);
            _cancelReservationGetter = new EntityGetter<Transaction<Reservation>, B24ListElement, CancelReservationSettingsQuery>(b24ListRepository);
            _b24ListRepository = b24ListRepository;
            _dealRepository = dealRepository;
            _mapper = mapper;
        }

        public async Task Handle(Transaction<Reservation> entity)
        {
            // Костыль: исключаем тестовые мероприятия по наименованию
            // TODO: Найти способ как исключить их всех, 
            // даже если в имени нет слова тест
            //
            if (entity.Data.Event.EventName.ToUpper().Contains("ТЕСТ"))
            {
                return;
            }

            var newDeal = await HandleDeal(entity).ConfigureAwait(false);
            if (newDeal == null)
                return;
            await HandleSeat(entity, newDeal).ConfigureAwait(false);
        }

        private async Task<Deal> HandleDeal(Transaction<Reservation> entity)
        {
            var deal = await _dealGetter
                .GetEntity(entity)
                .ConfigureAwait(false);

            var newDeal = _mapper.Map<Transaction<Reservation>, Deal>(entity);
            if (deal == null && entity.Data.Status != ReservationStatus.Reserve) return null;
            if (entity.Data.Status != ReservationStatus.Reserve)
            {
                if (deal == null) return null;
                newDeal.Id = deal.Id;
                await CancelSeatReservation(entity, deal).ConfigureAwait(false);
                return null;
            }
            if (deal == null || deal.Id < 1)
            {
                newDeal.SetValue(Const.Fields.DealConst.PROP_RESERVATION_STATUS, GetReservationStatus(ReservationStatus.Reserve));
                newDeal.Id = await _dealRepository.CreateEntityAsync(newDeal).ConfigureAwait(false);
            }
            else if (deal.Compare(newDeal) == false)
            {
                newDeal.Id = deal.Id;
                var result = await _dealRepository.UpdateEntityAsync(newDeal).ConfigureAwait(false);
                if (result == false)
                {
                    throw new ProviderOperationException($"Не удалось обновить сделку: {newDeal.Title}");
                }
            }
            if (newDeal.Id < 1)
            {
                throw new ProviderOperationException($"Не корректный идентификатор сделки: {newDeal.Id} для \"{deal?.Title}\"");
            }

            return newDeal;
        }

        private async Task HandleSeat(Transaction<Reservation> entity, Deal newDeal)
        {
            var seat = await _reservationGetter.GetEntity(entity, Const.UniversalLists.BLOCK_ID_SEATS).ConfigureAwait(false);
            if (seat == null)
            {
                var newSeat = _mapper.Map<Transaction<Reservation>, B24ListElement>(entity);
                newSeat.SetValue(Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_DEAL_ID, newDeal.Id);
                newSeat.Id = await _b24ListRepository.CreateEntityAsync(newSeat).ConfigureAwait(false);
                if (newSeat.Id < 1)
                {
                    throw new ProviderOperationException($"Не удалось создать елемент списка \"Место мероприятия для бронирования\"!");
                }
            }
            else
            {
                var linkedDealId = seat.GetValueDefault<long>(Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_DEAL_ID);
                if (linkedDealId != newDeal.Id)
                {
                    seat.SetValue(Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_DEAL_ID, newDeal.Id);
                    seat.BlockId = Const.UniversalLists.BLOCK_ID_SEATS;

                    var result = await _b24ListRepository.UpdateEntityAsync(seat).ConfigureAwait(false);
                    if (result == false)
                    {
                        throw new ProviderOperationException($"Ошибка обновления места для сделки {newDeal.Id}!");
                    }
                }
            }
        }

        private async Task CancelSeatReservation(Transaction<Reservation> data, Deal deal)
        {
            var seat = await _cancelReservationGetter.GetEntity(data, Const.UniversalLists.BLOCK_ID_SEATS).ConfigureAwait(false);
            var reserveDealId = seat?.GetValueDefault<long>(Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_DEAL_ID);
            if (seat != null && reserveDealId == deal.Id)
            {
                seat.SetValue(Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_DEAL_ID, "");
                seat.BlockId = Const.UniversalLists.BLOCK_ID_SEATS;

                var result = await _b24ListRepository.UpdateEntityAsync(seat).ConfigureAwait(false);
                if (result == false)
                {
                    throw new ProviderOperationException($"Ошибка отмены брони для места сектор {data.Data.Seat.SectorName}, ряд {data.Data.Seat.Row}, место {data.Data.Seat.Num}!");
                }
            }

            var linkedSeats = await _linkedSeatsGetter.GetEntities(data, Const.UniversalLists.BLOCK_ID_SEATS)
                .ConfigureAwait(false);
            if (!linkedSeats.Any())
            {
                deal.SetValue(Const.Fields.DealConst.PROP_RESERVATION_STATUS, GetReservationStatus(data.Data.Status));
                var result = await _dealRepository.UpdateEntityAsync(deal).ConfigureAwait(false);
                if (result == false)
                {
                    throw new ProviderOperationException($"Не удалось обновить сделку: {deal.Title}", deal);
                }
            }
        }

        private string GetReservationStatus(ReservationStatus status)
        {
            switch (status)
            {
                case ReservationStatus.Reserve:
                    return "101";
                case ReservationStatus.Cancel:
                    return "102";
                case ReservationStatus.AutoCancel:
                    return "103";
                default:
                    throw new InvalidCastException($"Тип статуса брони не поддерживается! \"{status}\"");
            }
        }
    }
}
