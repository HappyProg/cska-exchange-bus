﻿using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class CustomerContactHandler : IBaseEntityHandler<Customer>
    {
        private readonly IEntityGetter<Customer, B24Contact> _getter;

        private readonly IB24Repository<B24Contact> _repository;

        private readonly IMapper _mapper;

        private readonly IBaseEntityHandler<Customer> _companyHandler;

        public CustomerContactHandler(IMapper mapper, B24ContactRepository repository, CustomerCompanyHandler companyHandler)
        {
            _repository = repository;
            _mapper = mapper;
            _companyHandler = companyHandler;
            _getter = new EntityGetter<Customer, B24Contact, CustomerContactSettingsQuery>(repository);
        }

        public async Task Handle(Customer entity)
        {
            var b24Contact = await _getter.GetEntity(entity)
                .ConfigureAwait(false);
            // Если нет в Б24 - Значит создаем компанию
            //
            if (b24Contact == null)
            {
                await _companyHandler.Handle(entity).ConfigureAwait(false);
                return;
            }
            await UpdateContact(entity, b24Contact).ConfigureAwait(false);
        }

        private async Task UpdateContact(Customer customer, B24Contact contact)
        {
            var updatedContact = _mapper.Map(customer, contact);
            var result = await _repository.UpdateEntityAsync(updatedContact).ConfigureAwait(false);
            if (!result)
            {
                throw new ProviderOperationException($"Не удалось обновить контакт, Bitrix24 ID - {updatedContact.Id}", customer);
            }
        }
    }
}
