﻿using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class PassageHandler : IBaseEntityHandler<Transaction<Passage>>
    {
        private readonly IEntityGetter<Transaction<Passage>, Deal> _dealGetter;

        private readonly IEntityGetter<Customer, B24Company> _companyGetter;

        private readonly IEntityGetter<Customer, B24Contact> _contactGetter;

        private readonly IEntityGetter<Transaction<Passage>, B24ListElement> _passageGetter;

        private readonly IB24Repository<B24ListElement> _repository;

        private readonly IMapper _mapper;

        public PassageHandler(IMapper mapper, B24ListElementRepository repository, DealRepository dealRepository, B24CompanyRepository companyRepository, B24ContactRepository contactRepository)
        {
            _dealGetter = new EntityGetter<Transaction<Passage>, Deal, PassageDealSettingsQuery>(dealRepository);
            _companyGetter = new EntityGetter<Customer, B24Company, PassageCompanySettingsQuery>(companyRepository);
            _contactGetter = new EntityGetter<Customer,B24Contact, PassageContactSettingsQuery>(contactRepository);
            _passageGetter = new EntityGetter<Transaction<Passage>,B24ListElement, PassageB24ListSettingsQuery>(repository);
            _repository = repository;
            _mapper = mapper;
        }

        public async Task Handle(Transaction<Passage> entity)
        {
            // Костыль: исключаем тестовые мероприятия по наименованию
            // TODO: Найти способ как исключить их всех, 
            // даже если в имени нет слова тест
            //
            if (entity.Data.Ticket.Event.EventName.ToUpper().Contains("ТЕСТ"))
            {
                return;
            }
            

            var deal = await _dealGetter.GetEntity(entity).ConfigureAwait(false);
            var dealId = deal?.Id;
            var contactOrCompanyId = await GetContactIdOrCompanyId(entity).ConfigureAwait(false);
            var passage = await _passageGetter.GetEntity(entity, Const.UniversalLists.BLOCK_ID_PASSAGE).ConfigureAwait(false);
            if (passage == null)
            {
                var newPassage = _mapper.Map<Transaction<Passage>, B24ListElement>(entity);
                if (dealId != default)
                {
                    newPassage.SetValue(Const.Fields.B24ListElement.PROP_NAME_DEAL_ID, dealId);
                }
                newPassage.SetValue(Const.Fields.B24ListElement.PROP_NAME_CONTACT_OR_COMPANY_ID, contactOrCompanyId.Item2 ? "CO_" + contactOrCompanyId.Item1 : "C_" + contactOrCompanyId.Item1);
                await _repository.CreateEntityAsync(newPassage).ConfigureAwait(false);
            }
            else if (dealId != default && passage.GetValueDefault<long>(Const.Fields.B24ListElement.PROP_NAME_DEAL_ID) != dealId)
            {
                passage.SetValue(Const.Fields.B24ListElement.PROP_NAME_DEAL_ID, dealId);
                passage.BlockId = Const.UniversalLists.BLOCK_ID_PASSAGE;
                await _repository.UpdateEntityAsync(passage).ConfigureAwait(false);
            }
        }

        private async Task<(long, bool)> GetContactIdOrCompanyId(Transaction<Passage> entity)
        {
            var contact = await _contactGetter.GetEntity(entity.Customer).ConfigureAwait(false);
            if (contact != null) return (contact.Id, false);
            var company = await _companyGetter.GetEntity(entity.Customer).ConfigureAwait(false);
            if (company == null)
            {
                throw new ProviderOperationException("Отсутствует контакт для прохода по " +
                                                     $"билету \"{entity.Data.Ticket.Barcode}\" для мероприятия \"{entity.Data.Ticket.Event.EventName}\", id мероприятия - {entity.Data.Ticket.Event.EventId}, clientId - {entity.Customer.Id}", entity.Data.Ticket);
            }
            return (company.Id, true);
        }
    }
}
