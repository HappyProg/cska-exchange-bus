﻿using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;
using Evotor.API;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class EvotorDeviceHandler : IBaseEntityHandler<EvotorDevice>
    {
        private readonly IMapper _mapper;

        private readonly IEntityGetter<EvotorDevice, B24ListElement> _getter;

        private readonly IB24Repository<B24ListElement> _b24ListRepository;

        public EvotorDeviceHandler(IMapper mapper, B24ListElementRepository b24ListRepository)
        {
            _mapper = mapper;
            _b24ListRepository = b24ListRepository;
            _getter = new EntityGetter<EvotorDevice, B24ListElement, EvotorDeviceSettingsQuery>(b24ListRepository);
        }

        public async Task Handle(EvotorDevice entity)
        {
            var device = await _getter.GetEntity(entity, Const.UniversalLists.BITRIX_LIST_EVOTOR_DEVICE).ConfigureAwait(false);
            var newDevice = _mapper.Map<EvotorDevice, B24ListElement>(entity);
            if (device == null)
            { 
                await _b24ListRepository.CreateEntityAsync(newDevice).ConfigureAwait(false);
            }
            else
            {
                newDevice.Id = device.Id;
                await _b24ListRepository.UpdateEntityAsync(newDevice).ConfigureAwait(false);
            }
        }
    }
}
