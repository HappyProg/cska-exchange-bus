﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Helpers;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;
using Evotor.API;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class EvotorReceiptHandler : IBaseEntityHandler<EvotorReceipt>
    {
        private readonly IEvotorApiConnector _evotorApiConnector;

        private readonly IEntityGetter<EvotorDocument<EvotorDocumentSell>, Deal> _baseDealGetter;

        private readonly IEntityGetter<EvotorReceipt, Deal> _dealGetter;

        private readonly IEntityGetter<EvotorReceiptItem, B24Product> _productGetter;

        private readonly IEntityGetter<EvotorReceipt, B24ListElement> _terminalGetter;

        private readonly IEntityGetter<EvotorReceipt, B24ListElement> _storeGetter;

        private readonly IB24Repository<B24Product> _productRepository;

        private readonly IDealProductAddition _dealRepository;

        private readonly IMapper _mapper;

        public EvotorReceiptHandler(IMapper mapper, IEvotorApiConnector evotorApiConnector, DealRepository dealRepository, B24ProductRepository productRepository, B24ListElementRepository b24ListElementRepository)
        {
            _mapper = mapper;
            _evotorApiConnector = evotorApiConnector;
            _productRepository = productRepository;
            _dealRepository = dealRepository;
            _baseDealGetter = new EntityGetter<EvotorDocument<EvotorDocumentSell>, Deal, EvotorDocumentDealQuerySettingsQuery>(dealRepository);
            _productGetter = new EntityGetter<EvotorReceiptItem, B24Product, EvotorReceiptB24ProductSettingsQuery>(productRepository);
            _terminalGetter = new EntityGetter<EvotorReceipt,B24ListElement,EvotorReceiptDeviceSettingsQuery>(b24ListElementRepository);
            _storeGetter = new EntityGetter<EvotorReceipt,B24ListElement,EvotorReceiptStoreSettingsQuery>(b24ListElementRepository);
            _dealGetter = new EntityGetter<EvotorReceipt, Deal, EvotorReceiptDealSettingsQuery>(dealRepository);
        }

        public async Task Handle(EvotorReceipt entity)
        {
            var deal = await HandleDeal(entity).ConfigureAwait(false);
            await HandleProducts(entity, deal.Id).ConfigureAwait(false);
        }

        private async Task<Deal> GetBaseDocument(EvotorReceipt entity)
        {
            var baseReceipt = await _evotorApiConnector
                .GetDocumentAsync<EvotorDocumentSell>(entity.Data.StoreId, entity.Data.Id)
                .ConfigureAwait(false);
            if (baseReceipt == null)
            {
                throw new ProviderOperationException($"Не удалось найти базовый документ для чека возврата id - {entity.Data.Id}, в облаке Эвотор!");
            }
            var deal = await _baseDealGetter.GetEntity(baseReceipt).ConfigureAwait(false);
            if (deal == null)
            {
                throw new ProviderOperationException($"Не удалось найти документ для чека возврата id - {entity.Data.Id}, базовый документ - {baseReceipt.Body.BaseDocumentId}, в Битрикс24!");
            }
            return deal;
        }

        private async Task HandleProducts(EvotorReceipt receipt, long dealId)
        {
            var prodCollection = new List<DealProduct>();
            foreach (var product in receipt.Data.Items)
            {
                var b24Product = await _productGetter.GetEntity(product).ConfigureAwait(false);
                if (b24Product == null)
                {
                    b24Product = new B24Product
                    {
                        Name = product.Name,
                        CurrencyId = Bitrix24Currency.RUB,
                        Price = product.Price,                      
                        ExternalId = EvotorHelper.ParseId(product.Id)
                    };
                    b24Product.Id = await _productRepository.CreateEntityAsync(b24Product).ConfigureAwait(false);
                }
                prodCollection.Add(new DealProduct
                {
                    // Пишем цену с учетом всех скидок
                    //
                    Price = product. SumPrice,
                    Quantity = product.Quantity,
                    ProductId = b24Product.Id,
                    // По странной логике Б24 пишем в поле не суммированую скидку, а скидку на штуку
                    //
                    DiscountAmount = product.Discount / product.Quantity
                });
            }
            await AddProducts(dealId, prodCollection).ConfigureAwait(false);
        }

        private async Task AddProducts(long dealId, List<DealProduct> prodCollection)
        {
            if (prodCollection.Count > 0)
            {
                var result = await _dealRepository.AddProducts(dealId, prodCollection)
                    .ConfigureAwait(false);
                if (result == false)
                {
                    throw new ProviderOperationException($"Ошибка добавления товаров в сделку Id - {dealId}!");
                }
            }
        }

        private async Task<Deal> HandleDeal(EvotorReceipt receipt)
        {
            var store = await _storeGetter.GetEntity(receipt, Const.UniversalLists.BITRIX_LIST_EVOTOR_STORE).ConfigureAwait(false);
            if (store == null)
            {
                throw new NotFoundException($"Магазин {receipt.Data.StoreId} не найден в Битрикс 24!");
            }
            var terminal = await _terminalGetter.GetEntity(receipt, Const.UniversalLists.BITRIX_LIST_EVOTOR_DEVICE).ConfigureAwait(false);
            if (terminal == null)
            {
                throw new NotFoundException($"Терминал {receipt.Data.DeviceId} не найден в Битрикс 24!");
            }

            var deal = await GetDeal(receipt, store, terminal).ConfigureAwait(false);
            if (deal.Id <= 0)
            {
                deal.Id = await _dealRepository.CreateEntityAsync(deal).ConfigureAwait(false);
            }
            else
            {
                var result = await _dealRepository.UpdateEntityAsync(deal).ConfigureAwait(false);
                if (result == false)
                {
                    throw new ProviderOperationException("Ошибка обновления чека Эвотор!");
                }
            }

            return deal;
        }

        private async Task<Deal> GetDeal(EvotorReceipt receipt, B24ListElement store, B24ListElement terminal)
        {
            var deal = await _dealGetter.GetEntity(receipt).ConfigureAwait(false) ?? new Deal();
            deal.Title = GetTitleForDeal(receipt, store);


            if (receipt.Data.Type == ReceiptType.PAYBACK)
            {
                var baseDeal = await GetBaseDocument(receipt).ConfigureAwait(false);
                deal[Const.Fields.EvotorReceipts.RECEIPT_BASE_UUID] = baseDeal.Id;
            }
            deal.SetValue(Const.Fields.EvotorReceipts.DEVICE, terminal.Id);
            deal.SetValue(Const.Fields.EvotorReceipts.STORE, store.Id);
            deal = _mapper.Map(receipt, deal);
            return deal;
        }

        private string GetTitleForDeal(EvotorReceipt receipt, B24ListElement store)
        {
            return receipt.Data.Type == ReceiptType.SELL
                ? $"Продажа \"{store.Name}\" от {receipt.Data.DateTime.ToString("dd.MM.yyyy HH:mm")}"
                : $"Возврат \"{store.Name}\" от {receipt.Data.DateTime}";
        }
    }
}
