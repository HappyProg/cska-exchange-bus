﻿using System.Threading.Tasks;
using AutoMapper;
using Bitrix24.Connector;
using CSKA.Queue.Providers.B24.Getters;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;
using Evotor.API;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public class EvotorStoreHandler : IBaseEntityHandler<EvotorStore>
    {
        private readonly IB24Repository<B24ListElement> _b24ListRepository;

        private readonly IEntityGetter<EvotorStore, B24ListElement> _getter;

        private readonly IMapper _mapper;

        public EvotorStoreHandler(IMapper mapper, B24ListElementRepository b24ListRepository)
        {
            _mapper = mapper;
            _b24ListRepository = b24ListRepository;
            _getter = new EntityGetter<EvotorStore, B24ListElement, EvotorStoreSettingsQuery>(b24ListRepository);
        }

        public async Task Handle(EvotorStore entity)
        {
            var store = await _getter.GetEntity(entity, Const.UniversalLists.BITRIX_LIST_EVOTOR_STORE).ConfigureAwait(false);
            var newStore = _mapper.Map<EvotorStore, B24ListElement>(entity);
            if (store == null)
            { 
                await _b24ListRepository.CreateEntityAsync(newStore).ConfigureAwait(false);
            }
            else
            {
                newStore.Id = store.Id;
                await _b24ListRepository.UpdateEntityAsync(newStore).ConfigureAwait(false);
            }
        }
    }
}
