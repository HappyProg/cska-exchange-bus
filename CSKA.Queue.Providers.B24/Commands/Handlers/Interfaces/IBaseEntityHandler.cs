﻿using System.Threading.Tasks;
using CSKA.Queue.Domain.Core.Entities;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public interface IBaseEntityHandler<in T> where T : BaseProviderObject
    {
        Task Handle(T entity);
    }
}
