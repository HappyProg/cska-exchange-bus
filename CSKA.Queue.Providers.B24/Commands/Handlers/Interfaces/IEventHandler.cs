﻿using System.Threading.Tasks;
using CSKA.Queue.Domain.Core;

namespace CSKA.Queue.Providers.B24.Handlers
{
    public interface IEventHandler<in T> : IBaseEntityHandler<Event>
    {
        Task<long> GetEventId(T entity);
    }
}
