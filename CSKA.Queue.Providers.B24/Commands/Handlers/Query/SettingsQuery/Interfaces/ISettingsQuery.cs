﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core.Entities;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    public interface ISettingsQuery<in T>  where T : BaseProviderObject
    {
        IEnumerable<string> Selects { get; }

        IEnumerable<B24CustomFilter> GetCustomFilters(T entity);
    }
}
