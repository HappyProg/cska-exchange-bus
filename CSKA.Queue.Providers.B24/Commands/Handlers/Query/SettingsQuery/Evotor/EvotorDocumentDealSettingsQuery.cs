﻿using System.Collections.Generic;
using CSKA.Queue.Providers.B24.Models;
using Evotor.API;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class EvotorDocumentDealQuerySettingsQuery : ISettingsQuery<EvotorDocument<EvotorDocumentSell>>
    {
        public IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(EvotorDocument<EvotorDocumentSell> entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.EvotorReceipts.RECEIPT_UUID,
                    FieldValue = entity.Body.BaseDocumentId
                }
            };
        }
    }
}
