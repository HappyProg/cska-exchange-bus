﻿using System.Collections.Generic;
using CSKA.Queue.Providers.B24.Models;
using Evotor.API;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class EvotorReceiptStoreSettingsQuery : ISettingsQuery<EvotorReceipt>
    {
        public IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(EvotorReceipt entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.EvotorStores.UUID,
                    FieldValue = entity.Data.StoreId
                }
            };
        }
    }
}
