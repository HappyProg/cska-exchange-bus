﻿using System.Collections.Generic;
using CSKA.Queue.Providers.B24.Helpers;
using CSKA.Queue.Providers.B24.Models;
using Evotor.API;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class EvotorReceiptB24ProductSettingsQuery : ISettingsQuery<EvotorReceiptItem>
    {
        public IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(EvotorReceiptItem entity)
        {
            var productId = EvotorHelper.ParseId(entity.Id);

            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.Products.EXT_ID,
                    FieldValue = productId
                }
            };
        }
    }
}