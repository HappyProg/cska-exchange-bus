﻿using System.Collections.Generic;
using CSKA.Queue.Providers.B24.Models;
using Timepad.API.Webhooks;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    public class TimepadTicketB24ListSettingsQuery :  ISettingsQuery<TimepadOrderWebhookTicket>
    {
        public IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(TimepadOrderWebhookTicket entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ListElement.PROP_NAME_TICKET_ID,
                    FieldValue = entity.Id
                }
            };
        }
    }
}
