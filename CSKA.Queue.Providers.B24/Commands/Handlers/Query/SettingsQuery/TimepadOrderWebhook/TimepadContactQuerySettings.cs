﻿using System;
using System.Collections.Generic;
using System.Text;
using Bitrix24.Connector;
using CSKA.Queue.Providers.B24.Models;
using Timepad.API.Webhooks;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    public class TimepadContactQuerySettings : ISettingsQuery<TimepadOrderWebhook>
    {
        public IEnumerable<string> Selects => 
            new List<string>
            {
                Const.Fields.B24ContactConst.PROP_NAME,
                Const.Fields.B24ContactConst.PROP_SECOND_NAME,
                Const.Fields.B24ContactConst.PROP_LAST_NAME
            };

        public IEnumerable<B24CustomFilter> GetCustomFilters(TimepadOrderWebhook entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ContactConst.EMAIL_NAME,
                    FieldValue = entity.Mail,
                    Comparer = B24Comparer.None
                }
            };
        }
    }
}
