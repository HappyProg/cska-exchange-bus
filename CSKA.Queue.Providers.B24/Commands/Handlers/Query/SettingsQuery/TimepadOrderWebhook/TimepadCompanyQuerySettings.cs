﻿using System.Collections.Generic;
using Bitrix24.Connector;
using CSKA.Queue.Providers.B24.Models;
using Timepad.API.Webhooks;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    public class TimepadCompanyQuerySettings : ISettingsQuery<TimepadOrderWebhook>
    {
        public IEnumerable<string> Selects => 
            new List<string>
            {
                Const.Fields.B24CompanyConst.TITLE_NAME
            };

        public IEnumerable<B24CustomFilter> GetCustomFilters(TimepadOrderWebhook entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ContactConst.EMAIL_NAME,
                    FieldValue = entity.Mail,
                    Comparer = B24Comparer.None
                }
            };
        }
    }
}
