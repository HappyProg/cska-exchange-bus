﻿using System;
using System.Collections.Generic;
using CSKA.Queue.Providers.B24.Models;
using Timepad.API.Webhooks;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    public class TimepadDealQuerySettings : ISettingsQuery<TimepadOrderWebhook>
    {
        public IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(TimepadOrderWebhook entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.DealConst.PROP_TICKET_ID,
                    FieldValue = entity.Id
                }
            };
        }
    }
}
