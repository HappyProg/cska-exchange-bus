﻿using System;
using System.Collections.Generic;
using System.Text;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class TicketDealSettingsQuery : ISettingsQuery<Transaction<Ticket>>
    {
        public IEnumerable<string> Selects => new List<string>
        {
            Const.Fields.DealConst.PROP_CODE
        };

        public IEnumerable<B24CustomFilter> GetCustomFilters(Transaction<Ticket> entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.DealConst.PROP_TICKET_BARCODE,
                    FieldValue = entity.Data.Barcode
                }
            };
        }
    }
}