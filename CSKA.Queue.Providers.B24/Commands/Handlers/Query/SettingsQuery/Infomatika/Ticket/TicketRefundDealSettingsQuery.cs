﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    class TicketRefundDealSettingsQuery : ISettingsQuery<Transaction<Ticket>>
    {
        public IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(Transaction<Ticket> entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.DealConst.PROP_TICKET_ID,
                    FieldValue = entity.Data.Id
                }
            };
        }
    }
}
