﻿using System.Collections.Generic;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class TicketCompanySettingsQuery : CustomerCompanySettingsQuery
    {
        public override IEnumerable<string> Selects =>
            new List<string>
            {
                Const.Fields.B24CompanyConst.TITLE_NAME
            };
    }
}