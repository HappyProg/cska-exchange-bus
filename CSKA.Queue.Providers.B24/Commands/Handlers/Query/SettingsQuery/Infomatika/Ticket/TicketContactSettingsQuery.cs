﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class TicketContactSettingsQuery : CustomerContactSettingsQuery
    {
        public override IEnumerable<B24CustomFilter> GetCustomFilters(Customer entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ContactConst.IDs,
                    FieldValue = entity.Id
                }
            };
        }

        public override IEnumerable<string> Selects =>
            new List<string>
            {
                Const.Fields.B24ContactConst.PROP_NAME,
                Const.Fields.B24ContactConst.PROP_SECOND_NAME,
                Const.Fields.B24ContactConst.PROP_LAST_NAME,
            };
    }
}
