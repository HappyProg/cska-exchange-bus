﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class ReservationB24ListSettingsQuery : ISettingsQuery<Transaction<Reservation>>
    {
        public virtual IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(Transaction<Reservation> entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ListElement.PROP_NAME_RESERVATION_EVENT_ID,
                    FieldValue = entity.Data.Event.EventId
                },
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_SECTOR,
                    FieldValue = entity.Data.Seat.SectorName
                },
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_ROW,
                    FieldValue = entity.Data.Seat.Row
                },
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_NUM,
                    FieldValue = entity.Data.Seat.Num
                }
            };
        }
    }
}
