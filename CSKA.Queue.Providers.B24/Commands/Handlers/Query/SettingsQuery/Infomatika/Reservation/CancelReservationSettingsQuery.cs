﻿using System.Collections.Generic;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class CancelReservationSettingsQuery : ReservationB24ListSettingsQuery
    {
        public override IEnumerable<string> Selects => 
            new List<string>
            {
                Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_DEAL_ID
            };
    }
}
