﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    public class LinkedSeatsSettingsQuery : ISettingsQuery<Transaction<Reservation>>
    {
        public IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(Transaction<Reservation> entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ListElement.PROP_NAME_RESERVATION_SEAT_DEAL_ID,
                    FieldValue = entity.Id
                }
            };
        }
    }
}
