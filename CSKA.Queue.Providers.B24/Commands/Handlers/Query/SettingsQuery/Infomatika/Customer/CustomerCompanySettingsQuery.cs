﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;

namespace CSKA.Queue.Providers.B24.Query
{
    internal class CustomerCompanySettingsQuery : ISettingsQuery<Customer>
    {
        public virtual IEnumerable<string> Selects =>
            new List<string>
            {
                Const.Fields.B24CompanyConst.EMAIL_NAME,
                Const.Fields.B24CompanyConst.TITLE_NAME,
                Const.Fields.B24CompanyConst.IDs,
            };

        public virtual IEnumerable<B24CustomFilter> GetCustomFilters(Customer entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24CompanyConst.IDs,
                    FieldValue = entity.Id
                }
            };
        }
    }
}
