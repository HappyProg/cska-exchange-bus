﻿using System.Collections.Generic;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class CustomerContactSettingsQuery : ISettingsQuery<Customer>
    {
        public virtual IEnumerable<string> Selects =>
            new List<string>
            {
                Const.Fields.B24ContactConst.IDs
            };

        public virtual IEnumerable<B24CustomFilter> GetCustomFilters(Customer entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ContactConst.EMAIL_NAME,
                    FieldValue = entity.Email,
                    Comparer = B24Comparer.None
                }
            };
        }
    }
}
