﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class PassageB24ListSettingsQuery : ISettingsQuery<Transaction<Passage>>
    {
        public IEnumerable<string> Selects { get; } = null;

        public IEnumerable<B24CustomFilter> GetCustomFilters(Transaction<Passage> entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ListElement.PROP_NAME_TICKET_EVENT_ID,
                    FieldValue = entity.Data.Ticket.Event.EventId
                },
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ListElement.PROP_NAME_TICKET_BARCODE,
                    FieldValue = entity.Data.Ticket.Barcode
                }
            };
        }
    }
}
