﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class PassageCompanySettingsQuery : ISettingsQuery<Customer>
    {
        public IEnumerable<string> Selects =>
            new List<string>
            {
                Const.Fields.B24CompanyConst.TITLE_NAME
            };

        public IEnumerable<B24CustomFilter> GetCustomFilters(Customer entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.B24ContactConst.IDs,
                    FieldValue = entity.Id
                }
            };
        }
    }
}