﻿using System.Collections.Generic;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Providers.B24.Models;

namespace CSKA.Queue.Providers.B24.Query.SettingsQuery
{
    internal class PassageDealSettingsQuery : ISettingsQuery<Transaction<Passage>>
    {
        public IEnumerable<string> Selects => new List<string>
        {
            Const.Fields.DealConst.PROP_CODE,
            Const.Fields.DealConst.PROP_CONTACT_ID
        };

        public IEnumerable<B24CustomFilter> GetCustomFilters(Transaction<Passage> entity)
        {
            return new List<B24CustomFilter>
            {
                new B24CustomFilter
                {
                    FieldName = Const.Fields.DealConst.PROP_TICKET_BARCODE,
                    FieldValue = entity.Data.Ticket.Barcode
                }
            };
        }
    }
}
