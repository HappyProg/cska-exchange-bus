﻿using Bitrix24.Connector;

namespace CSKA.Queue.Providers.B24.Models
{
    public class B24CustomFilter
    {
        public string FieldName { get; set; }

        public object FieldValue { get; set; }

        public B24Comparer Comparer { get; set; } = B24Comparer.Equal;
    }
}
