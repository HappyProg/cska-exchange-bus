﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Infrastructure;
using System;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24SmsProvider : ICommandProvider<SmsMessage>
    {
        private readonly ISmsProviderFactory _providerFactory;
        private readonly HttpClient _httpClient;

        internal class Bitrix24SmsConfirmation
        {
            public string CODE { get; set; }
            public string MESSAGE_ID { get; set; }
            public string STATUS { get; set; }
        }

        public Bitrix24SmsProvider(ISmsProviderFactory providerFactory, HttpClient httpClient)
        {
            _providerFactory = providerFactory ?? throw new ArgumentNullException(nameof(providerFactory));
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task SendAsync(SmsMessage data)
        {
            try
            {
                var provider = _providerFactory
                    .CreateProvider(data.Provider);
                var status = await provider
                    .GetMessageStatusAsync(data.ProviderId)
                    .ConfigureAwait(false);

                var sms = new Bitrix24SmsConfirmation
                {
                    CODE = data.Sender,
                    MESSAGE_ID = data.Id,
                    STATUS = status.ToString().ToLower()
                };

                // Если статус "Отправлено", то не передаем информацию в Б24,
                // т.к. она там и так уже есть
                //
                if (status != MessageStatus.Send)
                {
                    var body = Serializer.JSON.Serialize(sms);
                    var content = new StringContent(content: body, encoding: Encoding.UTF8, mediaType: "Application/json");

                    var response = await _httpClient
                        .PostAsync("", content)
                        .ConfigureAwait(false);

                    response.EnsureSuccessStatusCode();
                }

                // Если не конечный статус то инциируем новую отправку
                //
                if (status != MessageStatus.Delivered && status != MessageStatus.Fail)
                {
                    throw new ProviderOperationException($"Статус СМС сообщения не является окончательным - {status}");
                }
            }
            catch (SocketException ex)
            {
                throw new ProviderNotConnectedException(ex.Message);
            }
        }
    }
}
