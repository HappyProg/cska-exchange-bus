﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core.Entities;

namespace CSKA.Queue.Providers.B24.Getters
{
    public interface IEntityGetter<in TSource, TDestination> 
        where TSource : BaseProviderObject
        where TDestination : EntityBase
    {
        Task<TDestination> GetEntity(TSource entity, long id = default);

        Task<IEnumerable<TDestination>> GetEntities(TSource entity, long id = default);
    }
}
