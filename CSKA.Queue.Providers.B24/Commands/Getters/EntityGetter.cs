﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bitrix24.Connector;
using CSKA.Queue.Domain.Core.Entities;
using CSKA.Queue.Providers.B24.Extensions;
using CSKA.Queue.Providers.B24.Query.SettingsQuery;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24.Getters
{
    internal class EntityGetter<TSource, TDestination, TQuerySettings> : IEntityGetter<TSource, TDestination> 
        where TSource : BaseProviderObject
        where TDestination : EntityBase
        where TQuerySettings : ISettingsQuery<TSource>, new()
    {

        protected readonly IB24Repository<TDestination> Repository;

        protected readonly TQuerySettings SettingsQuery;

        public EntityGetter(IB24Repository<TDestination> repository)
        {
            Repository = repository;
            SettingsQuery = new TQuerySettings();
        }

        public virtual Task<TDestination> GetEntity(TSource entity, long id = default)
        {
            var query = B24QueryBuilder
                .Create();
            var filters = SettingsQuery.GetCustomFilters(entity);
            query.AddFilters(filters);
            var selects = SettingsQuery.Selects;
            query.AddSelects(selects);
            var buildQuery = query.Build();
            return Repository.GetEntityAsync(buildQuery, id);
        }

        public virtual async Task<IEnumerable<TDestination>> GetEntities(TSource entity, long id = default)
        {
            var query = B24QueryBuilder
                .Create();
            var filters = SettingsQuery.GetCustomFilters(entity);
            query.AddFilters(filters);
            var selects = SettingsQuery.Selects;
            query.AddSelects(selects);
            var buildQuery = query.Build();
            var res = await Repository.GetListEntityAsync(id, buildQuery).ConfigureAwait(false);
                return res.Result;
        }
    }
}
