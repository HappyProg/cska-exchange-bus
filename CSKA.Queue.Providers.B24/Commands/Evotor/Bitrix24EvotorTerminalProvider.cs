﻿using CSKA.Queue.Domain.Interfaces;
using Evotor.API;
using System.Threading.Tasks;
 using CSKA.Queue.Providers.B24.Handlers;

 namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24EvotorTerminalProvider : ICommandProvider<EvotorDevice>
    {
        private readonly IBaseEntityHandler<EvotorDevice> _handler;

        public Bitrix24EvotorTerminalProvider(EvotorDeviceHandler handler)
        {
            _handler = handler;
        }

        public async Task SendAsync(EvotorDevice data)
        {
            // Сохраянем
            //
            await _handler.Handle(data).ConfigureAwait(false);
        }
    }
}
