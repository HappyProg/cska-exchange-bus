﻿using Bitrix24.Connector;
using Bitrix24.Connector.Core;
using CSKA.Queue.Domain.Interfaces;
using Evotor.API;
using System;
using System.Threading.Tasks;
using CSKA.Queue.Providers.B24.Handlers;
using CSKA.Queue.Providers.B24.Repositories;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24EvotorStoreProvider : ICommandProvider<EvotorStore>
    {
        private readonly IB24Repository<B24ListElement> _b24ListRepository;

        private readonly IBaseEntityHandler<EvotorStore> _handler;

        public Bitrix24EvotorStoreProvider(B24ListElementRepository b24ListRepository, EvotorStoreHandler handler)
        {
            _b24ListRepository = b24ListRepository;
            _handler = handler;
        }

        public async Task SendAsync(EvotorStore data)
        {
            // Сначала удаляем все, т.е. из эво приходит всегда массив всех магазинов
            // даже на опрацию удаления магазина
            //
            // upd: пока удалять не будем, возможно позже сделаем просто признак удаления
            //
            //await RemoveAll().ConfigureAwait(false);

            // Сохраянем
            //
            await _handler.Handle(data).ConfigureAwait(false);
            //foreach (var store in data)
            //{
            //    await _handler.Handle(store).ConfigureAwait(false);
            //}
        }

        private async Task RemoveAll()
        {
            // TODO:
            // Т.к. магазинов немного то сейчас делаем 1 запрос,
            // но лучше все переписать на запрос по страницам (50 на странице)
            //
            var elements = await _b24ListRepository.GetListEntityAsync(Const.UniversalLists.BITRIX_LIST_EVOTOR_STORE).ConfigureAwait(false);

            if (elements.Result != null)
            {
                foreach (var el in elements.Result)
                {
                    await _b24ListRepository.DeleteAsync(Const.UniversalLists.BITRIX_LIST_EVOTOR_STORE, el.ElementCode).ConfigureAwait(false);
                }
            }
        }


    }
}
