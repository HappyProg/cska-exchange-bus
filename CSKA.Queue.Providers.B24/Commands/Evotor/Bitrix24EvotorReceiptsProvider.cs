﻿using CSKA.Queue.Domain.Interfaces;
using Evotor.API;
using System.Threading.Tasks;
using CSKA.Queue.Providers.B24.Handlers;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24EvotorReceiptsProvider :  ICommandProvider<EvotorReceipt>
    {
        private readonly IBaseEntityHandler<EvotorReceipt> _handler;
        public Bitrix24EvotorReceiptsProvider(EvotorReceiptHandler handler)
        {
            _handler = handler;
        }

        public Task SendAsync(EvotorReceipt receipt)
        {
            return _handler.Handle(receipt);
        }
    }
}
