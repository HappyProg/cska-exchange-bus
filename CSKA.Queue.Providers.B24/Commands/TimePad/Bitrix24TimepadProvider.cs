﻿using Bitrix24.Connector.Core;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using System.Threading.Tasks;
using CSKA.Queue.Providers.B24.Handlers;
using Timepad.API.Webhooks;

namespace CSKA.Queue.Providers.B24
{
    public class Bitrix24TimepadProvider : ICommandProvider<TimepadOrderWebhook>
    {
        private readonly IBaseEntityHandler<TimepadOrderWebhook> _handler;

        public Bitrix24TimepadProvider(TimepadHandler handler)
        {
            _handler = handler;
        }

        public async Task SendAsync(TimepadOrderWebhook data)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(data.Mail))
                {
                    throw new ProviderOperationException("[Timepad]: пустой email!");
                }

                await _handler.Handle(data).ConfigureAwait(false);

            }
            catch (B24NotConnectedException ex)
            {
                throw new ProviderNotConnectedException(ex.Message);
            }
        }
    }
}
