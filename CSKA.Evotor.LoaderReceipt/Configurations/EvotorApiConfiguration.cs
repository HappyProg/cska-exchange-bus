﻿
namespace CSKA.Evotor.LoaderReceipt
{
    public class EvotorApiConfiguration
    {
        public string BaseAddress { get; set; }

        public string Token { get; set; }
    }
}
