﻿
namespace CSKA.Evotor.LoaderReceipt
{
    public class LoadPeriodConfiguration
    {
        public  string BeginDate { get; set; }

        public string EndDate { get; set; }
    }
}
