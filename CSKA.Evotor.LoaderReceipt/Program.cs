﻿using System;
using CSKA.Evotor.LoaderReceipt.Services;
using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Infrastructure;
using CSKA.Queue.Infrastructure.BL.Configuration;
using Evotor.API;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CSKA.Evotor.LoaderReceipt
{
    public static class Program
    {
        public static IConfiguration Configuration { get; private set; }

        public static void Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder().
                AddJsonFile("appsettings.json", true, true)
                .Build();
            Configuration = config;
            var host = CreateHostBuilder(args).Build();
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var evotorApiConfig = Configuration.GetSection("EvotorApi").Get<EvotorApiConfiguration>();
            return Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {

                    services
                        .AddRabbitMqEvotorEventBus(new RabbitMqConfiguration
                        {
                            Password = "guest",
                            UserName = "guest",
                            Uri = "rabbitmq://localhost/"
                        })
                        .AddHostedService<LoaderReceiptService>()
                        .AddTransient<ICreatorReceiptService, CreatorReceiptService>()
                        .AddTransient<PublisherReceiptService>()
                        .AddHttpClient<IEvotorApiConnector, EvotorApiConnector>(cfg =>
                        {
                            cfg.BaseAddress = new Uri(evotorApiConfig.BaseAddress);
                            cfg.DefaultRequestHeaders.Add("Authorization", evotorApiConfig.Token);
                        });


                });
        }

        internal static IServiceCollection AddRabbitMqEvotorEventBus(this IServiceCollection services, RabbitMqConfiguration rabbitMq)
        {
            var rabbitUri = new Uri(rabbitMq.Uri);

            services.AddMassTransit(x =>
            {
                x.AddRabbitMqMessageScheduler();
                x.SetKebabCaseEndpointNameFormatter();

                x.UsingRabbitMq((context, cfg) =>
                {

                    cfg.Host(rabbitUri, host =>
                    {
                        host.Username(rabbitMq.UserName);
                        host.Password(rabbitMq.Password);
                    });
                    cfg.AutoDelete = false;
                    cfg.PurgeOnStartup = false;


                    cfg.ConfigureEndpoints(context);
                });
            });


            services.AddHostedService<RabbitMQEventBus>();
            services.AddSingleton<IEventBus, EventBus>();
            return services;
        }
    }
}
