﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CSKA.Evotor.LoaderReceipt.Services;
using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Infrastructure;
using Evotor.API;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CSKA.Evotor.LoaderReceipt
{
    internal class LoaderReceiptService : BackgroundService
    {
        private readonly ILogger<LoaderReceiptService> _logger;
        private readonly IEventBus _eventBus;
        private readonly IEvotorApiConnector _apiConnector;

        private readonly DateTime _beginDate;

        private readonly DateTime _endDate;

        private readonly ICreatorReceiptService _creatorReceiptService;

        private readonly PublisherReceiptService _publisherReceiptService;

        public LoaderReceiptService(IEvotorApiConnector apiConnector, ILogger<LoaderReceiptService> logger, ICreatorReceiptService creatorReceiptService, PublisherReceiptService publisherReceiptService, IEventBus eventBus)
        {
            var loadPeriod = GetPeriodConfiguration();
            _beginDate = DateTime.Parse(loadPeriod.BeginDate);
            _endDate = DateTime.Parse(loadPeriod.EndDate);
            _apiConnector = apiConnector;
            _logger = logger;
            _creatorReceiptService = creatorReceiptService;
            _publisherReceiptService = publisherReceiptService;
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        private LoadPeriodConfiguration GetPeriodConfiguration()
        {
            var loadPeriodSection = Program.Configuration.GetSection("LoadPeriod");
            if (loadPeriodSection == null)
                throw new ArgumentNullException(nameof(loadPeriodSection));
            var loadPeriod = loadPeriodSection.Get<LoadPeriodConfiguration>();
            if (loadPeriod == null)
                throw new ArgumentNullException(nameof(loadPeriod));
            if (loadPeriod.BeginDate == null || loadPeriod.EndDate == null)
                throw new ArgumentNullException($"{nameof(loadPeriod.BeginDate)} == null or {nameof(loadPeriod.EndDate)} == null ");
            return loadPeriod;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Processing...");
            try
            {
                var stores = await _apiConnector.GetStoresV1Async().ConfigureAwait(false);
                var evotorStores = stores.ToList();
                // Продажи
                //
                var sells = await GetDocuments(evotorStores, DocumentType.SELL).ConfigureAwait(false);
               
                // Возвраты
                //
                var payback = await GetDocuments(evotorStores, DocumentType.PAYBACK).ConfigureAwait(false);
                var receipts = _creatorReceiptService.CreateReceipts(sells);
                var paybackReceipts = _creatorReceiptService.CreateReceipts(payback);
                receipts.AddRange(paybackReceipts);
                
                foreach (var r in receipts)
                {
                    await _eventBus.Publish(r).ConfigureAwait(false);
                }

                _logger.LogInformation($"Task complete, receipts count: {receipts.Count}");
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        }

        private async Task<EvotorDocumentList<EvotorDocumentSell>> GetDocuments(List<EvotorStore> stores, DocumentType documentType)
        {
            var sells = new EvotorDocumentList<EvotorDocumentSell>();
            foreach (var store in stores)
            {
                var storeReceipts = await _apiConnector.GetDocumentsAsync<EvotorDocumentSell>(documentType, store.Uuid, null, _beginDate, _endDate)
                    .ConfigureAwait(false);
                sells.Items.AddRange(storeReceipts.Items);
            }

            return sells;
        }
    }
}
