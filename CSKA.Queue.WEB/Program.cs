using App.Metrics.Formatters.Prometheus;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NLog.Web;

namespace CSKA.Queue.WEB
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseNLog()
                .UseMetricsWebTracking(opt =>
                {
                    opt.OAuth2TrackingEnabled = true;
                })
                .UseMetricsEndpoints(opt =>
                {
                    opt.EnvironmentInfoEndpointEnabled = false;
                    opt.MetricsTextEndpointOutputFormatter = new MetricsPrometheusTextOutputFormatter();
                    opt.MetricsEndpointOutputFormatter = new MetricsPrometheusProtobufOutputFormatter();
                });
    }
}
