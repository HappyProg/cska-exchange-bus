using System.Collections.Generic;
using System.Text.Json.Serialization;
using CSKA.PBI.Infrastructure.Configuration;
using CSKA.PBI.Infrastructure.Extensions;
using CSKA.Queue.AutoMapper;
using CSKA.Queue.Data;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Infrastructure.BL.Configuration;
using CSKA.Queue.Infrastructure.BL.Extensions;
using CSKA.Queue.Services.Auth.Options;
using CSKA.Queue.Services.Auth.Services;
using CSKA.Queue.Services.Auth.Services.JWT;
using CSKA.Queue.WEB.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CSKA.Queue.WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });

            services.Configure<JWTAuthOptions>(Configuration.GetSection("JWT"));
            services.Configure<PBIConfiguration>(Configuration.GetSection("PBI"));
            services
                .Configure<Bitrix24Configuration>(Configuration.GetSection("Bitrix24"))
                .AddScoped(sp => sp.GetRequiredService<IOptions<Bitrix24Configuration>>().Value);

            var jwtSecure = Configuration["JWT:SecretKey"];
            var pbiHost = Configuration["PBI:Host"];
            services.AddScoped<IAuthService, JWTAuthService>();

            // Services
            //
            services
                .AddRouting(options => options.LowercaseUrls = true)
                // Jwt 
                //
                .AddJwtAuthentication(jwtSecure)
                // Open API documentation
                //
                .AddOpenApi("CSKA Service REST API")
                // Automapper DI
                //
                .AddAutomapper()
                // SMS providers configure
                //
                .AddSmsProviders(Configuration)
                // Event bus, commands & queries
                //
                .AddEventBus(Configuration)
                // Mailer service
                .AddMailer()
                // Pbi service
                .AddPbiService(pbiHost, Configuration.GetConnectionString("StorageConnection"))
                // App database context
                //
                .AddAppDatabase(Configuration.GetConnectionString("ProdConnection"));
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                #region swagger
                app.UseOpenApi();
                app.UseSwaggerUi3();
                #endregion
            }

            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddConsole();
            });
            app.UseExceptionHandler(loggerFactory);

            app.UseCookiePolicy(new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Strict,
                HttpOnly = HttpOnlyPolicy.Always,
                Secure = CookieSecurePolicy.Always
            });
            app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseJWTToken();

            app.UseAuthorization();
            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseDefaultFiles(new DefaultFilesOptions
            {
                DefaultFileNames = new List<string> { "index.html" }
            });
        }
    }
}
