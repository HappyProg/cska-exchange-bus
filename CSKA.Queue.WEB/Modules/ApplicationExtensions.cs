﻿using CSKA.Queue.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CSKA.Queue.WEB
{
    public static class ApplicationExtensions
    {
        public static IServiceCollection AddSmsProviders(this IServiceCollection services, IConfiguration configuration)
        {
            ISmsProviderConfiguration smsProvider = configuration.GetSection("SmsProvider").Get<SmsProviderConfiguration>();
            services.AddScoped<ISmsProviderFactory, SmsProviderFactory>(sp => 
            {
                var factory = SmsProviderFactory.Create(builder => 
                {
                    builder.AddConfiguration(cfg =>
                    {
                        cfg.Url = smsProvider.Url;
                        cfg.Password = smsProvider.Password;
                        cfg.Username = smsProvider.Username;
                        cfg.Provider = smsProvider.Provider;
                    });
                });

                return (SmsProviderFactory)factory;
            });

            return services;
        }

        //public static IServiceCollection AddQueryProviders(this IServiceCollection services)
        //{
        //    //services.AddHostedService(sp =>
        //    //{
        //    //    var provider = new InfomatikaEventProvider(url: "http://mail.cskabasket.com:1213/soap",
        //    //        token: "025DADDB6FEFFF46C9B005B1DDC289210D", TransactionType.ClientInformation);
        //    //    return new TransactionQueryDispatcher<Event>(provider);
        //    //});


        //    //services.AddHostedService(sp =>
        //    //{
        //    //    var provider = new InfomatikaCustomerProvider(url: "http://mail.cskabasket.com:1213/soap",
        //    //        token: "025DADDB6FEFFF46C9B005B1DDC289210D", TransactionType.ClientInformation);
        //    //    return new TransactionQueryDispatcher<Customer>(provider);
        //    //});

        //    //services.AddHostedService(sp =>
        //    //{
        //    //    var provider = new InfomatikaTicketProvider(url: "http://mail.cskabasket.com:1213/soap",
        //    //        token: "025DADDB6FEFFF46C9B005B1DDC289210D", TransactionType.Ticket);
        //    //    return new TransactionQueryDispatcher<Transaction<Ticket>>(provider);
        //    //});

        //    //services.AddHostedService(sp =>
        //    //{
        //    //    var provider = new InfomatikaTicketProvider(url: "http://mail.cskabasket.com:1213/soap",
        //    //        token: "025DADDB6FEFFF46C9B005B1DDC289210D", TransactionType.Subscription);
        //    //    return new TransactionQueryDispatcher<Transaction<Ticket>>(provider);
        //    //});

        //    //services.AddHostedService(sp =>
        //    //{
        //    //    var provider = new InfomatikaPassageProvider(url: "http://mail.cskabasket.com:1213/soap",
        //    //        token: "025DADDB6FEFFF46C9B005B1DDC289210D", TransactionType.Ticket);
        //    //    return new TransactionQueryDispatcher<Transaction<Passage>>(provider);
        //    //});

        //    //services.AddHostedService(sp =>
        //    //{
        //    //    var provider = new InfomatikaReservationProvider(url: "http://mail.cskabasket.com:1213/soap",
        //    //        token: "025DADDB6FEFFF46C9B005B1DDC289210D", TransactionType.Ticket);
        //    //    return new TransactionQueryDispatcher<Transaction<Reservation>>(provider);
        //    //});

        //    return services;
        //}
    }
}
