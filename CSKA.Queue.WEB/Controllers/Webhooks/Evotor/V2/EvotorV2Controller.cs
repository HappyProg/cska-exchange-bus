﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using CSKA.Queue.Domain.Interfaces;
using Evotor.API;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.WEB
{
    [Route("api/evotor/v2")]
    [ApiController]
    [Authorize]
    public class EvotorV2Controller : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IEventBus _eventBus;

        public EvotorV2Controller(ILogger<EvotorV2Controller> logger, IEventBus eventBus)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        [HttpPost("receipts")]
        [ApiConventionMethod(typeof(DefaultApiConventions),
            nameof(DefaultApiConventions.Post))]
        public async Task<IActionResult> Receipts(EvotorReceipt data)
        {
            _logger.LogInformation(JsonSerializer.Serialize(data));
            await _eventBus.Publish(data);

            return Ok();
        }
    }
}
