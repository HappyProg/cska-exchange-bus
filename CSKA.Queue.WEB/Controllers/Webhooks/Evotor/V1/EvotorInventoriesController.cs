﻿using CSKA.Queue.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.Json;
using System;
using System.Threading.Tasks;
using Evotor.API;
using Microsoft.AspNetCore.Authorization;

namespace CSKA.Queue.WEB
{
    [Route("api/v1/inventories")]
    [ApiController]
    [Authorize]
    public class EvotorInventoriesController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IEventBus _eventBus;

        public EvotorInventoriesController(ILogger<EvotorInventoriesController> logger, IEventBus eventBus)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        [HttpPut("stores")]
        [ApiConventionMethod(typeof(DefaultApiConventions),
            nameof(DefaultApiConventions.Put))]
        public async Task<IActionResult> ChangeStore(EvotorStore[] data)
        {
            _logger.LogInformation(JsonSerializer.Serialize(data));
            await _eventBus.Publish(data);

            return Ok();
        }

        [HttpPut("devices")]
        [ApiConventionMethod(typeof(DefaultApiConventions),
            nameof(DefaultApiConventions.Put))]
        public async Task<IActionResult> ChangeDevice(EvotorDevice[] data)
        {
            _logger.LogInformation(JsonSerializer.Serialize(data));
            await _eventBus.Publish(data);

            return Ok();
        }
    }
}