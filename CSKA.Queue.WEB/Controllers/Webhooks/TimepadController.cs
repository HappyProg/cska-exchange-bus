﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Timepad.API.Webhooks;

namespace CSKA.Queue.WEB.Controllers
{
    [Route("api/v1/webhooks/[controller]")]
    [ApiController]
    public class TimepadController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IEventBus _eventBus;

        public TimepadController(ILogger<TimepadController> logger, IEventBus eventBus)
        {
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new System.ArgumentNullException(nameof(eventBus));
        }

        [HttpPost("orders")]
        [ApiConventionMethod(typeof(DefaultApiConventions),
            nameof(DefaultApiConventions.Post))]
        public async Task<IActionResult> OrderStatus()
        {         
            var req = HttpContext.Request;

            using (var reader = new StreamReader(req.Body))
            {
                var bodyStr = await reader.ReadToEndAsync();
                _logger.LogInformation("timepad order webhook: " + bodyStr);

                if (CheckSignature(bodyStr) == false)
                {
                    _logger.LogError("Не корректная подпись запроса!");
                    return Unauthorized();
                }

                try
                {
                    var order = Serializer.JSON.Deserialize<TimepadOrderWebhook>(bodyStr);
                    await _eventBus.Publish(order);
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                }               
            }
           
            return Ok();
        }

        private bool CheckSignature(string body)
        {
            var req = HttpContext.Request;
            var signature = req
                .Headers["X-Hub-Signature"]
                .ToString()
                .Replace("sha1=", "");

            var key = "DBC055D177534532A2A647E45CADF0410486E9E04B0892110C925746124C";
            var computedHash = GetHMacSha1(key, body);

            return computedHash.ToLower() == signature.ToLower();
        }

        private string GetHMacSha1(string key, string message)
        {
            var encoding = Encoding.Default;
            var keyBytes = encoding.GetBytes(key);
            var messageBytes = encoding.GetBytes(message);

            using (var hmacsha = new HMACSHA1(keyBytes))
            using (MemoryStream stream = new MemoryStream(messageBytes))
            {
                return hmacsha
                    .ComputeHash(stream)
                    .Aggregate("", (s, e) => s + string.Format("{0:x2}", e), s => s);
            }
        }
    }
}
