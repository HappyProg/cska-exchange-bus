﻿using System;
using System.Threading.Tasks;
using CSKA.Queue.Data.Interfaces;
using CSKA.Queue.Services.Auth.Options;
using CSKA.Queue.Services.Auth.Services;
using CSKA.Queue.WEB.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using CSKA.Queue.Data;

namespace CSKA.Queue.WEB.Controllers.Authentication
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        private readonly IOptions<JWTAuthOptions> _jwtOptions;

        private readonly IAccountRepository _accountRepository;

        public AuthController(IOptions<JWTAuthOptions> jwtOptions, IAuthService authenticationService, IAccountRepository accountRepository)
        {
            _jwtOptions = jwtOptions;
            _authService = authenticationService;
            _accountRepository = accountRepository;
        }

        [HttpPost("login")]
        public async Task<ActionResult<Models.Authentication>> Authorize([FromBody] Models.Authentication authentication)
        {
            if (authentication.Login == null || authentication.Password == null)
                throw new Exception("Incorrect request");

            var account = await _accountRepository
                .GetAccountAsync(authentication.Login);

            if (account == null) return Unauthorized();

            if (!PasswordManager.VerifyPassword(authentication.Password, account.Password))
            {
                return Unauthorized();
            }

            var token = _authService.GenerateToken(account, _jwtOptions.Value);

            if (!string.IsNullOrEmpty(token))
            {
                //WriteAuthCookies(token);
                return Ok(new
                {
                    access_token = token
                });
            }

            throw new Exception("Failed to generate token");
        }

        //private void WriteAuthCookies(string token)
        //{
        //    HttpContext.Response.Cookies.Append(JWTMiddleware.JWTCookieKey, token,
        //        new CookieOptions());
        //}
    }
}
