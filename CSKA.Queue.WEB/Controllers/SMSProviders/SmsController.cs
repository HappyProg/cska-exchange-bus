﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using App.Metrics;
using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using CSKA.Queue.Infrastructure;
using CSKA.Queue.WEB.Metrics;
using CSKA.Queue.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CSKA.Queue.WEB.Controllers
{
    [Route("api/v1/providers/[controller]")]
    [ApiController]
    [Authorize]
    public class SMSController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly ISmsProviderFactory _providerFactory;
        private readonly IEventBus _eventBus;
        private readonly IMetrics _metrics;

        public SMSController(ILogger<SMSController> logger, ISmsProviderFactory providerFactory, IEventBus eventBus, IMetrics metrics)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _providerFactory = providerFactory ?? throw new ArgumentNullException(nameof(providerFactory));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
            _metrics = metrics ?? throw new ArgumentNullException(nameof(metrics));
        }

        [HttpPost]
        [ApiConventionMethod(typeof(DefaultApiConventions),
            nameof(DefaultApiConventions.Create))]
        public async Task<ActionResult<Sms>> Send(Sms sms)
        {
            var smsStr = JsonSerializer.Serialize(sms);
            _logger.LogInformation("Отправка смс: " + smsStr);

            var provider = _providerFactory.CreateProvider(sms.Provider);
            var smsMessage = new SmsMessage
            {
                Sender = sms.Sender,
                Text = sms.Text,
                Receiver = sms.MessageTo,
                Provider = sms.Provider.ToString(),
                Status = MessageStatus.Send
            };

            smsMessage.ProviderId = await provider.SendAsync(smsMessage);
            smsMessage.Id = sms.Id;

            // Метрика, просто увеличиваем счетчик 
            //
            _metrics.Measure.Counter.Increment(CskaMetrics.SmsCounter);

            // Считаем что если есть id то ставим в очередь для отслеживания статуса
            //
            if (string.IsNullOrWhiteSpace(smsMessage.ProviderId) == false)
            {
                await _eventBus.PublishDelay(smsMessage, TimeSpan.FromSeconds(30));
            }

            return Ok(smsMessage.Id);
        }
    }
}