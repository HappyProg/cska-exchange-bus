﻿using CSKA.Queue.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace CSKA.Queue.WEB.Models
{
    /// <summary>
    /// СМС сообщение
    /// </summary>
    public class Sms
    {
        /// <summary>
        /// Идентификатор сообщения
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }
        /// <summary>
        /// Номер телефона получателя
        /// </summary>
        [Required]
        [JsonProperty("messageTo")]
        public string MessageTo { get; set; }
        /// <summary>
        /// Отправитель сообщения
        /// </summary>
        [Required]
        [JsonProperty("sender")]
        public string Sender { get; set; }
        /// <summary>
        /// Текст сообщения
        /// </summary>
        [Required]
        [JsonProperty("text")]
        public string Text { get; set; }
        /// <summary>
        /// Провайдер через который будет отправлено сообщение
        /// </summary>
        [Required]
        [JsonProperty("provider")]
        [JsonConverter(typeof(StringEnumConverter))]
        public SmsProvider Provider { get; set; }
    }
}
