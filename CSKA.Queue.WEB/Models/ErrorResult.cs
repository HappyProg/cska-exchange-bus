﻿namespace CSKA.Queue.WEB.Models
{
    public class ErrorResult
    {
        public string Message { get; set; }
        public int Code { get; set; }
    }
}
