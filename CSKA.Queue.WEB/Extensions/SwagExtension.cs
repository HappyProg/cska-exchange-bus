﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using NSwag.Generation.Processors.Security;
using NSwag;

namespace CSKA.Queue.WEB.Extensions
{
    public static class SwagExtension
    {
        public static IServiceCollection AddOpenApi(this IServiceCollection services, string title)
        {
            services.AddOpenApiDocument(document =>
            {
                document.Title = title;
                document.GenerateEnumMappingDescription = true;
                document.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT"));
                document.DocumentProcessors.Add(new SecurityDefinitionAppender("JWT", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",                   
                    Scheme = JwtBearerDefaults.AuthenticationScheme,
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\""
                }));
            });
            return services;
        }
    }
}
