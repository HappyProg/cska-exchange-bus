﻿using Microsoft.AspNetCore.Builder;

namespace CSKA.Queue.WEB.Extensions
{
    public static class JWTMiddleware
    {
        public const string JWTCookieKey = "CSKA.Queue.Application.Id";

        public static void UseJWTToken(this IApplicationBuilder app)
        {
            app.Use(async (context, next) =>
            {
                var token = context.Request.Cookies[JWTCookieKey];
                if (!string.IsNullOrEmpty(token))
                {
                    context.Request.Headers.Add("Authorization", "Bearer " + token);
                    context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                    context.Response.Headers.Add("X-Xss-Protection", "1");
                    context.Response.Headers.Add("X-Frame-Options", "DENY");
                }
                await next();
            });
        }
    }
}