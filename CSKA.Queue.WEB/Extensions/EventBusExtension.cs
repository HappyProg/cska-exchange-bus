﻿using CSKA.Queue.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using CSKA.Queue.Infrastructure.BL.Configuration;
using Microsoft.Extensions.Configuration;
using CSKA.Queue.Providers.Infomatika;
using CSKA.Queue.Domain.Core;

namespace CSKA.Queue.WEB.Extensions
{
    public static class EventBusExtension
    {
        internal static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
        {
            var infomatika = configuration.GetSection("Infomatika").Get<InfomatikaConfiguration>();
            var bitrix24 = configuration.GetSection("Bitrix24").Get<Bitrix24Configuration>();
            var rabbitMq = configuration.GetSection("RabbitMq").Get<RabbitMqConfiguration>();

            services
                // Configure MassTransit bus / RabbitMq
                //
                .AddRabbitMQEventBus(rabbitMq)
                // Провайдеры для эвотор api
                //
                .AddEvotorQueryProviders()
                .AddInfomatikaQueryProviders(infomatika)
                // All commands
                //
                .AddCommandProviders(bitrix24)
                ;

            return services;
        }
    }
}
