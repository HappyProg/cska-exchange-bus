﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.WEB.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Text.Json;

namespace CSKA.Queue.WEB.Extensions
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void UseExceptionHandler(this IApplicationBuilder app, ILoggerFactory logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";

                    var contextFeature = context
                    .Features
                    .Get<IExceptionHandlerFeature>();

                    if (contextFeature != null)
                    {
                        var exception = contextFeature.Error;
                        if (exception is NotFoundException) context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                        else if (exception is UnauthorizedAccessException) context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                        logger.CreateLogger("GlobalException")
                          .LogError(exception, exception.Message);

                        var errorJson = JsonSerializer.Serialize(new ErrorResult
                        {
                            Code = context.Response.StatusCode,
                            Message = exception.Message
                        }, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
                        await context.Response.WriteAsync(errorJson);
                    }
                });
            });
        }
    }
}
