﻿using System;

namespace CSKA.Queue.Providers.Infomatika
{
    public class EventsNotFoundException : Exception
    {
        public EventsNotFoundException(string message) : base(message) { }
    }
}
