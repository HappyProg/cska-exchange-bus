﻿using CSKA.Queue.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CSKA.Queue.Providers.Infomatika
{
    public class InfomatikaReservationProvider : InfomatikaProviderBase<Transaction<Reservation>>
    {
        private readonly Regex regexSector = new Regex(@"(\w*)$");

        private class ReservationState
        {
            public long DocId { get; set; }
            public byte[] RowVersion { get; set; }
            public long LastItemId { get; set; }
        }

        public InfomatikaReservationProvider(InfomatikaConfiguration cfg) : base(cfg)
        {
        }

        public override string Key => "INFOMATIKA_RESERVATION";

        protected override TransactionType TransactionType => throw new NotImplementedException();

        public override async Task<QueryResult<Transaction<Reservation>>> GetDataAsync(int pageNum, int pageSize, string state = null)
        {
            var result = new QueryResult<Transaction<Reservation>>
            {
                Data = new List<Transaction<Reservation>>()
            };

            var stateObject = DeserializeState<ReservationState>(state);
            var resDocs = await _infomatikaClient
                .GetReservationDocsAsync(_token, pageSize, stateObject.RowVersion)
                .ConfigureAwait(false);

            var eventsRV = await _infomatikaClient
                .GetEventsRVAsync(_token, 3000, null)
                .ConfigureAwait(false);

            var eventsIds = eventsRV?.payload
                    .Where(x => (DateBegin == null || x.sdate >= DateBegin)
                    && (DateEnd == null || x.sdate <= DateEnd)
                    && x.is_del == false)
                    .Select(s => s.sitting_ID)
                    .ToArray();


            foreach (var doc in resDocs.payload)
            {
                //if (doc.booking_ID == 28773 || doc.number_doc == 36737)
                //{

                //}

                if (result.Count == pageSize)
                {
                    break;
                }

                stateObject.RowVersion = doc.row_version;
                while (result.Count < pageSize)
                {
                    var resItems = await _infomatikaClient
                        .GetReservationDocsItemsAsync(_token,
                            doc.document_ID, pageSize - result.Count, (int)stateObject.LastItemId)
                        .ConfigureAwait(false);

                    //if (resItems.payload.Any(x => x.event_ID == 687))
                    //{

                    //}

                    var itemsResult = resItems
                        .payload
                        // Фильтруем по событиям которые получили по указанному периоду
                        //
                        ?.Where(x => eventsIds == null || eventsIds.Contains(x.event_ID))
                        .Select(i =>
                        new Transaction<Reservation>
                        {
                            Data = new Reservation
                            {
                                Comment = doc.description,
                                DocumentId = doc.document_ID,
                                IsDeleted = doc.is_del,
                                IsPost = doc.is_posting,
                                Number = doc.number_doc,
                                Status = (ReservationStatus)doc.document_type,
                                Seat = new Seat
                                {
                                    Num = i.seat_num,
                                    Row = i.seat_row,
                                    SectorName = ParseSectorName(WebUtility.HtmlDecode(i.sector_name)),
                                    TribuneName = i.tribune_name
                                },
                                Event = new Event
                                {
                                    EventId = i.event_ID,
                                },
                                BookingId = doc.booking_ID
                            },
                            Date = doc.date_doc,
                            TransactionType = TransactionType.Reservation,
                            Comment = doc.description
                        });

                    result.Data.AddRange(itemsResult);
                    if (resItems.payload.Length <= pageSize - result.Count)
                    {
                        stateObject.LastItemId = 0;
                        break;
                    }
                    else
                    {
                        stateObject.LastItemId = resItems.payload.Last().document_item_ID;
                    }
                }
            }

            var eventsWithoutNames = result.Data
               .Where(x => string.IsNullOrWhiteSpace(x.Data.Event.EventName))
               .GroupBy(g => g.Data.Event.EventId)
               .Select(s => s.Key)
               .ToArray();

            foreach (var eId in eventsWithoutNames)
            {
 

                var e = eventsRV.payload.FirstOrDefault(x => x.sitting_ID == eId);
                if (e == null)
                {
                    throw new ProviderOperationException($"Не удалось запросить информацию по событию, id - {eId}", e);
                }

                SetEvent(result.Data, new Event
                {
                    EventId = e.sitting_ID,
                    EventName = e.sitting_name,
                    Date = e.sdate,
                    ArenaId = e.unit_ID,
                    ArenaName = WebUtility.HtmlDecode(e.unit_name),
                    SeatsCount = e.total_count,
                    SeatsCountFree = e.free_count
                });
            }

            result.State = SerializeState(stateObject);

            //if (resDocs.payload.FirstOrDefault()?.date_doc.Year == DateTime.Now.Year)
            //{

            //}
            return result;
        }

        private string ParseSectorName(string originValue)
        {           
            var matches = regexSector.Matches(originValue);
            return matches.Count > 0 ? matches[0].Value : originValue;
        }

        private void SetEvent(IEnumerable<Transaction<Reservation>> transactions, Event e)
        {
            var curTransactions = transactions
                .Where(x => x.Data.Event.EventId == e.EventId && string.IsNullOrWhiteSpace(x.Data.Event.EventName));
            foreach(var t in curTransactions)
            {
                t.Data.Event = e;
            }
        }

        internal override Task<InfomatikaResult<Transaction<Reservation>>> GetData(TransactionType transactionType, int pageSize, byte[] rowVersion, Event e)
        {
            throw new NotImplementedException();
        }


    }
}
