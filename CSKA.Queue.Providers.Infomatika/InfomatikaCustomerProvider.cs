﻿using CSKA.Queue.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CSKA.Queue.Providers.Infomatika
{
    public class InfomatikaCustomerProvider : InfomatikaProviderBase<Customer>
    {
        public InfomatikaCustomerProvider(InfomatikaConfiguration cfg) : base(cfg)
        {
        }

        public override string Key => "INFOMATIKA_CUSTOMERS";

        protected override TransactionType TransactionType => throw new NotImplementedException();

        public override async Task<QueryResult<Customer>> GetDataAsync(int pageNum, int pageSize, string state = null)
        {
            var currentState = DeserializeState<InfomatikaProviderState>(state);
            var result = new QueryResult<Customer>();

            //while (result.Count < pageSize)
            //{
            //    var customers = await GetData(TransactionType.ClientInformation,
            //    pageSize - result.Count,
            //    currentState.RowVersion, null);

            //    currentState.RowVersion = customers.RowVersion;

            //    if (customers.Data.Count == 0)
            //    {
            //        break;
            //    }

            //    result.Data.AddRange(customers.Data);
            //}

            var customers = await GetData(TransactionType.ClientInformation,
                pageSize,
                currentState.RowVersion, null);

            currentState.RowVersion = customers.RowVersion;
            result.Data.AddRange(customers.Data);

            result.State = SerializeState(currentState);
            return result;
        }

        internal override async Task<InfomatikaResult<Customer>> GetData(TransactionType transactionType, int pageSize, byte[] rowVersion, Event e)
        {
            //var ct = await _infomatikaClient.GetClientsTypesAsync(_token);
            var transactionsData = await _infomatikaClient.GetClientsAsync(
                 authorization_key: _token,
                 size: pageSize,
                 row_version: rowVersion).ConfigureAwait(false);

            if (transactionsData.ErrorCode > 0)
            {
                throw new ProviderOperationException($"Ошибка загрузки данных, key - {Key}, error code - {transactionsData.ErrorCode}");
            }

            //var t = transactionsData.payload?.FirstOrDefault(x => x.email == "jump305@yandex.ru");
            //if (t != null)
            //{

            //}

            var transactions = transactionsData?.payload?
                .Select(t => new Customer
                {
                    Address = t.address,
                    Birth = t.birthday,
                    Email = t.email,
                    Name = WebUtility.HtmlDecode(t.name),
                    Gender = GetBuyerGender(t.sex),
                    MobilePhone = t.phone_mobile,
                    Phone = t.phone,
                    Id = t.client_ID.ToString()
                })
                .ToList();

            return new InfomatikaResult<Customer>
            {
                Data = transactions,
                RowVersion = transactionsData?
                .payload?
                .LastOrDefault()?
                .row_version ?? rowVersion
            };
        }
    }
}
