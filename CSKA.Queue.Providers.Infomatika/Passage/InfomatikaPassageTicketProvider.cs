﻿using CSKA.Queue.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSKA.Queue.Providers.Infomatika
{
    public class InfomatikaPassageTicketProvider : InfomatikaProviderBase<Transaction<Passage>>
    {
        public InfomatikaPassageTicketProvider(InfomatikaConfiguration cfg) : base(cfg)
        {
        }

        public override string Key => "INFOMATIKA_PASSAGE_TICKETS";

        protected override TransactionType TransactionType => TransactionType.Ticket;

        internal override async Task<InfomatikaResult<Transaction<Passage>>> GetData(TransactionType transactionType, int pageSize, byte[] rowVersion, Event e)
        {
            //if (e.EventId != 688)
            //{
            //    return new InfomatikaResult<Transaction<Passage>>
            //    {
            //        Data = new List<Transaction<Passage>>(),
            //        RowVersion = rowVersion
            //    };
            //}

            var now = DateTime.Now;
            var endOfDay = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
            // Нам не нужны проходы из будущего
            //
            if (e.Date >= endOfDay)
            {
                return new InfomatikaResult<Transaction<Passage>>()
                {
                    RowVersion = rowVersion,
                    Data = new List<Transaction<Passage>>()
                };
            }

            var transactionsData = await _infomatikaClient.GetPassageAsync(
                 authorization_key: _token,
                 type: (byte)transactionType,
                 size: pageSize,
                 row_version: rowVersion,
                 eventID: e.EventId,
                 bar_code: null,
                 sdate: null,
                 edate: null).ConfigureAwait(false);

            //if (transactionsData.payload.Any(x => x.ticket_barcode == "7206869177010"))
            //{
            //    var tt = transactionsData.payload.First(x => x.ticket_barcode == "7206869177010");
            //}

            var transactions = transactionsData?.payload?
              .Select(t => new Transaction<Passage>
              {
                  Customer = new Customer
                  {
                      Name = t.buyer_client_name,
                      Id = t.buyer_client_ID?.ToString()
                  },
                  Date = t.sale_date,
                  Data = new Passage
                  {
                      Ticket = new Ticket
                      {
                          Barcode = t.ticket_barcode,
                          Price = t.price,
                          TicketType = (TicketType)t.e_ticket_type,
                          Event = e,
                          TicketState = TicketState.Undefined
                      },
                      Date = t.traffic_first_date,
                      IsUsed = t.is_used
                  },
                  TransactionType = transactionType
              })
              .ToList();

            var datesEvents = transactions
                .Where(x => x.Data.Date != null)
                .GroupBy(g => g.Data.Date.Value.Date)
                .Select(s => s.Key)
                .ToArray();

            return new InfomatikaResult<Transaction<Passage>>
            {
                Data = transactions,
                RowVersion = transactionsData?
                .payload?
                .LastOrDefault()?
                .row_version ?? rowVersion
            };
        }
    }
}
