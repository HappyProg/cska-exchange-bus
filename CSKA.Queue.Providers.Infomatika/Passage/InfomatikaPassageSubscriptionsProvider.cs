﻿using CSKA.Queue.Domain.Core;

namespace CSKA.Queue.Providers.Infomatika
{
    public class InfomatikaPassageSubscriptionsProvider : InfomatikaPassageTicketProvider
    {
        public override string Key => "INFOMATIKA_PASSAGE_SUBSCRIPTIONS";
        protected override TransactionType TransactionType => TransactionType.Subscription;
        public InfomatikaPassageSubscriptionsProvider(InfomatikaConfiguration cfg) : base(cfg)
        {
        }
    }
}
