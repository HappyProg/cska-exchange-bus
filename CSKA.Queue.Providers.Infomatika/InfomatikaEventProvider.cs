﻿using CSKA.Queue.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CSKA.Queue.Providers.Infomatika
{
    public class InfomatikaEventProvider : InfomatikaProviderBase<Event>
    {
        public InfomatikaEventProvider(InfomatikaConfiguration cfg) : base(cfg)
        {
        }

        public override string Key => "INFOMATIKA_EVENTS";

        protected override TransactionType TransactionType => throw new NotImplementedException();

        internal override Task<InfomatikaResult<Event>> GetData(TransactionType transactionType, int pageSize, byte[] rowVersion, Event e)
        {
            throw new NotImplementedException();
        }

        public override async Task<QueryResult<Event>> GetDataAsync(int pageNum, int pageSize, string state = null)
        {
            var stateObj = DeserializeState<InfomatikaProviderState>(state);
            var events = await GetEvents(stateObj.EventRowVersion).ConfigureAwait(false);
            if (events.Length > 0)
            {
                stateObj.EventRowVersion = events.Last().row_version;
            }



            var result = new QueryResult<Event>
            {
                Data = events.Select(s => s.CreateDto()).ToList()
            };

            foreach (var e in result.Data)
            {
                if (e.EventType == EventType.Batch || e.EventType == EventType.Subscription)
                {
                    await LoadLinkedEvents(e).ConfigureAwait(false);
                }
            }

            result.State = SerializeState(stateObj);
            return result;
        }
    }
}
