﻿using CSKA.Queue.Domain.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Event = CSKA.Queue.Domain.Core.Event;
using Ticket = CSKA.Queue.Domain.Core.Ticket;

namespace CSKA.Queue.Providers.Infomatika
{
    public class InfomatikaTicketProvider : InfomatikaProviderBase<Transaction<Ticket>>
    {
        private Dictionary<int, string> _paymentKinds = new Dictionary<int, string>();

        public InfomatikaTicketProvider(InfomatikaConfiguration cfg) : base(cfg)
        {
        }

        //public InfomatikaTicketProvider(string url, string token, TransactionType type) : base(url, token, type)
        //{
        //}

        public override string Key => "INFOMATIKA_TICKETS";


        protected override TransactionType TransactionType => TransactionType.Ticket;

        private IEnumerable<PaymentTypeItem> ParsePaymentsKinds(string data)
        {

            var normalizeData = WebUtility.HtmlDecode(data.Replace("&amp;", "&"));
            var obj = DeserializeXML<InfomatokaPaymentkinds>(normalizeData);
            return obj?.Paymentkind
                .Select(s => new PaymentTypeItem
                {
                    Name = _paymentKinds.FirstOrDefault(x => x.Key == ConvertValues<int>(s.Id)).Value,
                    Amount = ConvertValues<decimal>(s.Summ),
                    Id = s.Id
                }).ToArray();
        }

        private T ConvertValues<T>(string input)
        {
            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(T));
                if (converter != null)
                {
                    // Cast ConvertFromString(string text) : object to (T)
                    return (T)converter.ConvertFromString(input);
                }
                return default(T);
            }
            catch (NotSupportedException)
            {
                return default(T);
            }
        }

        private async Task LoadPaymentTypes()
        {
            var payloadData = await _infomatikaClient
                .GetPaymentKindsAsync(_token, 1000, 0)
                .ConfigureAwait(false);

            _paymentKinds = payloadData.payload?
                .ToDictionary(el => el.payment_kind_ID, el => el.name);
        }

        internal override async Task<InfomatikaResult<Transaction<Ticket>>> GetData(TransactionType transactionType, int pageSize, byte[] rowVersion, Event e)
        { 
            // Загружаем типы оплат
            //
            await LoadPaymentTypes().ConfigureAwait(false);

            //if (e.EventId != 718)
            //{
            //    return new InfomatikaResult<Transaction<Ticket>>
            //    {
            //        Data = new List<Transaction<Ticket>>(),
            //        RowVersion = rowVersion
            //    };
            //}

            var transactionsData = await _infomatikaClient
                .GetTicketsRVAsync(_token, (byte)transactionType, e.EventId,
                pageSize, rowVersion)
                .ConfigureAwait(false);

            var transactions = transactionsData?.payload?
              .Select(t => new Transaction<Ticket>
              {
                  Customer = new Customer
                  {
                      Id = t.client_ID.ToString()
                  },
                  Date = t.sale_date,
                  Data = new Ticket
                  {
                      Event = e,
                      Barcode = t.barcode,
                      Price = t.price,
                      TicketState = (TicketState)t.ticket_state,
                      TicketType = (TicketType)t.e_ticket_type,
                      Seat = new Seat
                      {
                          Num = t.seat_num,
                          Row = t.seat_row,
                          SectorName = WebUtility.HtmlDecode(t.sector_name)
                      },
                      BookingId = t.booking_ID,
                      Id = t.ticket_ID,
                      Payments = ParsePaymentsKinds(t.payment_kinds)
                  },
                  Discount = new Discount
                  {
                      Amount = t.discount_value,
                      Name = WebUtility.HtmlDecode(t.discount_name)
                  },
                  TransactionType = transactionType
              })
              .ToList();

            //var allReturned = transactions.Where(x => x.Data.TicketState == TicketState.Returned).ToArray();

            //var bonusSum = transactions
            //    .Where(x => x.Data.Payments.Any(p => p.Name == "Бонусы"))              
            //    .Sum(t => t.Data.Payments.Where(p => p.Name == "Бонусы").Sum(p => p.Amount));

            //var bonusSumSold = transactions
            //    .Where(x => x.Data.Payments.Any(p => p.Name == "Бонусы") && x.Data.TicketState == TicketState.Sold)
            //    .Sum(t => t.Data.Payments.Where(p => p.Name == "Бонусы").Sum(p => p.Amount));
            //var bonusSumNotActive= transactions
            //    .Where(x => x.Data.Payments.Any(p => p.Name == "Бонусы") && x.Data.TicketState != TicketState.Sold)
            //    .Sum(t => t.Data.Payments.Where(p => p.Name == "Бонусы").Sum(p => p.Amount));

            //var finalAmount = bonusSumSold - bonusSumNotActive;

           

            //if (transactions.Any(x => x.Data.Payments.Sum(s => s.Amount) > 0))
            //{
            //    var t = transactions.Where(x => x.Data.Payments.Sum(s => s.Amount) > 0);
            //}

            //if (transactions.Any(x => x.Data.Payments.Any(p => p.Name == "Бонусы")) == false)
            //{
            //    return new InfomatikaResult<Transaction<Ticket>>
            //    {
            //        Data = new List<Transaction<Ticket>>(),
            //        RowVersion = rowVersion
            //    };
            //}

            return new InfomatikaResult<Transaction<Ticket>>
            {
                Data = transactions/*.Where(x => x.Data.Payments.Any(p => p.Name == "Бонусы") && x.Data.Payments.First(p => p.Name == "Бонусы").Amount > 0).ToArray()*/,
                RowVersion = transactionsData?
                .payload?
                .LastOrDefault()?
                .row_version ?? rowVersion
            };
        }
    }
}
