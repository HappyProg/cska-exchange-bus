﻿using CSKA.Queue.Domain.Core;

namespace CSKA.Queue.Providers.Infomatika
{
    public class InfomatikaSubscriptionProvider : InfomatikaTicketProvider
    {
        public override string Key => "INFOMATIKA_SUBSCRIPTIONS";
        protected override TransactionType TransactionType => TransactionType.Subscription;
        public InfomatikaSubscriptionProvider(InfomatikaConfiguration cfg) : base(cfg)
        {
        }
    }
}
