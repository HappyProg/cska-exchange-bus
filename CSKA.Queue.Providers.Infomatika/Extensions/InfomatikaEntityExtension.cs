﻿using CSKA.Queue.Domain.Core;
using InfomatikaServiceReference;
using System.Net;
using Event = CSKA.Queue.Domain.Core.Event;

namespace CSKA.Queue.Providers.Infomatika
{
    internal static class InfomatikaEntityExtension
    {
        public static Event CreateDto(this LinkEventData e)
        {
            return new Event
            {
                ArenaId = e.unit_ID,
                ArenaName = WebUtility.HtmlDecode(e.unit_name),
                Date = e.sdate,
                EventId = e.sitting_ID,
                EventName = WebUtility.HtmlDecode(e.sitting_name),
                EventType = GetEventType(e),
                SeatsCount = e.total_count,
                SeatsCountFree = e.free_count
            };
        }

        public static Event CreateDto(this EventRVData e)
        {

            return new Event
            {
                IsActive = e.is_active,
                IsDelete = e.is_del,
                ArenaId = e.unit_ID,
                ArenaName = WebUtility.HtmlDecode(e.unit_name),
                Date = e.sdate,
                EventId = e.sitting_ID,
                EventName = WebUtility.HtmlDecode(e.sitting_name),
                EventType = GetEventType(e),
                SeatsCount = e.total_count,
                SeatsCountFree = e.free_count,
                DateEnd = e.sdate == null || e.duration < 0 ? e.sdate : e.sdate.Value.AddSeconds(e.duration)
            };
        }

        private static EventType GetEventType(EventRVData e)
        {
            if (e.is_abon && e.is_in_series)
                return EventType.Subscription;
            else if (e.is_abon && e.is_in_series == false)
                return EventType.Batch;
            else
                return EventType.Game;
        }
        private static EventType GetEventType(LinkEventData e)
        {
            if (e.is_abon && e.is_in_series)
                return EventType.Subscription;
            else if (e.is_abon && e.is_in_series == false)
                return EventType.Batch;
            else
                return EventType.Game;
        }
    }
}
