﻿using CSKA.Queue.Domain.Core;
using CSKA.Queue.Domain.Interfaces;
using InfomatikaServiceReference;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Event = CSKA.Queue.Domain.Core.Event;

namespace CSKA.Queue.Providers.Infomatika
{
    [XmlRoot(ElementName = "paymentkind")]
    public class InfomatokaPaymentkind
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "summ")]
        public string Summ { get; set; }
    }

    [XmlRoot(ElementName = "paymentkinds")]
    public class InfomatokaPaymentkinds
    {
        [XmlElement(ElementName = "paymentkind")]
        public InfomatokaPaymentkind[] Paymentkind { get; set; }
    }

    internal class InfomatikaResult<T>
    {
        public ICollection<T> Data { get; set; }
        public byte[] RowVersion { get; set; }
    }

    public abstract class InfomatikaProviderBase<TResult> : IQueryProvider<TResult>, IAsyncDisposable
    {
        public DateTime? DateBegin = null;
        public DateTime? DateEnd = null;

        protected abstract TransactionType TransactionType { get; }
        
        protected class InfomatikaProviderState
        {
            public int? EventId { get; set; }
            public byte[] RowVersion { get; set; }
            public byte[] EventRowVersion { get; set; }
        }

        protected class InfomatikaStateCollection
        {
            public DateTime DateFrom { get; set; }
            public DateTime DateTo { get; set; }
            public List<InfomatikaProviderState> States { get; set; } = new List<InfomatikaProviderState>();
        }

        //protected class InfomatikaProviderState
        //{
        //    public int? EventId { get; set; }
        //    public byte[] RowVersion { get; set; }
        //}

        //protected class InfomatikaStateCollection
        //{
        //    public DateTime DateFrom { get; set; }
        //    public DateTime DateTo { get; set; }
        //    public List<InfomatikaProviderState> States { get; set; }
        //}

        public abstract string Key { get; }

        protected readonly GetDataForCRMClient _infomatikaClient;
        private readonly string _url;
        protected readonly string _token;

        protected T DeserializeXML<T>(string xml)
        {
            if (string.IsNullOrWhiteSpace(xml))
                return default;

            var serializer = new XmlSerializer(typeof(T));
            using (var reader = new StringReader(xml))
            {      
                return (T)serializer.Deserialize(reader);
            }
        }

        //protected InfomatikaProviderBase(string url, string token, TransactionType type)
        protected InfomatikaProviderBase(InfomatikaConfiguration cfg)
        {
            if (cfg is null)
            {
                throw new ArgumentNullException(nameof(cfg));
            }

            _url = cfg.Url;
            _token = cfg.Token;

            if (string.IsNullOrWhiteSpace(_url))
            {
                throw new ArgumentException("message", nameof(_url));
            }

            if (string.IsNullOrWhiteSpace(_token))
            {
                throw new ArgumentException("message", nameof(_token));
            }

            _infomatikaClient = CreateClient(cfg.Timeout);


        }

        private GetDataForCRMClient CreateClient(TimeSpan timeout)
        {
            var result = new GetDataForCRMClient();
            result.Endpoint.Binding.OpenTimeout = timeout;
            result.Endpoint.Binding.CloseTimeout = timeout;
            result.Endpoint.Binding.SendTimeout = timeout;
            result.Endpoint.Binding.ReceiveTimeout = timeout;
            result.Endpoint.Address = new EndpointAddress(_url);
            return result;
        }

        protected async Task<EventRVData[]> GetEvents(byte[] rowVersion = null)
        {
            try
            {

                //var e = DeserializeState<EventRVData>("{\"is_active\":true,\"is_del\":false,\"org_id\":4,\"row_version\":\"AAAAABWNzQw=\",\"duration\":7200,\"free_count\":4,\"guest_team_ID\":null,\"host_team_ID\":null,\"is_abon\":false,\"is_in_series\":false,\"nom_ID\":24,\"nom_name\":\"Евролига 2019-2020\",\"plan_data_ID\":38,\"restriction_age\":0,\"total_count\":6532,\"unit_name\":\"ДС &quot;Янтарный&quot;, Калининград, ул. Согласия, 39\",\"begin_pass_date\":\"2020-01-03T17:30:00\",\"end_pass_date\":\"2020-01-04T00:00:00\",\"org_name\":\"ООО «Профессиональный баскетбольный клуб «ЦСКА»\",\"sdate\":\"2020-01-03T20:00:00\",\"sitting_ID\":628,\"sitting_name\":\"EL ЦСКА - Панатинаикос\",\"unit_ID\":20}");

                var eventsData = await _infomatikaClient
                    .GetEventsRVAsync(_token, 3000, rowVersion)
                    .ConfigureAwait(false);

                
                //await _infomatikaClient
                //.GetEventsAsync(_token, null, stateCollection.DateFrom, stateCollection.DateTo)
                //.ConfigureAwait(false);
                //var eeee = eventsData.payload
                //    .Select(s => new { s.sitting_ID, s.sitting_name })
                //    .ToArray();

                var events = eventsData?.payload?
                    //.OrderBy(e => e.row_version)
                    .Where(x => (DateBegin == null || x.sdate >= DateBegin)
                        && (DateEnd == null || x.sdate <= DateEnd)
                        && x.is_del == false)
                    .ToArray();
                
                // var se = SerializeState(e);
                return events;
            }
            catch (Exception ex)
            {
                throw new ProviderNotConnectedException($"[{Key}] Ошибка подключения к сервису: {ex.Message}");
            }
        }

        protected InfomatikaProviderState GetCurrentState(Event e, InfomatikaStateCollection stateCollection)
        {
            var currentState = stateCollection.States
                .FirstOrDefault(x => x.EventId == e.EventId);

            if (currentState == null)
            {
                currentState = new InfomatikaProviderState
                {
                    EventId = e.EventId,
                    RowVersion = null
                };

                stateCollection.States.Add(currentState);
            }

            return currentState;
        }

        protected Gender GetBuyerGender(bool? gender)
        {
            return gender switch
            {
                true => Gender.Male,
                false => Gender.Female,
                _ => Gender.Undefined
            };
        }

        protected string SerializeState<T>(T state)
        {
            return Serializer.JSON.Serialize(state);
        }

        protected virtual T InitState<T>() where T : new()
        {
            return new T();
            //return new InfomatikaStateCollection
            //{
            //    DateFrom = DateTime.ParseExact("01.09.2018 00:00:00", "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture),
            //    DateTo = DateTime.ParseExact("01.03.2021 23:59:59", "dd.MM.yyyy HH:mm:ss", CultureInfo.InvariantCulture),
            //    States = new List<InfomatikaProviderState>()
            //};

        }

        protected T DeserializeState<T>(string state) where T : new()
        {
            return string.IsNullOrWhiteSpace(state) 
                ? InitState<T>()
                : Serializer.JSON.Deserialize<T>(state);
        }

        public async ValueTask DisposeAsync()
        {
            if (_infomatikaClient?.State == CommunicationState.Opened)
            {
                await _infomatikaClient
                    .CloseAsync()
                    .ConfigureAwait(false);
            }
        }



        internal abstract Task<InfomatikaResult<TResult>> GetData(TransactionType transactionType, int pageSize, byte[] rowVersion, Event e);

        protected async Task LoadLinkedEvents(Event e)
        {
            var le = await _infomatikaClient
                .GetLinkEventsAsync(_token, e.EventId)
                .ConfigureAwait(false);

            // Получаем только связанные матчи, без пакетов и абонементов
            //
            e.LinkedEvents = le.payload
                .Where(x => x.is_abon == false && x.is_in_series == false)
                .Select(s => s.CreateDto())
                .ToArray();
        }

        public virtual async Task<QueryResult<TResult>> GetDataAsync(int pageNum, int pageSize, string state = null)
        {
            var stateCollection = DeserializeState<InfomatikaStateCollection>(state);

            //if (stateCollection.DateFrom > stateCollection.DateTo)
            //{
            //    throw new InvalidOperationException("Дата начала периода запроса не может быть больше даты окончания!");
            //}

            var result = new QueryResult<TResult>
            {
                Data = new List<TResult>()
            };

            var events = await GetEvents()
                .ConfigureAwait(false);

            if (events?.Length == 0)
            {
                return result;
            }

            //var names = events
            //    //.Select(eee => new
            //    //{
            //    //    Name = eee.sitting_name,
            //    //    Id = eee.sitting_ID,
            //    //    BeginDate = eee.begin_pass_date,
            //    //    EndDate = eee.end_pass_date,
            //    //    Date = eee.sdate
            //    //})
            //    //.OrderBy(o => o.Id)
            //    .Where(x => x.sitting_ID == 641 || x.sitting_ID == 640)
            //    .ToArray();
            //    //.ToArray();

            // Проходим по событиям
            //
            foreach (var e in events)
            {
                //if (e.sitting_ID != 625)
                //{
                //    continue;
                //}
                var transactEvent = e.CreateDto();

            //    var transactEvent = new Event
            //    {
            //        EventId = e.sitting_ID,
            //        EventName = WebUtility.HtmlDecode(e.sitting_name),
            //        Date = e.sdate,
            //        ArenaId = e.unit_ID,
            //        ArenaName = WebUtility.HtmlDecode(e.unit_name),
            //        SeatsCount = e.total_count,
            //        EventType = GetTicketKind(e)
            //};

                if (transactEvent.EventType == EventType.Batch || transactEvent.EventType == EventType.Subscription)
                {
                    await LoadLinkedEvents(transactEvent).ConfigureAwait(false);
                }

                var currentState = GetCurrentState(transactEvent, stateCollection);

                // Загружаем пока не наберем нужное количество
                //
                while (result.Count < pageSize)
                {
                    // Загружаем билеты / абонементы / проходы на матч
                    // В зависимсоти от transactionType
                    //
                    var transactions = await GetData(TransactionType, pageSize - result.Count,
                        currentState.RowVersion, transactEvent)
                    .ConfigureAwait(false);


                    result.Data.AddRange(transactions.Data);

                    // Обновляем текущий стейт
                    //
                    currentState.RowVersion = transactions.RowVersion;

                    // Завершаем если достигли предела или добрали остаток
                    //
                    if (transactions.Data.Count < pageSize)
                    {
                        break;
                    }
                }

                // Выходим, если набрали нужное количество
                //
                if (result.Count == pageSize)
                {
                    break;
                }
            }

            result.State = SerializeState(stateCollection);
            return result;
        }

    }
}
