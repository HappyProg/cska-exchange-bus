﻿namespace CSKA.Queue.Providers.Infomatika
{
    internal enum InfomatikaSimpleDictionariesKind
    {
        Services = 1,
        PaymentKinds = 2,
        ObjectTypes = 3
    }
}
