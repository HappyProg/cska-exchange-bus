﻿using System;

namespace CSKA.Queue.Providers.Infomatika
{
    public class InfomatikaConfiguration
    {
        public string Url { get; set; }
        public string Token { get; set; }
        public TimeSpan Timeout { get; set; }
    }
}
